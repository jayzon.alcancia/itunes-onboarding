package com.appetiser.module.payments.utils.extensions

import android.app.Activity
import android.content.DialogInterface
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.text.HtmlCompat

fun Activity.showAlertDialog(
    title: String?,
    body: String,
    dialogInterface: (DialogInterface, Int) -> Unit
) {
    if (body.isEmpty()) {
        return
    }

    val alertBuilder = AlertDialog.Builder(this)
    alertBuilder.setTitle(title)
    alertBuilder.setMessage(HtmlCompat.fromHtml(body, HtmlCompat.FROM_HTML_MODE_LEGACY))
    alertBuilder.setCancelable(false)
    alertBuilder.setPositiveButton(this.getString(android.R.string.ok), dialogInterface)
    alertBuilder.create().show()
}

fun Activity.toast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}
