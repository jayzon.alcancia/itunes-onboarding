package com.appetiser.module.domain.models.marketplace

data class SearchFilters(
    var sortType: String = SORT_TYPE_DEFAULT,
    var minPrice: Double? = null,
    var maxPrice: Double? = null,
    var placesId: String? = null,
    var placeName: String? = null,
    var distanceInKm: Int? = null,
    var currentLatitude: Double? = null,
    var currentLongitude: Double? = null
) {
    companion object {
        const val SORT_TYPE_TITLE = "title"
        const val SORT_TYPE_PRICE_LOW_TO_HIGH = "price"
        const val SORT_TYPE_PRICE_HIGH_TO_LOW = "-price"
        const val SORT_TYPE_DEFAULT = SORT_TYPE_TITLE
    }

    fun getPriceRangeCsv(): String? {
        return if (minPrice == null && maxPrice == null) {
            null
        } else {
            "$minPrice,$maxPrice"
        }
    }

    fun getWithinDistanceToCsv(): String? {
        return if (currentLatitude == null || currentLongitude == null) {
            null
        } else {
            val distanceInMeters = if (distanceInKm != null) {
                distanceInKm!! * 1000
            } else {
                0
            }

            "$currentLatitude,$currentLongitude,$distanceInMeters"
        }
    }

    fun getFilterCount(): Int {
        var filterCount = 0

        if (sortType != SORT_TYPE_DEFAULT) {
            filterCount++
        }

        if (minPrice != null || maxPrice != null) {
            // If any of minimum price or maximum price is set.
            filterCount++
        }

        if (distanceInKm != null && distanceInKm!! >= 0) {
            filterCount++
        }

        if (!placeName.isNullOrEmpty()) {
            filterCount++
        }

        return filterCount
    }
}