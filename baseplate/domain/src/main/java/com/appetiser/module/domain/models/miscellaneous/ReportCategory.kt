package com.appetiser.module.domain.models.miscellaneous

data class ReportCategory(
    val id: Int,
    val label: String
)
