package com.appetiser.module.domain.models.places

open class BasePlace(
    var description: String = "",
    var placeId: String = "",
    var name: String = "",
)