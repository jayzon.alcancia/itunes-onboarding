package com.appetiser.module.domain.models.places

data class Place(
    val latitude: Double,
    val longitude: Double
) : BasePlace()