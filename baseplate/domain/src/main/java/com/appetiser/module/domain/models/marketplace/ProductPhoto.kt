package com.appetiser.module.domain.models.marketplace

data class ProductPhoto(
    val collectionName: String = "",
    val createdAt: String = "",
    val fileName: String = "",
    val id: Long = 0,
    val mimeType: String = "",
    val name: String = "",
    val size: Long = 0,
    val thumbUrl: String = "",
    val url: String = ""
)