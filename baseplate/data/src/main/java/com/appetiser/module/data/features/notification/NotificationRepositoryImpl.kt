package com.appetiser.module.data.features.notification

import com.appetiser.module.domain.models.notification.Notification
import com.appetiser.module.local.features.session.SessionLocalSource
import com.appetiser.module.network.features.notification.NotificationRemoteSource
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

class NotificationRepositoryImpl @Inject constructor(
    private val sessionLocalSource: SessionLocalSource,
    private val notificationRemoteSource: NotificationRemoteSource
) : NotificationRepository {

    override fun registerDeviceToken(
        deviceToken: String,
        deviceId: String
    ): Single<Boolean> {
        return sessionLocalSource
            .getSession()
            .flatMap {
                notificationRemoteSource
                    .registerDeviceToken(
                        it.accessToken,
                        deviceToken,
                        deviceId
                    )
            }
    }

    override fun unRegisterDeviceToken(deviceToken: String, deviceId: String): Single<Boolean> {
        return sessionLocalSource
            .getSession()
            .flatMap {
                notificationRemoteSource.unRegisterDeviceToken(it.accessToken, deviceToken, deviceId)
            }
    }

    override fun getNotifications(type: String): Observable<List<Notification>> {
        return sessionLocalSource
            .getSession()
            .flatMapObservable {
                notificationRemoteSource.getNotifications(it.accessToken, type)
            }
    }
}
