package com.appetiser.module.data.features.places

import com.appetiser.module.domain.models.places.Place
import com.appetiser.module.domain.models.places.SearchPlace
import io.reactivex.Single

interface PlacesRepository {
    fun getPlaces(query: String): Single<List<SearchPlace>>

    fun getNearbyPlaces(latitude: Double, longitude: Double): Single<List<Place>>

    fun getPlacesDetails(placesId: String): Single<Place>
}
