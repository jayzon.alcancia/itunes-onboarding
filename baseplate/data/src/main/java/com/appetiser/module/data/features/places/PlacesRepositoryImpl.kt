package com.appetiser.module.data.features.places

import com.appetiser.module.domain.models.places.Place
import com.appetiser.module.domain.models.places.SearchPlace
import com.appetiser.module.local.features.session.SessionLocalSource
import com.appetiser.module.network.features.places.PlacesRemoteSource
import io.reactivex.Single
import javax.inject.Inject

class PlacesRepositoryImpl @Inject constructor(
    private val sessionLocalSource: SessionLocalSource,
    private val placesRemoteSource: PlacesRemoteSource
) : PlacesRepository {

    override fun getPlaces(query: String): Single<List<SearchPlace>> {
        return sessionLocalSource
            .getSession()
            .flatMap { session ->
                placesRemoteSource
                    .getPlaces(
                        session.accessToken,
                        query
                    )
            }
    }

    override fun getNearbyPlaces(latitude: Double, longitude: Double): Single<List<Place>> {
        return sessionLocalSource
            .getSession()
            .flatMap { session ->
                placesRemoteSource
                    .getNearbyPlaces(
                        session.accessToken,
                        latitude,
                        longitude
                    )
            }
    }

    override fun getPlacesDetails(placesId: String): Single<Place> {
        return sessionLocalSource
            .getSession()
            .flatMap { session ->
                placesRemoteSource
                    .getPlacesDetails(
                        session.accessToken,
                        placesId
                    )
            }
    }
}
