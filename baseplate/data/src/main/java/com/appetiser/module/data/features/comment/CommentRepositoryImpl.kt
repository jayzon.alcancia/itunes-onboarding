package com.appetiser.module.data.features.comment

import com.appetiser.module.domain.models.Paging
import com.appetiser.module.domain.models.feed.comment.Comment
import com.appetiser.module.local.features.session.SessionLocalSource
import com.appetiser.module.network.features.comment.CommentRemoteSource
import io.reactivex.Single
import javax.inject.Inject

class CommentRepositoryImpl @Inject constructor(
    private val sessionLocalSource: SessionLocalSource,
    private val commentRemoteSource: CommentRemoteSource
) : CommentRepository {

    override fun getComments(feedId: Long, page: Int): Single<Paging<Comment>> {
        return sessionLocalSource
            .getSession()
            .flatMap {
                commentRemoteSource
                    .getComments(
                        it.accessToken,
                        feedId,
                        page
                    )
            }
    }

    override fun getCommentDetails(commentId: Long): Single<Comment> {
        return sessionLocalSource
            .getSession()
            .flatMap {
                commentRemoteSource
                    .getCommentDetails(
                        it.accessToken,
                        commentId
                    )
            }
    }

    override fun postComment(postId: Long, body: String): Single<Comment> {
        return sessionLocalSource
            .getSession()
            .flatMap {
                commentRemoteSource
                    .postComment(
                        it.accessToken,
                        postId,
                        body
                    )
            }
    }
}
