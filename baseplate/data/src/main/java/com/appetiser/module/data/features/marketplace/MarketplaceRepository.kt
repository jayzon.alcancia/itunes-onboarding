package com.appetiser.module.data.features.marketplace

import com.appetiser.module.domain.models.Paging
import com.appetiser.module.domain.models.marketplace.Product
import com.appetiser.module.domain.models.marketplace.ProductCategory
import com.appetiser.module.domain.models.marketplace.SearchFilters
import io.reactivex.Observable
import io.reactivex.Single

interface MarketplaceRepository {

    fun postProducts(
        title: String,
        description: String,
        price: String,
        photos: List<String>?,
        placesId: String? = "",
        categoryId: Long? = 0
    ): Single<Product>

    fun getProductCategories(): Observable<List<ProductCategory>>

    fun getProductDetails(productId: Long): Single<Product>

    fun getSimilarProducts(productId: Long): Observable<List<Product>>

    fun getProducts(page: Int): Single<Paging<Product>>

    fun getSellerProducts(page: Int, profileId: Long): Single<Paging<Product>>

    fun searchProducts(
        page: Int,
        keyword: String,
        searchFilters: SearchFilters
    ): Single<Paging<Product>>
}
