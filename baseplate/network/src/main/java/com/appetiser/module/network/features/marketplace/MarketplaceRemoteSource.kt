package com.appetiser.module.network.features.marketplace

import com.appetiser.module.domain.models.Paging
import com.appetiser.module.domain.models.marketplace.Product
import com.appetiser.module.domain.models.marketplace.ProductCategory
import com.appetiser.module.domain.models.marketplace.SearchFilters
import com.appetiser.module.domain.models.token.AccessToken
import io.reactivex.Observable
import io.reactivex.Single

interface MarketplaceRemoteSource {

    fun postProducts(
        accessToken: AccessToken,
        title: String,
        description: String,
        price: String,
        photos: List<String>?,
        placesId: String?,
        categoryId: Long?
    ): Single<Product>

    fun getProductDetails(accessToken: AccessToken, productId: Long): Single<Product>

    fun getSimilarProducts(accessToken: AccessToken, productId: Long): Observable<List<Product>>

    fun getProductCategories(accessToken: AccessToken): Observable<List<ProductCategory>>

    fun getProducts(accessToken: AccessToken, page: Int): Single<Paging<Product>>

    fun searchProducts(
        accessToken: AccessToken,
        page: Int,
        keyword: String,
        searchFilters: SearchFilters
    ): Single<Paging<Product>>

    fun getSellerProducts(accessToken: AccessToken, page: Int, profileId: Long): Single<Paging<Product>>
}
