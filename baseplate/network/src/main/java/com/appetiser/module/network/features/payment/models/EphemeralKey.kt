package com.appetiser.module.network.features.payment.models

import com.google.gson.annotations.SerializedName

/**
 * NOTE: This is only used for receiving API response.
 * This should be converted to json when returning from remote source.
 */
data class EphemeralKey(
    val id: String,
    @SerializedName("object") val obj: String,
    @SerializedName("associated_objects") val associatedObjects: List<AssociatedObjects>,
    val created: Long,
    val expires: Long,
    @SerializedName("livemode") val liveMode: Boolean,
    val secret: String
)

data class AssociatedObjects(
    val id: String,
    val type: String
)
