package com.appetiser.module.network.features.places

import com.appetiser.module.domain.models.places.Place
import com.appetiser.module.domain.models.places.SearchPlace
import com.appetiser.module.domain.models.token.AccessToken
import io.reactivex.Single

interface PlacesRemoteSource {
    fun getPlaces(
        accessToken: AccessToken,
        query: String
    ): Single<List<SearchPlace>>

    fun getNearbyPlaces(
        accessToken: AccessToken,
        latitude: Double,
        longitude: Double
    ): Single<List<Place>>

    fun getPlacesDetails(
        accessToken: AccessToken,
        placesId: String
    ): Single<Place>
}
