package com.appetiser.module.network.features.payouts

import com.google.gson.annotations.SerializedName

data class GetPayoutDetailsResponse(
    val data: Data,
    val http_status: Int,
    val success: Boolean
) {
    data class Data(
        val id: String,
        val country: String,
        val individual: Individual,
        @SerializedName("external_accounts") val externalAccounts: ExternalAccounts
    ) {
        data class Individual(
            val id: String,
            val address: Address,
            val dob: DateOfBirth,
            @SerializedName("first_name") val firstName: String,
            @SerializedName("last_name") val lastName: String
        ) {
            data class Address(
                val city: String,
                val country: String,
                val line1: String,
                @SerializedName("postal_code") val postalCode: String,
                val state: String
            )

            data class DateOfBirth(
                val day: Int,
                val month: Int,
                val year: Int
            )
        }

        data class ExternalAccounts(
            val data: List<ExternalAccount>
        ) {
            data class ExternalAccount(
                val id: String,
                val `object`: String,
                val account: String,
                @SerializedName("account_holder_name") val accountHolderName: String,
                @SerializedName("routing_number") val routingNumber: String,
                val last4: String
            )
        }
    }
}
