package com.appetiser.module.network.features.payment

import com.appetiser.module.domain.models.token.AccessToken
import io.reactivex.Single

interface PaymentRemoteSource {
    /**
     * Used to initialize stripe CustomerSession.
     */
    fun createEphemeralKey(
        accessToken: AccessToken
    ): Single<String>
}
