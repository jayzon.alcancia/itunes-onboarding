package com.appetiser.module.network.features.places.models

import com.appetiser.module.domain.models.places.Place
import com.appetiser.module.domain.models.places.SearchPlace
import com.google.gson.annotations.SerializedName

data class PlaceDTO(
    @SerializedName("description") val description: String? = "",
    @SerializedName("place_id") val placeId: String? = "",
    @SerializedName("name") val name: String?,
    @SerializedName("geometry") val geometry: GeometryDTO?
) {
    companion object {
        fun toSearchPlace(dto: PlaceDTO): SearchPlace {
            return SearchPlace().apply {
                description = dto.description.orEmpty()
                placeId = dto.placeId.orEmpty()
                name = if (dto.name.isNullOrEmpty()) {
                    dto.description.orEmpty()
                } else {
                    dto.name
                }
            }
        }

        fun toDomain(dto: PlaceDTO): Place {
            return Place(
                latitude = dto.geometry?.location?.latitude ?: 0.0,
                longitude = dto.geometry?.location?.longitude ?: 0.0
            ).apply {
                description = dto.description.orEmpty()
                placeId = dto.placeId.orEmpty()
                name = if (dto.name.isNullOrEmpty()) {
                    dto.description.orEmpty()
                } else {
                    dto.name
                }
            }
        }

        fun mapPlacesDTOToPlaces(dto: List<PlaceDTO>?): List<Place> {
            return if (!dto.isNullOrEmpty()) dto.map { PlaceDTO.toDomain(it) } else emptyList()
        }
    }
}

data class GeometryDTO(
    @SerializedName("location") val location: LocationDTO?
)

data class LocationDTO(
    @SerializedName("lat") val latitude: Double,
    @SerializedName("lng") val longitude: Double
)
