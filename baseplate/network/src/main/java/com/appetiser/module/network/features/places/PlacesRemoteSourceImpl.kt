package com.appetiser.module.network.features.places

import com.appetiser.module.domain.models.places.Place
import com.appetiser.module.domain.models.places.SearchPlace
import com.appetiser.module.domain.models.token.AccessToken
import com.appetiser.module.network.base.BaseRemoteSource
import com.appetiser.module.network.features.BaseplateApiServices
import com.appetiser.module.network.features.places.models.PlaceDTO
import io.reactivex.Single
import javax.inject.Inject

class PlacesRemoteSourceImpl @Inject constructor(
    private val apiServices: BaseplateApiServices
) : BaseRemoteSource(), PlacesRemoteSource {

    override fun getPlaces(
        accessToken: AccessToken,
        query: String
    ): Single<List<SearchPlace>> {
        return apiServices
            .getPlaces(
                accessToken.bearerToken,
                query
            )
            .map { response ->
                response.data.map {
                    PlaceDTO.toSearchPlace(it)
                }
            }
    }

    override fun getNearbyPlaces(
        accessToken: AccessToken,
        latitude: Double,
        longitude: Double
    ): Single<List<Place>> {
        return apiServices
            .getNearbyPlaces(
                accessToken.bearerToken,
                latitude,
                longitude
            )
            .map { response ->
                response.data.map {
                    PlaceDTO.toDomain(it)
                }
            }
    }

    override fun getPlacesDetails(
        accessToken: AccessToken,
        placesId: String
    ): Single<Place> {
        return apiServices
            .getPlaceDetails(
                accessToken.bearerToken,
                placesId
            )
            .map { response ->
                PlaceDTO.toDomain(
                    response.data
                )
            }
    }
}
