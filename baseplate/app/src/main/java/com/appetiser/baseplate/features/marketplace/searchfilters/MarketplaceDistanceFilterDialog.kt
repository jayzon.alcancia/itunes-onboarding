package com.appetiser.baseplate.features.marketplace.searchfilters

import android.content.DialogInterface
import androidx.core.os.bundleOf
import com.appetiser.baseplate.R
import com.appetiser.baseplate.databinding.BottomSheetDistanceFilterBinding
import com.appetiser.module.common.ninjaTap
import com.appetiser.module.domain.utils.DEFAULT_SEARCH_FILTER_DISTANCE_IN_KM
import io.reactivex.rxkotlin.addTo

class MarketplaceDistanceFilterDialog : BaseSearchFiltersBottomSheetDialog<BottomSheetDistanceFilterBinding>() {

    companion object {
        private const val ARGS_DISTANCE_IN_KM = "ARGS_DISTANCE_IN_KM"

        fun newInstance(distanceInKm: Int): MarketplaceDistanceFilterDialog {
            return MarketplaceDistanceFilterDialog()
                .apply {
                    arguments = bundleOf(ARGS_DISTANCE_IN_KM to distanceInKm)
                }
        }
    }

    private var distanceInKm: Int = DEFAULT_SEARCH_FILTER_DISTANCE_IN_KM
    private var distanceSetListener: ((distanceInKm: Int) -> Unit)? = null

    override fun getLayoutId(): Int = R.layout.bottom_sheet_distance_filter

    override fun isNested(): Boolean = true

    override fun onDialogCreated() {
        distanceInKm = requireArguments().getInt(ARGS_DISTANCE_IN_KM, DEFAULT_SEARCH_FILTER_DISTANCE_IN_KM)

        setupViews()
    }

    private fun setupViews() {
        setupClickListeners()

        binding.sliderDistance.apply {
            addOnChangeListener { _, value, _ ->
                binding.txtDistance.text = getKmFormat(value)
            }

            setLabelFormatter(::getKmFormat)
        }

        binding.sliderDistance.value = distanceInKm.toFloat()
    }

    private fun setupClickListeners() {
        binding
            .btnBack
            .ninjaTap {
                dismiss()
            }
            .addTo(disposables)

        binding
            .btnClear
            .ninjaTap {
                binding.sliderDistance.value = DEFAULT_SEARCH_FILTER_DISTANCE_IN_KM.toFloat()
            }
            .addTo(disposables)
    }

    private fun getKmFormat(value: Float) = getString(R.string.km_format, value.toInt())

    fun setOnDistanceSetListener(callback: (distanceInKm: Int) -> Unit) {
        distanceSetListener = callback
    }

    override fun onDismiss(dialog: DialogInterface) {
        distanceSetListener?.invoke(binding.sliderDistance.value.toInt())
        super.onDismiss(dialog)
    }
}
