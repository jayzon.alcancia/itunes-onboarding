package com.appetiser.baseplate.features.marketplace.createitem

import com.appetiser.baseplate.features.marketplace.createitem.adapter.PhotoAttachmentsViewType
import com.appetiser.baseplate.features.marketplace.createitem.adapter.PhotoAttachmentsViewTypeObject
import com.appetiser.module.domain.models.marketplace.Product
import com.appetiser.module.domain.models.marketplace.ProductCategory

sealed class CreateMarketPlaceItemState {
    data class AddPhoto(val imagePath: String) : CreateMarketPlaceItemState()
    data class RemovePhoto(val item: PhotoAttachmentsViewTypeObject) : CreateMarketPlaceItemState()
    data class DisplayAddButton(val item: PhotoAttachmentsViewType) : CreateMarketPlaceItemState()
    object ShowProgressLoading : CreateMarketPlaceItemState()
    object HideProgressLoading : CreateMarketPlaceItemState()
    data class Error(val throwable: Throwable) : CreateMarketPlaceItemState()

    object NoImagesFound : CreateMarketPlaceItemState()

    data class UploadProductSuccess(val item: Product) : CreateMarketPlaceItemState()

    data class GetProductCategories(val items: List<ProductCategory>) : CreateMarketPlaceItemState()
}
