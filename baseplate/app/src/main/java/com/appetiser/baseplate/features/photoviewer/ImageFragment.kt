package com.appetiser.baseplate.features.photoviewer

import android.os.Bundle
import android.view.View
import com.appetiser.baseplate.R
import com.appetiser.baseplate.base.BaseFragment
import com.appetiser.baseplate.databinding.ItemPhotoViewerBinding
import com.appetiser.baseplate.ext.loadAvatarUrl

class ImageFragment : BaseFragment<ItemPhotoViewerBinding>() {

    companion object {
        private const val EXTRA_PATH = "EXTRA_PATH"
        fun newInstance(path: String): ImageFragment {
            val fragment = ImageFragment()
            val args = Bundle()
            args.putString(EXTRA_PATH, path)
            fragment.arguments = args
            return fragment
        }
    }

    override fun getLayoutId(): Int = R.layout.item_photo_viewer

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (arguments != null) {
            val path: String = requireArguments().getString(EXTRA_PATH).orEmpty()
            binding.ivContent.loadAvatarUrl(path)
        }
    }
}
