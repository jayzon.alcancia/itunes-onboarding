package com.appetiser.baseplate.features.main

sealed class MainState {

    object ShowProgressLoading : MainState()

    object HideProgressLoading : MainState()
}
