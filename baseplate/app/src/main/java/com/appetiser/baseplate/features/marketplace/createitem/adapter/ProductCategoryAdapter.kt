package com.appetiser.baseplate.features.marketplace.createitem.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.appetiser.baseplate.R
import com.appetiser.module.domain.models.marketplace.ProductCategory

class ProductCategoryAdapter(
    context: Context,
    private val productCategoryList: List<ProductCategory>
) : ArrayAdapter<ProductCategory>(context, R.layout.item_marketplace_category) {

    override fun getItem(position: Int): ProductCategory? {
        return productCategoryList[position]
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        return convertView ?: LayoutInflater
            .from(parent.context)
            .inflate(R.layout.item_marketplace_category, parent, false)
            .apply {
                findViewById<TextView>(R.id.selectedCategory)
                    .text = productCategoryList[position].label
            }
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        return convertView ?: LayoutInflater
            .from(parent.context)
            .inflate(R.layout.item_marketplace_category, parent, false)
            .apply {
                findViewById<TextView>(R.id.selectedCategory)
                    .text = productCategoryList[position].label
            }
    }

    override fun getCount(): Int = productCategoryList.size
}
