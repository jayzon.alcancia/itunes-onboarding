package com.appetiser.baseplate.features.marketplace.searchfilters

import android.content.DialogInterface
import androidx.core.os.bundleOf
import com.appetiser.baseplate.R
import com.appetiser.baseplate.databinding.BottomSheetSortByFilterBinding
import com.appetiser.module.common.ninjaTap
import com.appetiser.module.domain.models.marketplace.SearchFilters
import io.reactivex.rxkotlin.addTo

class MarketplaceSortByFilterDialog : BaseSearchFiltersBottomSheetDialog<BottomSheetSortByFilterBinding>() {

    companion object {
        private const val ARGS_SORT_TYPE = "ARGS_SORT_TYPE"

        fun newInstance(sortType: String): MarketplaceSortByFilterDialog {
            return MarketplaceSortByFilterDialog().apply {
                arguments = bundleOf(ARGS_SORT_TYPE to sortType)
            }
        }
    }

    private var sortTypeSetListener: ((selectedSortType: String) -> Unit)? = null
    private var selectedSortType = SearchFilters.SORT_TYPE_DEFAULT

    override fun getLayoutId(): Int = R.layout.bottom_sheet_sort_by_filter

    override fun isNested() = true

    override fun onDialogCreated() {
        selectedSortType = requireArguments().getString(ARGS_SORT_TYPE, SearchFilters.SORT_TYPE_DEFAULT)

        setupViews()
    }

    private fun setupViews() {
        setupClickListeners()
        preSelectSortType()
    }

    private fun preSelectSortType() {
        when (selectedSortType) {
            SearchFilters.SORT_TYPE_DEFAULT -> {
                binding.txtDefault.isSelected = true

                binding.txtPriceLowToHigh.isSelected = false
                binding.txtPriceHighToLow.isSelected = false
            }
            SearchFilters.SORT_TYPE_PRICE_LOW_TO_HIGH -> {
                binding.txtPriceLowToHigh.isSelected = true

                binding.txtDefault.isSelected = false
                binding.txtPriceHighToLow.isSelected = false
            }
            SearchFilters.SORT_TYPE_PRICE_HIGH_TO_LOW -> {
                binding.txtPriceHighToLow.isSelected = true

                binding.txtDefault.isSelected = false
                binding.txtPriceLowToHigh.isSelected = false
            }
        }
    }

    private fun setupClickListeners() {
        binding
            .btnBack
            .ninjaTap {
                dismiss()
            }
            .addTo(disposables)

        binding
            .txtDefault
            .ninjaTap {
                selectedSortType = SearchFilters.SORT_TYPE_DEFAULT

                binding.txtDefault.isSelected = true
                binding.txtPriceLowToHigh.isSelected = false
                binding.txtPriceHighToLow.isSelected = false
            }
            .addTo(disposables)

        binding
            .txtPriceLowToHigh
            .ninjaTap {
                selectedSortType = SearchFilters.SORT_TYPE_PRICE_LOW_TO_HIGH

                binding.txtPriceLowToHigh.isSelected = true
                binding.txtDefault.isSelected = false
                binding.txtPriceHighToLow.isSelected = false
            }
            .addTo(disposables)

        binding
            .txtPriceHighToLow
            .ninjaTap {
                selectedSortType = SearchFilters.SORT_TYPE_PRICE_HIGH_TO_LOW

                binding.txtPriceHighToLow.isSelected = true
                binding.txtDefault.isSelected = false
                binding.txtPriceLowToHigh.isSelected = false
            }
            .addTo(disposables)
    }

    override fun onDismiss(dialog: DialogInterface) {
        sortTypeSetListener?.invoke(selectedSortType)
        super.onDismiss(dialog)
    }

    fun setOnSortTypeSetListener(callback: (selectedSortType: String) -> Unit) {
        sortTypeSetListener = callback
    }
}
