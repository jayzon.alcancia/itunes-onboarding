package com.appetiser.baseplate.features.auth.register.profile

sealed class UploadPhotoState {

    object ShowProgressLoading : UploadPhotoState()

    object HideProgressLoading : UploadPhotoState()

    object SuccessUploadPhoto : UploadPhotoState()

    data class ErrorUploadPhoto(val throwable: Throwable) : UploadPhotoState()
}
