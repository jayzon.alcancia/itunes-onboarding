package com.appetiser.baseplate.features.marketplace.seller.profile

import androidx.paging.PagingData
import com.appetiser.module.domain.models.marketplace.Product
import com.appetiser.module.domain.models.user.User

sealed class MarketplaceSellerProfileState {

    data class GetSellerProfileDetails(val seller: User) : MarketplaceSellerProfileState()

    data class GetProducts(val products: PagingData<Product>) : MarketplaceSellerProfileState()

    data class ShowPhotoViewerScreen(val photos: List<String>) : MarketplaceSellerProfileState()

    data class Error(val message: String) : MarketplaceSellerProfileState()
}
