package com.appetiser.baseplate.di.builders

import com.appetiser.baseplate.di.scopes.ServiceScope
import com.appetiser.baseplate.services.fcm.AppFirebaseMessagingService
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ServiceBuilder {

    @ServiceScope
    @ContributesAndroidInjector
    abstract fun contributeAppFirebaseMessagingService(): AppFirebaseMessagingService
}
