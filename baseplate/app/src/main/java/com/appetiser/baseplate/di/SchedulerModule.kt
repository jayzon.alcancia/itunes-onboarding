package com.appetiser.baseplate.di

import com.appetiser.baseplate.utils.schedulers.BaseSchedulerProvider
import com.appetiser.baseplate.utils.schedulers.SchedulerProvider
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class SchedulerModule {

    @Provides
    @Singleton
    fun providesSchedulerSource(): BaseSchedulerProvider =
        SchedulerProvider.getInstance()
}
