package com.appetiser.baseplate.features.searchlocation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.appetiser.baseplate.R
import com.appetiser.baseplate.databinding.ItemSearchLocationBinding
import com.appetiser.module.domain.models.places.SearchPlace

class SearchLocationAdapter(
    private val itemClickListener: (item: SearchPlace) -> Unit
) : ListAdapter<SearchPlace, SearchLocationAdapter.ViewHolder>(PlaceDiffUtil()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            DataBindingUtil
                .inflate<ItemSearchLocationBinding>(
                    LayoutInflater.from(parent.context),
                    R.layout.item_search_location,
                    parent,
                    false
                )
        val holder = ViewHolder(binding)

        binding.itemClickable.setOnClickListener {
            itemClickListener
                .invoke(
                    currentList[holder.bindingAdapterPosition]
                )
        }

        return holder
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(currentList[position])
    }

    class ViewHolder(val binding: ItemSearchLocationBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: SearchPlace) {
            binding.item = item
            binding.executePendingBindings()
        }
    }
}
