package com.appetiser.baseplate.features.marketplace.searchfilters

import androidx.core.os.bundleOf
import com.appetiser.baseplate.R
import com.appetiser.baseplate.databinding.BottomSheetSearchFiltersBinding
import com.appetiser.baseplate.features.marketplace.searchfilters.location.MarketplaceLocationFilterDialog
import com.appetiser.module.common.ninjaTap
import com.appetiser.module.domain.models.marketplace.SearchFilters
import com.appetiser.module.domain.utils.*
import io.reactivex.rxkotlin.addTo
import timber.log.Timber

class MarketplaceSearchFiltersDialog : BaseSearchFiltersBottomSheetDialog<BottomSheetSearchFiltersBinding>() {

    companion object {
        private const val ARGS_SORT_TYPE = "ARGS_SORT_TYPE"
        private const val ARGS_MIN_PRICE = "ARGS_MIN_PRICE"
        private const val ARGS_MAX_PRICE = "ARGS_MAX_PRICE"
        private const val ARGS_DISTANCE_IN_KM = "ARGS_DISTANCE_IN_KM"
        private const val ARGS_PLACE_ID = "ARGS_PLACES_ID"
        private const val ARGS_PLACE_NAME = "ARGS_PLACES_NAME"
        private const val ARGS_CURRENT_LATITUDE = "ARGS_CURRENT_LATITUDE"
        private const val ARGS_CURRENT_LONGITUDE = "ARGS_CURRENT_LONGITUDE"

        fun newInstance(searchFilters: SearchFilters): MarketplaceSearchFiltersDialog {
            return MarketplaceSearchFiltersDialog().apply {
                arguments =
                    bundleOf(
                        ARGS_SORT_TYPE to searchFilters.sortType,
                        ARGS_DISTANCE_IN_KM to searchFilters.distanceInKm,
                        ARGS_PLACE_ID to searchFilters.placesId,
                        ARGS_PLACE_NAME to searchFilters.placeName
                    ).apply {
                        if (searchFilters.minPrice != null) {
                            putDouble(ARGS_MIN_PRICE, searchFilters.minPrice!!)
                        }

                        if (searchFilters.maxPrice != null) {
                            putDouble(ARGS_MAX_PRICE, searchFilters.maxPrice!!)
                        }

                        if (searchFilters.currentLatitude != null) {
                            putDouble(ARGS_CURRENT_LATITUDE, searchFilters.currentLatitude!!)
                        }

                        if (searchFilters.currentLongitude != null) {
                            putDouble(ARGS_CURRENT_LONGITUDE, searchFilters.currentLongitude!!)
                        }
                    }
            }
        }
    }

    override fun getLayoutId(): Int = R.layout.bottom_sheet_search_filters

    private var minPrice: Double? = null
    private var maxPrice: Double? = null
    private var distanceInKm: Int = DEFAULT_SEARCH_FILTER_DISTANCE_IN_KM
    private var sortType: String = SearchFilters.SORT_TYPE_DEFAULT
    private var placeId: String? = null
    private var placeName: String? = null
    private var currentLatitude: Double? = null
    private var currentLongitude: Double? = null

    private var onApplyFilterCallback: ((searchFilters: SearchFilters) -> Unit)? = null

    override fun onDialogCreated() {
        sortType = requireArguments().getString(ARGS_SORT_TYPE, SearchFilters.SORT_TYPE_DEFAULT)

        if (requireArguments().containsKey(ARGS_MIN_PRICE)) {
            minPrice = requireArguments().getDouble(ARGS_MIN_PRICE)
        }

        if (requireArguments().containsKey(ARGS_MAX_PRICE)) {
            maxPrice = requireArguments().getDouble(ARGS_MAX_PRICE)
        }

        distanceInKm = requireArguments().getInt(ARGS_DISTANCE_IN_KM, DEFAULT_SEARCH_FILTER_DISTANCE_IN_KM)
        placeId = requireArguments().getString(ARGS_PLACE_ID)
        placeName = requireArguments().getString(ARGS_PLACE_NAME)

        if (requireArguments().containsKey(ARGS_CURRENT_LATITUDE)) {
            currentLatitude = requireArguments().getDouble(ARGS_CURRENT_LATITUDE)
        }

        if (requireArguments().containsKey(ARGS_CURRENT_LONGITUDE)) {
            currentLongitude = requireArguments().getDouble(ARGS_CURRENT_LONGITUDE)
        }

        setupViews()
    }

    private fun setupViews() {
        displayFilters()
        setupClickListeners()
    }

    private fun displayFilters() {
        displaySortType()
        displayPriceRange()
        displayDistance()
        displaySelectedLocation()
    }

    private fun setupClickListeners() {
        binding
            .btnDone
            .ninjaTap {
                onApplyFilterCallback?.invoke(
                    SearchFilters(
                        sortType = sortType,
                        minPrice = minPrice,
                        maxPrice = maxPrice,
                        distanceInKm = distanceInKm,
                        placesId = placeId,
                        placeName = placeName,
                        currentLatitude = currentLatitude,
                        currentLongitude = currentLongitude
                    )
                )
                dismiss()
            }
            .addTo(disposables)

        binding
            .layoutSortBy
            .ninjaTap {
                showSortByFilterDialog()
            }
            .addTo(disposables)

        binding
            .layoutPrice
            .ninjaTap {
                showPriceFilterDialog()
            }
            .addTo(disposables)

        binding
            .layoutDistance
            .ninjaTap {
                showDistanceFilterDialog()
            }
            .addTo(disposables)

        binding
            .layoutLocation
            .ninjaTap {
                showLocationFilterDialog()
            }
            .addTo(disposables)

        binding
            .btnReset
            .ninjaTap {
                resetFilters()
            }
            .addTo(disposables)
    }

    private fun resetFilters() {
        minPrice = null
        maxPrice = null
        distanceInKm = DEFAULT_SEARCH_FILTER_DISTANCE_IN_KM
        sortType = SearchFilters.SORT_TYPE_DEFAULT
        placeId = null
        placeName = null
        currentLatitude = null
        currentLongitude = null

        displayFilters()
    }

    fun setOnApplyFilterCallback(callback: (searchFilters: SearchFilters) -> Unit) {
        this.onApplyFilterCallback = callback
    }

    private fun showPriceFilterDialog() {
        val priceFilterDialog =
            MarketplacePriceFilterDialog
                .newInstance(
                    minPrice,
                    maxPrice
                )
        priceFilterDialog.setOnPriceSetListener { minPrice, maxPrice ->
            Timber.tag("showPriceFilterDialog").d("$minPrice, $maxPrice")
            this.minPrice = minPrice
            this.maxPrice = maxPrice

            displayPriceRange()
        }
        priceFilterDialog.show(childFragmentManager, null)
    }

    private fun displayPriceRange() {
        val isMinPriceEmpty = minPrice == null || minPrice!! <= DEFAULT_SEARCH_FILTER_PRICE_RANGE
        val isMaxPriceEmpty = maxPrice == null || maxPrice!! <= DEFAULT_SEARCH_FILTER_PRICE_RANGE

        if (isMinPriceEmpty && isMaxPriceEmpty) {
            binding.txtPrice.text = getString(R.string.any)
            return
        }

        val minPriceText =
            if (isMinPriceEmpty) {
                getString(R.string.any)
            } else {
                String.format("%.2f", minPrice)
            }

        val maxPriceText =
            if (isMaxPriceEmpty) {
                getString(R.string.any)
            } else {
                String.format("%.2f", maxPrice)
            }

        binding.txtPrice.text =
            getString(
                R.string.price_range_format,
                minPriceText,
                maxPriceText
            )
    }

    private fun showDistanceFilterDialog() {
        val distanceFilterDialog =
            MarketplaceDistanceFilterDialog
                .newInstance(
                    distanceInKm
                )
        distanceFilterDialog.setOnDistanceSetListener { distanceInKm ->
            this.distanceInKm = distanceInKm

            displayDistance()
        }
        distanceFilterDialog.show(childFragmentManager, null)
    }

    private fun displayDistance() {
        binding.txtDistance.text = getString(R.string.km_format, distanceInKm)
    }

    private fun showLocationFilterDialog() {
        val locationFilterDialog =
            MarketplaceLocationFilterDialog
                .newInstance(
                    placeId,
                    placeName,
                    currentLatitude,
                    currentLongitude
                )
        locationFilterDialog
            .setOnLocationSetListener { placeId, placeName, currentLatitude, currentLongitude ->
                this.placeId = placeId
                this.placeName = placeName
                this.currentLatitude = currentLatitude
                this.currentLongitude = currentLongitude

                displaySelectedLocation()
            }
        locationFilterDialog.show(childFragmentManager, null)
    }

    private fun displaySelectedLocation() {
        if (placeName.isNullOrEmpty()) {
            binding.txtLocation.text = getString(R.string.any)
            return
        }

        binding.txtLocation.text = placeName
    }

    private fun showSortByFilterDialog() {
        val sortByFilterDialog = MarketplaceSortByFilterDialog.newInstance(sortType)
        sortByFilterDialog.setOnSortTypeSetListener { selectedSortType ->
            sortType = selectedSortType

            displaySortType()
        }
        sortByFilterDialog.show(childFragmentManager, null)
    }

    private fun displaySortType() {
        val sortByText = when (sortType) {
            SearchFilters.SORT_TYPE_DEFAULT -> {
                getString(R.string.default_text)
            }
            SearchFilters.SORT_TYPE_PRICE_LOW_TO_HIGH -> {
                getString(R.string.price_low_to_high)
            }
            SearchFilters.SORT_TYPE_PRICE_HIGH_TO_LOW -> {
                getString(R.string.price_high_to_low)
            }
            else -> {
                getString(R.string.default_text)
            }
        }

        binding.txtSortBy.text = sortByText
    }
}
