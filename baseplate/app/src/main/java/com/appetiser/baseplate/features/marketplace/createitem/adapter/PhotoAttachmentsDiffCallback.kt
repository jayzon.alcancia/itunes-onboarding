package com.appetiser.baseplate.features.marketplace.createitem.adapter

import androidx.recyclerview.widget.DiffUtil

class PhotoAttachmentsDiffCallback : DiffUtil.ItemCallback<PhotoAttachmentsViewTypeObject>() {

    override fun areItemsTheSame(oldItem: PhotoAttachmentsViewTypeObject, newItem: PhotoAttachmentsViewTypeObject): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: PhotoAttachmentsViewTypeObject, newItem: PhotoAttachmentsViewTypeObject): Boolean {
        return oldItem.type == newItem.type && oldItem.imagePath == newItem.imagePath
    }
}
