package com.appetiser.baseplate.features.marketplace.searchfilters.location

sealed class MarketplaceLocationFilterState {
    object ShowLoading : MarketplaceLocationFilterState()

    object HideLoading : MarketplaceLocationFilterState()

    object Error : MarketplaceLocationFilterState()
}
