package com.appetiser.baseplate.features.marketplace.search

import com.appetiser.module.domain.models.marketplace.Product
import com.appetiser.module.domain.models.marketplace.SearchFilters

sealed class SearchState {
    data class DisplayRecentSearches(
        val recentSearches: List<String>
    ) : SearchState()

    data class ShowFilterCount(
        val filterCount: Int
    ) : SearchState()

    data class ShowFilterDialog(
        val searchFilters: SearchFilters
    ) : SearchState()

    data class ShowSearchResults(
        val searchResults: List<Product>
    ) : SearchState()

    object HideFilterCount : SearchState()

    object ShowFilterMenu : SearchState()

    object HideFilterMenu : SearchState()

    object Error : SearchState()
}
