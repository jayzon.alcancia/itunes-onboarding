package com.appetiser.baseplate.features.marketplace.search

import android.os.Bundle
import com.appetiser.baseplate.base.BaseViewModel
import com.appetiser.baseplate.utils.RECENT_SEARCHES_MAX_SIZE
import com.appetiser.module.data.features.marketplace.MarketplaceRepository
import com.appetiser.module.data.features.session.SessionRepository
import com.appetiser.module.domain.models.Paging
import com.appetiser.module.domain.models.Session
import com.appetiser.module.domain.models.marketplace.Product
import com.appetiser.module.domain.models.marketplace.SearchFilters
import com.appetiser.module.domain.utils.DEFAULT_SEARCH_FILTER_DISTANCE_IN_KM
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import javax.inject.Inject

class MarketplaceSearchViewModel @Inject constructor(
    private val sessionRepository: SessionRepository,
    private val marketplaceRepository: MarketplaceRepository
) : BaseViewModel() {

    private val _state by lazy {
        PublishSubject.create<SearchState>()
    }

    val state: Observable<SearchState> = _state

    private var searchFilters: SearchFilters = SearchFilters()
    private var keyword: String = ""

    override fun isFirstTimeUiCreate(bundle: Bundle?) = Unit

    fun loadSearchScreen() {
        sessionRepository
            .getSession()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribeBy(
                onSuccess = { session ->
                    if (session.recentSearches.isEmpty()) return@subscribeBy

                    _state.onNext(
                        SearchState.DisplayRecentSearches(
                            session.recentSearches
                        )
                    )
                },
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }

    fun search(keyword: String) {
        this.keyword = keyword

        sessionRepository
            .getSession()
            .flatMap { session ->
                saveRecentSearches(session, keyword)
            }
            .ignoreElement()
            .andThen(searchProducts(keyword))
            .doAfterSuccess {
                _state.onNext(SearchState.ShowFilterMenu)
            }
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribeBy(
                onSuccess = {
                    // TODO: 12/15/20 Replace with paging 3 lib.
                    _state.onNext(
                        SearchState.ShowSearchResults(it.list)
                    )
                },
                onError = {
                    Timber.e(it)
                    _state.onNext(
                        SearchState.Error
                    )
                }
            )
            .addTo(disposables)
    }

    private fun searchProducts(keyword: String): Single<Paging<Product>> {
        return marketplaceRepository
            .searchProducts(
                1,
                keyword,
                searchFilters
            )
    }

    private fun saveRecentSearches(session: Session, value: String): Single<Session> {
        session.recentSearches =
            session.recentSearches.toMutableList().apply {
                add(0, value)
            }

        // If recent searches exceeds max size. Remove excess.
        if (session.recentSearches.size > RECENT_SEARCHES_MAX_SIZE) {
            session.recentSearches =
                session.recentSearches.subList(0, RECENT_SEARCHES_MAX_SIZE)
        }

        return sessionRepository.saveSession(session)
    }

    fun onApplyFilters(searchFilters: SearchFilters) {
        this.searchFilters = searchFilters

        val filterCount = searchFilters.getFilterCount()

        if (filterCount > 0) {
            _state.onNext(
                SearchState.ShowFilterCount(filterCount)
            )
        } else {
            _state.onNext(
                SearchState.HideFilterCount
            )
        }

        // Call search again after applying filters.
        search(keyword)
    }

    fun onFilterClick() {
        _state.onNext(
            SearchState.ShowFilterDialog(searchFilters)
        )
    }

    fun onLocationResult(currentLatitude: Double, currentLongitude: Double) {
        Timber.tag("onLocationResult").d("$currentLatitude,$currentLongitude")

        searchFilters.currentLatitude = currentLatitude
        searchFilters.currentLongitude = currentLongitude

        // If distance is not yet set and location was obtained.
        // Set it to DEFAULT_SEARCH_FILTER_DISTANCE_IN_KM.
        if (searchFilters.distanceInKm == null) {
            searchFilters.distanceInKm = DEFAULT_SEARCH_FILTER_DISTANCE_IN_KM
        }
    }
}
