package com.appetiser.baseplate.di.builders

import com.appetiser.baseplate.di.scopes.ActivityScope
import com.appetiser.baseplate.features.account.about.AboutActivity
import com.appetiser.baseplate.features.account.AccountActivity
import com.appetiser.baseplate.features.account.about.WebViewActivity
import com.appetiser.baseplate.features.account.changeemail.newemail.ChangeEmailNewEmailActivity
import com.appetiser.baseplate.features.account.changeemail.verifycode.ChangeEmailVerifyCodeActivity
import com.appetiser.baseplate.features.account.changeemail.verifypassword.ChangeEmailVerifyPasswordActivity
import com.appetiser.baseplate.features.account.changepassword.currentpassword.ChangeCurrentPasswordActivity
import com.appetiser.baseplate.features.account.changepassword.newpassword.ChangeNewPasswordActivity
import com.appetiser.baseplate.features.account.changephone.newphone.ChangePhoneNewPhoneActivity
import com.appetiser.baseplate.features.account.changephone.verifycode.ChangePhoneVerifyCodeActivity
import com.appetiser.baseplate.features.account.changephone.verifypassword.ChangePhoneVerifyPasswordActivity
import com.appetiser.baseplate.features.account.deleteaccount.DeleteAccountPasswordActivity
import com.appetiser.baseplate.features.account.settings.AccountSettingsActivity
import com.appetiser.baseplate.features.auth.emailcheck.EmailCheckActivity
import com.appetiser.baseplate.features.auth.forgotpassword.ForgotPasswordActivity
import com.appetiser.baseplate.features.auth.forgotpassword.newpassword.NewPasswordActivity
import com.appetiser.baseplate.features.auth.forgotpassword.verification.ForgotPasswordVerificationActivity
import com.appetiser.baseplate.features.auth.landing.LandingActivity
import com.appetiser.baseplate.features.auth.login.LoginActivity
import com.appetiser.baseplate.features.auth.phonenumbercheck.PhoneNumberCheckActivity
import com.appetiser.baseplate.features.auth.register.phonenumber.PhoneNumberInputActivity
import com.appetiser.baseplate.features.auth.register.createpassword.CreatePasswordActivity
import com.appetiser.baseplate.features.auth.register.details.InputNameActivity
import com.appetiser.baseplate.features.auth.register.profile.UploadProfilePhotoActivity
import com.appetiser.baseplate.features.auth.register.verification.RegisterVerificationCodeActivity
import com.appetiser.baseplate.features.auth.subscription.SubscriptionActivity
import com.appetiser.baseplate.features.auth.walkthrough.WalkthroughActivity
import com.appetiser.baseplate.features.feed.create.CreateFeedActivity
import com.appetiser.baseplate.features.feed.details.FeedDetailsActivity
import com.appetiser.baseplate.features.main.MainActivity
import com.appetiser.baseplate.features.marketplace.checkout.MarketplaceCheckoutActivity
import com.appetiser.baseplate.features.marketplace.createitem.CreateMarketplaceItemActivity
import com.appetiser.baseplate.features.marketplace.details.MarketplaceProductDetailsActivity
import com.appetiser.baseplate.features.marketplace.search.MarketplaceSearchActivity
import com.appetiser.baseplate.features.marketplace.seller.profile.MarketplaceSellerProfileActivity
import com.appetiser.baseplate.features.photoviewer.PhotoViewerActivity
import com.appetiser.baseplate.features.profile.editprofile.EditProfileActivity
import com.appetiser.baseplate.features.report.feed.ReportFeedActivity
import com.appetiser.baseplate.features.report.user.ReportUserActivity
import com.appetiser.baseplate.features.searchlocation.SearchLocationActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeEmailCheckActivity(): EmailCheckActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeLoginActivity(): LoginActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeForgotPasswordActivity(): ForgotPasswordActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeRegisterVerificationActivity(): RegisterVerificationCodeActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeRegisterActivity(): PhoneNumberInputActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeForgotPasswordVerificationActivity(): ForgotPasswordVerificationActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeNewPasswordActivity(): NewPasswordActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeUserDetailsActivity(): InputNameActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeMainActivity(): MainActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeLandingActivity(): LandingActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeWalkthroughActivity(): WalkthroughActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeCreatePasswordActivity(): CreatePasswordActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeUploadProfilePhotoActivity(): UploadProfilePhotoActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeSubscriptionActivity(): SubscriptionActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributePhoneNumberCheckActivity(): PhoneNumberCheckActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeReportUserActivity(): ReportUserActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeReportFeedActivity(): ReportFeedActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeFeedDetailsActivity(): FeedDetailsActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeCreateFeedActivity(): CreateFeedActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeAccountActivity(): AccountActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeEditProfileActivity(): EditProfileActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeAccountSettingsActivity(): AccountSettingsActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeAboutActivity(): AboutActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeAboutContainerActivity(): WebViewActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeChangeCurrentPasswordActivity(): ChangeCurrentPasswordActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeChangeNewPasswordActivity(): ChangeNewPasswordActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeChangeEmailVerifyPasswordActivity(): ChangeEmailVerifyPasswordActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeChangeEmailNewEmailActivity(): ChangeEmailNewEmailActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeChangeEmailVerifyCodeActivity(): ChangeEmailVerifyCodeActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeChangePhoneVerifyPasswordActivity(): ChangePhoneVerifyPasswordActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeChangePhoneNewPhoneActivity(): ChangePhoneNewPhoneActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeChangePhoneVerifyCodeActivity(): ChangePhoneVerifyCodeActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeDeleteAccountPasswordActivity(): DeleteAccountPasswordActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeCreateMarketplaceItemActivity(): CreateMarketplaceItemActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeMarketplaceSearchActivity(): MarketplaceSearchActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeMarketplaceSellerProfileActivity(): MarketplaceSellerProfileActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeMarketplaceProductDetailsActivity(): MarketplaceProductDetailsActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeMarketplaceCheckoutActivity(): MarketplaceCheckoutActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributePhotoViewerActivity(): PhotoViewerActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeSearchLocationActivity(): SearchLocationActivity
}
