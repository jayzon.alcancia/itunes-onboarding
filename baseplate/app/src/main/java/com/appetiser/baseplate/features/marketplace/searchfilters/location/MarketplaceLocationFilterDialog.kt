package com.appetiser.baseplate.features.marketplace.searchfilters.location

import android.annotation.SuppressLint
import android.content.DialogInterface
import android.content.IntentSender
import android.os.Bundle
import android.os.Looper
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.launch
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProviders
import com.appetiser.baseplate.R
import com.appetiser.baseplate.ViewModelFactory
import com.appetiser.baseplate.databinding.BottomSheetLocationFilterBinding
import com.appetiser.baseplate.features.marketplace.searchfilters.BaseSearchFiltersBottomSheetDialog
import com.appetiser.baseplate.features.searchlocation.SearchLocationContract
import com.appetiser.module.common.ninjaTap
import com.appetiser.module.common.setVisible
import com.appetiser.module.common.showGenericErrorSnackBar
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.tbruyelle.rxpermissions2.RxPermissions
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import javax.inject.Inject

class MarketplaceLocationFilterDialog : BaseSearchFiltersBottomSheetDialog<BottomSheetLocationFilterBinding>() {

    private var fusedLocationClient: FusedLocationProviderClient? = null
    private var locationRequest: LocationRequest? = null

    @Inject
    lateinit var factory: ViewModelFactory<MarketplaceLocationFilterViewModel>

    private lateinit var viewModel: MarketplaceLocationFilterViewModel

    protected val rxPermissions: RxPermissions by lazy {
        RxPermissions(requireActivity())
    }

    companion object {
        private const val REQUEST_CODE_CHECK_SETTINGS = 1009
        private const val ARGS_PLACE_ID = "ARGS_PLACE_ID"
        private const val ARGS_PLACE_NAME = "ARGS_PLACE_NAME"
        private const val ARGS_CURRENT_LATITUDE = "ARGS_CURRENT_LATITUDE"
        private const val ARGS_CURRENT_LONGITUDE = "ARGS_CURRENT_LONGITUDE"

        fun newInstance(
            placeId: String?,
            placeName: String?,
            currentLatitude: Double?,
            currentLongitude: Double?
        ): MarketplaceLocationFilterDialog {
            return MarketplaceLocationFilterDialog().apply {
                arguments = Bundle().apply {
                    putString(ARGS_PLACE_ID, placeId)
                    putString(ARGS_PLACE_NAME, placeName)

                    if (currentLatitude != null) {
                        putDouble(ARGS_CURRENT_LATITUDE, currentLatitude)
                    }

                    if (currentLongitude != null) {
                        putDouble(ARGS_CURRENT_LONGITUDE, currentLongitude)
                    }
                }
            }
        }
    }

    private var placeId: String? = null
    private var placeName: String? = null
    private var currentLatitude: Double? = null
    private var currentLongitude: Double? = null

    private var locationSetListener: ((placeId: String?, placeName: String?, currentLatitude: Double?, currentLongitude: Double?) -> Unit)? = null
    private lateinit var getContent: ActivityResultLauncher<Unit>

    override fun isNested() = true

    override fun getLayoutId(): Int = R.layout.bottom_sheet_location_filter

    override fun onDialogCreated() {
        getContent = registerForActivityResult(SearchLocationContract()) { place ->
            if (place == null) return@registerForActivityResult // do nothing

            viewModel.setNewLocation(place)
        }

        viewModel = ViewModelProviders
            .of(this, factory)
            .get(MarketplaceLocationFilterViewModel::class.java)

        if (requireArguments().containsKey(ARGS_CURRENT_LATITUDE) && requireArguments().containsKey(ARGS_CURRENT_LONGITUDE)) {
            currentLatitude = requireArguments().getDouble(ARGS_CURRENT_LATITUDE)
            currentLongitude = requireArguments().getDouble(ARGS_CURRENT_LONGITUDE)
        }

        placeId = requireArguments().getString(ARGS_PLACE_ID)
        placeName = requireArguments().getString(ARGS_PLACE_NAME)

        setupViews()
        setupVmObservers()

        when {
            currentLatitude != null && currentLongitude != null && placeId.isNullOrEmpty() -> {
                loadCurrentLocationDetails()
            }
            !placeId.isNullOrEmpty() && !placeName.isNullOrEmpty() -> {
                binding.txtSelectedLocation.text = placeName
            }
            else -> {
                startLoadCurrentLocation()
            }
        }
    }

    private fun loadCurrentLocationDetails() {
        viewModel.loadCurrentLocationDetails(
            currentLatitude!!,
            currentLongitude!!
        )
    }

    private fun setupVmObservers() {
        viewModel
            .currentLocationDetails
            .observe(this) { currentLocationDetails ->
                binding.txtSelectedLocation.text = currentLocationDetails.name

                placeId = currentLocationDetails.placeId
                placeName = currentLocationDetails.name
                currentLatitude = currentLocationDetails.latitude
                currentLongitude = currentLocationDetails.longitude
            }

        viewModel
            .state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = ::handleState,
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }

    private fun handleState(state: MarketplaceLocationFilterState) {
        when (state) {
            MarketplaceLocationFilterState.ShowLoading -> {
                binding.txtSelectedLocation.text = ""
                binding.loading setVisible true
            }
            MarketplaceLocationFilterState.HideLoading -> {
                binding.loading setVisible false
            }
            MarketplaceLocationFilterState.Error -> {
                requireActivity().showGenericErrorSnackBar(
                    binding.root,
                    getString(R.string.generic_error_short)
                )
            }
        }
    }

    fun setOnLocationSetListener(
        callback: (placeId: String?, placeName: String?, currentLatitude: Double?, currentLongitude: Double?) -> Unit
    ) {
        locationSetListener = callback
    }

    private fun setupViews() {
        setupClickListeners()
    }

    override fun onDismiss(dialog: DialogInterface) {
        locationSetListener?.invoke(
            placeId,
            placeName,
            currentLatitude,
            currentLongitude
        )
        super.onDismiss(dialog)
    }

    private fun setupClickListeners() {
        binding
            .btnBack
            .ninjaTap {
                dismiss()
            }
            .addTo(disposables)

        binding
            .txtSearch
            .ninjaTap {
                navigateToSearchLocationActivity()
            }
            .addTo(disposables)

        binding
            .txtUseCurrentLocation
            .ninjaTap {
                startLoadCurrentLocation()
            }
            .addTo(disposables)

        binding
            .btnClear
            .ninjaTap {
                placeId = null
                placeName = null
                currentLatitude = null
                currentLongitude = null
                startLoadCurrentLocation()
            }
            .addTo(disposables)
    }

    private fun navigateToSearchLocationActivity() {
        getContent.launch()
    }

    private fun startLoadCurrentLocation() {
        rxPermissions
            .requestEachCombined(android.Manifest.permission.ACCESS_FINE_LOCATION)
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = { permission ->
                    when {
                        permission.granted -> {
                            loadCurrentLocation()
                        }
                        permission.shouldShowRequestPermissionRationale -> {
                            // TODO: 12/10/20 Show require permission message.
                        }
                        else -> {
                            // At least 1 permission was denied and marked "never ask again"
                            // User needs to go to settings
                            // TODO: 12/10/20 Show require permission message.
                        }
                    }
                },
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }

    private fun loadCurrentLocation() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireContext())

        locationRequest = LocationRequest().apply {
            interval = 10000
            fastestInterval = 5000
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }
        val settingsBuilder = LocationSettingsRequest.Builder()
            .addLocationRequest(locationRequest!!)

        val client = LocationServices.getSettingsClient(requireContext())
        val task = client.checkLocationSettings(settingsBuilder.build())

        task.addOnSuccessListener {
            initializeLocationRequest()
        }
        task.addOnFailureListener { exception ->
            if (exception is ResolvableApiException) {
                // Location settings are not satisfied,
                // but this can be fixed by showing the user dialog.
                try {
                    // Show the dialog by calling startResolutionForResult(),
                    // and check the result on onActivityResult().
                    exception.startResolutionForResult(
                        requireActivity(),
                        REQUEST_CODE_CHECK_SETTINGS
                    )
                } catch (ex: IntentSender.SendIntentException) {
                    // Ignore the error.
                }
            }
        }
    }

    @SuppressLint("MissingPermission")
    private fun initializeLocationRequest() {
        binding.loading.isVisible = true
        fusedLocationClient!!.requestLocationUpdates(
            locationRequest,
            object : LocationCallback() {
                override fun onLocationResult(locationResult: LocationResult?) {
                    locationResult?.lastLocation ?: return

                    // TODO: 12/10/20 Request backend for geolocation?
                    Timber
                        .tag("onLocationResult")
                        .d("${locationResult.lastLocation.latitude},${locationResult.lastLocation.longitude}")

                    currentLatitude = locationResult.lastLocation.latitude
                    currentLongitude = locationResult.lastLocation.longitude

                    loadCurrentLocationDetails()

                    fusedLocationClient!!.removeLocationUpdates(this)
                }
            },
            Looper.getMainLooper()
        )
    }
}
