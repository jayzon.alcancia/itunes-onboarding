package com.appetiser.baseplate.features.marketplace.searchfilters

import android.content.DialogInterface
import android.os.Bundle
import android.view.WindowManager
import com.appetiser.baseplate.R
import com.appetiser.baseplate.databinding.BottomSheetPriceFilterBinding
import com.appetiser.module.common.ninjaTap
import io.reactivex.rxkotlin.addTo
import java.text.DecimalFormat

class MarketplacePriceFilterDialog : BaseSearchFiltersBottomSheetDialog<BottomSheetPriceFilterBinding>() {

    companion object {
        private const val ARGS_MIN_PRICE = "ARGS_MIN_PRICE"
        private const val ARGS_MAX_PRICE = "ARGS_MAX_PRICE"

        fun newInstance(minPrice: Double?, maxPrice: Double?): MarketplacePriceFilterDialog {
            return MarketplacePriceFilterDialog().apply {
                arguments = Bundle().apply {
                    if (minPrice != null) {
                        putDouble(ARGS_MIN_PRICE, minPrice)
                    }

                    if (maxPrice != null) {
                        putDouble(ARGS_MAX_PRICE, maxPrice)
                    }
                }
            }
        }
    }

    private var priceSetListener: ((minPrice: Double?, maxPrice: Double?) -> Unit)? = null

    override fun getLayoutId(): Int = R.layout.bottom_sheet_price_filter

    override fun isNested(): Boolean = true

    private val decimalFormat by lazy {
        DecimalFormat("#.00")
    }

    private var minPrice: Double? = null
    private var maxPrice: Double? = null

    override fun onDialogCreated() {
        if (requireArguments().containsKey(ARGS_MIN_PRICE)) {
            minPrice = requireArguments().getDouble(ARGS_MIN_PRICE)
        }

        if (requireArguments().containsKey(ARGS_MAX_PRICE)) {
            maxPrice = requireArguments().getDouble(ARGS_MAX_PRICE)
        }

        setupViews()
    }

    fun setOnPriceSetListener(callback: (minPrice: Double?, maxPrice: Double?) -> Unit) {
        priceSetListener = callback
    }

    private fun setupViews() {
        setupClickListeners()

        if (minPrice != null) {
            decimalFormat.format(minPrice).apply {
                binding.inputPriceMin.setText(this)
                binding.inputPriceMin.setSelection(length)
            }
        }

        if (maxPrice != null) {
            decimalFormat.format(maxPrice).apply {
                binding.inputPriceMax.setText(this)
                binding.inputPriceMax.setSelection(length)
            }
        }

        if (shouldFocusOnOpen()) {
            binding.inputPriceMin.requestFocus()
        }

        // TODO: 12/3/20 Add price validation
    }

    private fun setupClickListeners() {
        binding
            .btnBack
            .ninjaTap {
                dismiss()
            }
            .addTo(disposables)

        binding
            .btnClear
            .ninjaTap {
                clearText()
            }
            .addTo(disposables)
    }

    private fun clearText() {
        binding.inputPriceMin.setText("")
        binding.inputPriceMax.setText("")

        minPrice = null
        maxPrice = null
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if (!shouldFocusOnOpen()) {
            // Do not show keyboard when not null.
            // This could mean user just wants to view the currently set price range.
            return
        }

        // Ignore deprecation for now.
        // Android's suggested solution doesn't work for showing keyboard.
        // Reference: https://stackoverflow.com/a/17238023/5285687
        dialog
            ?.window
            ?.setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE
            )
    }

    /**
     * Returns true if price input field should be focused upon showing dialog.
     */
    private fun shouldFocusOnOpen() = minPrice == null

    override fun onDismiss(dialog: DialogInterface) {
        val minPrice = try {
            binding.inputPriceMin.text.toString().toDouble()
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }

        val maxPrice = try {
            binding.inputPriceMax.text.toString().toDouble()
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }

        priceSetListener?.invoke(
            minPrice,
            maxPrice
        )

        super.onDismiss(dialog)
    }
}
