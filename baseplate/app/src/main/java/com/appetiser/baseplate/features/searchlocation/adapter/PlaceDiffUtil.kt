package com.appetiser.baseplate.features.searchlocation.adapter

import androidx.recyclerview.widget.DiffUtil
import com.appetiser.module.domain.models.places.SearchPlace

class PlaceDiffUtil : DiffUtil.ItemCallback<SearchPlace>() {
    override fun areItemsTheSame(oldItem: SearchPlace, newItem: SearchPlace): Boolean {
        return oldItem.placeId == newItem.placeId
    }

    override fun areContentsTheSame(oldItem: SearchPlace, newItem: SearchPlace): Boolean {
        return oldItem.name == newItem.name
    }
}
