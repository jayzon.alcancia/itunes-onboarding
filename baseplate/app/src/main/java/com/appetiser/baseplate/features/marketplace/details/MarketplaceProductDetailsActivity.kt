package com.appetiser.baseplate.features.marketplace.details

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.method.LinkMovementMethod
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SnapHelper
import com.appetiser.baseplate.R
import com.appetiser.baseplate.base.BaseViewModelActivity
import com.appetiser.baseplate.databinding.ActivityMarketplaceProductDetailsBinding
import com.appetiser.baseplate.ext.loadAvatarUrl
import com.appetiser.baseplate.ext.loadImageUrl
import com.appetiser.baseplate.features.marketplace.details.adapter.MarketplaceSimilarProductsAdapter
import com.appetiser.baseplate.features.marketplace.seller.profile.MarketplaceSellerProfileActivity
import com.appetiser.baseplate.features.photoviewer.PhotoViewerActivity
import com.appetiser.baseplate.widget.ItemSnapHelper
import com.appetiser.module.common.ninjaTap
import com.appetiser.module.common.widget.ExpandableTextView
import com.appetiser.module.domain.models.marketplace.Product
import io.reactivex.BackpressureStrategy
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter

class MarketplaceProductDetailsActivity : BaseViewModelActivity<ActivityMarketplaceProductDetailsBinding,
    MarketplaceProductDetailsViewModel>() {

    companion object {
        fun openActivity(context: Context, productId: Long) {
            val intent = Intent(
                context,
                MarketplaceProductDetailsActivity::class.java
            )
            intent.putExtra(KEY_PRODUCT_ID, productId)
            context.startActivity(intent)
        }

        const val KEY_PRODUCT_ID = "KEY_PRODUCT_ID"
    }

    private val adapter by lazy { MarketplaceSimilarProductsAdapter(listener) }

    private val formatter: DateTimeFormatter by lazy {
        DateTimeFormatter.ofPattern("MMM yyyy")
            .withZone(ZoneOffset.systemDefault())
    }

    private val listener = object : MarketplaceSimilarProductsAdapter.Listener {
        override fun onItemClicked(id: Long) {
            openActivity(this@MarketplaceProductDetailsActivity, id)
        }
    }

    override fun canBack(): Boolean {
        return true
    }

    override fun getLayoutId(): Int = R.layout.activity_marketplace_product_details

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.vm = viewModel
        setupViewModel()
        setupRecyclerView()

        viewModel.getProductDetails()

        binding.ivBack
            .ninjaTap {
                finish()
            }
            .addTo(disposables)

        binding.containerSellerProfile
            .ninjaTap {
                viewModel.showSellerProfileScreen()
            }
            .addTo(disposables)

        binding.containerImageProduct
            .ninjaTap {
                viewModel.showPhotoViewerScreen()
            }
            .addTo(disposables)
    }

    private fun setupViewModel() {
        viewModel.state
            .toFlowable(BackpressureStrategy.BUFFER)
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = {
                    handleState(it)
                }
            ).addTo(disposables)
    }

    private fun setupRecyclerView() {
        with(binding.recommendedProducts) {
            layoutManager = LinearLayoutManager(this@MarketplaceProductDetailsActivity, RecyclerView.HORIZONTAL, false)
            adapter = this@MarketplaceProductDetailsActivity.adapter

            val snapHelper: SnapHelper = ItemSnapHelper()
            snapHelper.attachToRecyclerView(this)
        }
    }

    private fun handleState(state: MarketplaceProductDetailsState) {
        when (state) {
            is MarketplaceProductDetailsState.GetProductDetails -> {
                handleProducts(state.product)
            }

            is MarketplaceProductDetailsState.GetSimilarProducts -> {
                adapter.updateItems(state.products)
            }
            is MarketplaceProductDetailsState.ShowProgressLoading -> {
            }
            is MarketplaceProductDetailsState.HideProgressLoading -> {
            }

            is MarketplaceProductDetailsState.ShowSellerProfileScreen -> {
                MarketplaceSellerProfileActivity.openActivity(this, state.id)
            }

            is MarketplaceProductDetailsState.ShowPhotoViewerScreen -> {
                PhotoViewerActivity.openActivity(this, ArrayList(state.photos), 0)
            }
        }
    }

    private fun handleProducts(product: Product) {
        binding.imgProduct.loadImageUrl(product.photos[0].url)
        binding.photoCount.text = getString(R.string.photo_count_format, 1, product.photos.size)

        binding.productName.text = product.title
        binding.productAddress.text = product.placesAddress
        binding.productPrice.text = product.formattedPrice

        binding.imgUserProfile.loadAvatarUrl(product.seller.avatarPermanentThumbUrl)
        binding.sellerName.text = product.seller.fullName

        binding.sellerRating.text = getString(R.string.member_since_format, formatter.format(product.seller.createdAt))
        binding.sellerProductDescription.text = product.description
        binding.sellerProductDescription.movementMethod = LinkMovementMethod.getInstance()
        binding.sellerProductDescription.setFullText(product.description)
        binding.sellerProductDescription.triggerTruncateText()

        binding.sellerProductDescription.setOnMoreClickListener(object : ExpandableTextView.OnClickMoreListener {
            override fun onClickMoreListener() {
                binding.sellerProductDescription.toggle()
            }
        })
    }
}
