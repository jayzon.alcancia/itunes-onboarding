package com.appetiser.baseplate.features.marketplace.searchfilters

import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.widget.FrameLayout
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.appetiser.baseplate.R
import com.appetiser.baseplate.di.Injectable
import com.appetiser.baseplate.utils.schedulers.BaseSchedulerProvider
import com.appetiser.module.common.getDeviceHeight
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

abstract class BaseSearchFiltersBottomSheetDialog<B : ViewDataBinding> : BottomSheetDialogFragment(), Injectable {

    @Inject
    lateinit var scheduler: BaseSchedulerProvider

    @LayoutRes
    abstract fun getLayoutId(): Int

    /**
     * Set to true when bottom sheet is a nested bottom sheet.
     * Meaning, a bottom sheet that is shown/created from another bottom sheet. Default is false.
     */
    open fun isNested(): Boolean = false

    lateinit var binding: B
    lateinit var disposables: CompositeDisposable

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val bottomSheet =
            if (isNested()) {
                BottomSheetDialog(
                    requireContext(),
                    R.style.Theme_Baseplate_BottomSheetDialog_Nested
                )
            } else {
                BottomSheetDialog(
                    requireContext(),
                    R.style.Theme_Design_BottomSheetDialog
                )
            }
        val dialogView =
            View.inflate(
                requireContext(),
                getLayoutId(),
                null
            )
        val deviceHeight = requireActivity().getDeviceHeight()

        binding = DataBindingUtil.bind<B>(dialogView)!!
        binding.lifecycleOwner = this

        bottomSheet.setContentView(dialogView)

        val bottomSheetBehavior = BottomSheetBehavior.from(dialogView.parent as View)
        bottomSheetBehavior.peekHeight = deviceHeight / 2
        bottomSheetBehavior.skipCollapsed = false

        val frameLayout = bottomSheet
            .findViewById<FrameLayout>(com.google.android.material.R.id.design_bottom_sheet)
        frameLayout!!.layoutParams.height = deviceHeight / 2

        bottomSheet.setOnShowListener {
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        }

        disposables = CompositeDisposable()

        onDialogCreated()

        return bottomSheet
    }

    abstract fun onDialogCreated()

    override fun onDestroyView() {
        disposables.clear()
        super.onDestroyView()
    }
}
