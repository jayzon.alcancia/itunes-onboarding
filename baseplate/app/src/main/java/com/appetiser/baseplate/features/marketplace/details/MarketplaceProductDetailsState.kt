package com.appetiser.baseplate.features.marketplace.details

import com.appetiser.module.domain.models.marketplace.Product

sealed class MarketplaceProductDetailsState {
    data class GetProductDetails(val product: Product) : MarketplaceProductDetailsState()
    data class GetSimilarProducts(val products: List<Product>) : MarketplaceProductDetailsState()
    object ShowProgressLoading : MarketplaceProductDetailsState()
    object HideProgressLoading : MarketplaceProductDetailsState()
    data class ShowSellerProfileScreen(val id: Long) : MarketplaceProductDetailsState()
    data class ShowPhotoViewerScreen(val photos: List<String>) : MarketplaceProductDetailsState()
    data class Error(val message: String) : MarketplaceProductDetailsState()
}
