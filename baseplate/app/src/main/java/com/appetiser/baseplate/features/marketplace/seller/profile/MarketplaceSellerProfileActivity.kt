package com.appetiser.baseplate.features.marketplace.seller.profile

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.appetiser.baseplate.R
import com.appetiser.baseplate.base.BaseViewModelActivity
import com.appetiser.baseplate.databinding.ActivityMarketplaceSellerProfileBinding
import com.appetiser.baseplate.ext.loadAvatarUrl
import com.appetiser.baseplate.features.marketplace.details.MarketplaceProductDetailsActivity
import com.appetiser.baseplate.features.marketplace.feed.MarketplaceFeedAdapter
import com.appetiser.baseplate.features.photoviewer.PhotoViewerActivity
import com.appetiser.module.common.ninjaTap
import com.appetiser.module.common.showGenericErrorSnackBar
import com.appetiser.module.domain.models.user.User
import io.reactivex.BackpressureStrategy
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter

class MarketplaceSellerProfileActivity : BaseViewModelActivity<ActivityMarketplaceSellerProfileBinding, MarketplaceSellerProfileViewModel>() {

    companion object {

        fun openActivity(context: Context, sellerProfileId: Long) {
            val intent = Intent(
                context,
                MarketplaceSellerProfileActivity::class.java
            )
            intent.putExtra(KEY_PROFILE_ID, sellerProfileId)
            context.startActivity(intent)
        }

        const val KEY_PROFILE_ID = "KEY_PROFILE_ID"
    }

    private val formatter: DateTimeFormatter by lazy {
        DateTimeFormatter.ofPattern("MMM yyyy")
            .withZone(ZoneOffset.systemDefault())
    }

    override fun getLayoutId() = R.layout.activity_marketplace_seller_profile

    override fun canBack() = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.vm = viewModel

        setupToolbar()
        setupViews()
        setupViewModel()

        binding.avatar
            .ninjaTap {
                viewModel.showPhotoViewerScreen()
            }
            .addTo(disposables)
    }

    override fun onResume() {
        super.onResume()
        viewModel.getSellerDetails()
    }

    private val adapter by lazy { MarketplaceFeedAdapter(listener) }

    private val listener = object : MarketplaceFeedAdapter.Listener {
        override fun onItemClicked(id: Long) {
            MarketplaceProductDetailsActivity.openActivity(this@MarketplaceSellerProfileActivity, id)
        }
    }

    private fun setupToolbar() {
        setToolbarTitle(
            getString(R.string.seller_profile)
        )
    }

    private fun setupViews() {
        with(binding.feed) {
            layoutManager = LinearLayoutManager(this@MarketplaceSellerProfileActivity, RecyclerView.VERTICAL, false)
            adapter = this@MarketplaceSellerProfileActivity.adapter
        }
    }

    private fun setupViewModel() {
        viewModel.state
            .toFlowable(BackpressureStrategy.BUFFER)
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = { state ->
                    when (state) {
                        is MarketplaceSellerProfileState.GetProducts -> {
                            adapter.submitData(lifecycle, state.products)
                        }
                        is MarketplaceSellerProfileState.Error -> {
                            showGenericErrorSnackBar(
                                binding.root,
                                state.message
                            )
                        }

                        is MarketplaceSellerProfileState.ShowPhotoViewerScreen -> {
                            PhotoViewerActivity.openActivity(this, ArrayList(state.photos), 0)
                        }
                    }
                }
            ).addTo(disposables)

        viewModel.seller.observe(
            this,
            Observer {
                handleSellerProfile(it)
            }
        )
    }

    private fun handleSellerProfile(seller: User) {
        binding.avatar.loadAvatarUrl(seller.avatarPermanentUrl)
        binding.sellersName.text = seller.fullName
        binding.sellerInfo.text = getString(R.string.member_since_format, formatter.format(seller.createdAt))
    }
}
