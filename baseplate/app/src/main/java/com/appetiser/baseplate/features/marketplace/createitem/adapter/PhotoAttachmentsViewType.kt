package com.appetiser.baseplate.features.marketplace.createitem.adapter

data class PhotoAttachmentsViewTypeObject(val type: PhotoAttachmentsViewType, val imagePath: String)

enum class PhotoAttachmentsViewType(val type: Int) {
    DISPLAY_ADD_BTN(0), DISPLAY_PHOTO(1)
}
