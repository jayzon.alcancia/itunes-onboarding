package com.appetiser.module.payouts.displaypayoutdetails

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import com.appetiser.module.common.ninjaTap
import com.appetiser.module.payouts.PayoutsActivity
import com.appetiser.module.payouts.PayoutsState
import com.appetiser.module.payouts.R
import com.appetiser.module.payouts.editpayoutdetails.EditPayoutDetailsFragment
import dagger.android.support.AndroidSupportInjection
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber

class BankDetailsFragment : Fragment() {

    private lateinit var disposables: CompositeDisposable

    private val viewModel by lazy {
        (requireActivity() as PayoutsActivity).viewModel
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_bank_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpViews()
    }

    override fun onResume() {
        super.onResume()
        startObservers()
    }

    override fun onStop() {
        super.onStop()
        disposables.clear()
    }

    private fun setUpViews() {
        requireActivity().run {
            findViewById<TextView>(R.id.toolbarButton).visibility = View.GONE

            findViewById<Button>(R.id.editPayoutDetailsButton)
                .ninjaTap {
                    requireActivity().supportFragmentManager.commit {
                        addToBackStack(null)
                        replace(R.id.fragmentContainer, EditPayoutDetailsFragment())
                    }
                }

            findViewById<TextView>(R.id.removeAccountButton).ninjaTap {
                viewModel.deleteAccount()
            }
        }
    }

    private fun startObservers() {
        disposables = CompositeDisposable()

        viewModel
            .state
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onNext = ::handleState,
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }

    private fun handleState(state: PayoutsState) {
        when (state) {
            is PayoutsState.ShowPayoutDetails -> {
                requireView().run {
                    findViewById<TextView>(R.id.accountHolderNameValue).text =
                        state.details.bankDetails?.accountHolderName
                    findViewById<TextView>(R.id.bsbValue).text =
                        state.details.bankDetails?.bsb
                    findViewById<TextView>(R.id.accountNumberValue).text =
                        state.details.bankDetails?.accountNumber
                }
            }

            PayoutsState.GoBackToSettingsScreen -> {
                Toast.makeText(
                    requireContext(),
                    getString(R.string.payout_details_deleted),
                    Toast.LENGTH_LONG
                ).show()
                requireActivity().finish()
            }
        }
    }
}