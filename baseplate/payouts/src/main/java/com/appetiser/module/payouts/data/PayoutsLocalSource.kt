package com.appetiser.module.payouts.data

import android.content.SharedPreferences
import javax.inject.Inject

class PayoutsLocalSource @Inject constructor(
    private val sharedPreferences: SharedPreferences
) {
    companion object {
        private const val PREF_PAYOUTS_ENABLED = "PREF_PAYOUTS_ENABLED"
    }

    fun enablePayouts() {
        sharedPreferences
            .edit()
            .apply {
                putBoolean(PREF_PAYOUTS_ENABLED, true)
                apply()
            }
    }

    fun disablePayouts() {
        sharedPreferences
            .edit()
            .apply {
                putBoolean(PREF_PAYOUTS_ENABLED, false)
                apply()
            }
    }

    fun isPayoutsEnabled(): Boolean {
        return sharedPreferences.getBoolean(PREF_PAYOUTS_ENABLED, false)
    }
}