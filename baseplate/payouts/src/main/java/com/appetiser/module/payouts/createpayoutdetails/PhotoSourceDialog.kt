package com.appetiser.module.payouts.createpayoutdetails

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.appetiser.module.common.ninjaTap
import com.appetiser.module.payouts.ActivityRequestCodes.REQUEST_GALLERY
import com.appetiser.module.payouts.R
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.tbruyelle.rxpermissions2.RxPermissions

class PhotoSourceDialog : BottomSheetDialogFragment() {

    // Parent fragment is needed to propagate onActivityResult properly from Camera
    val camera: Camera by lazy { Camera(requireParentFragment()) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.dialog_photo_source, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.findViewById<Button>(R.id.btnTakePhoto).ninjaTap {
            this.dismiss()
            camera.takePhoto()
        }

        view.findViewById<Button>(R.id.btnGallery).ninjaTap {
            this.dismiss()
            Intent(Intent.ACTION_OPEN_DOCUMENT).apply {
                addCategory(Intent.CATEGORY_OPENABLE)
                type = "image/*"
            }.let {
                requireParentFragment().startActivityForResult(
                    Intent.createChooser(
                        it,
                        getString(R.string.select_picture)
                    ), REQUEST_GALLERY
                )
            }
        }

        view.findViewById<Button>(R.id.btnCancel).ninjaTap {
            this.dismiss()
        }
    }
}
































