package com.appetiser.module.notification.fcm

import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import io.reactivex.disposables.CompositeDisposable
import timber.log.Timber

abstract class BaseplateFirebaseMessagingService() : FirebaseMessagingService() {

    lateinit var disposables: CompositeDisposable

    override fun onCreate() {
        super.onCreate()
        disposables = CompositeDisposable()
    }

    override fun onDestroy() {
        disposables.clear()
        super.onDestroy()
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        val notification = remoteMessage.notification

        if (notification != null) {
            // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
            Timber.d("From: %s", remoteMessage.from)
            Timber.d("Message Notification Title: %s", remoteMessage.notification?.title)
            Timber.d("Message Notification Body: %s", remoteMessage.notification?.body)
            // TODO uncomment if you want to use `data
            val data = remoteMessage.data

            Timber.d("Message Notification Body: %s", remoteMessage.notification?.body)
            onMessageReceived(data)
        }
    }

    override fun onNewToken(token: String) {
        Timber.d("Refreshed token: %s", token)
    }

    abstract fun onMessageReceived(data: Map<String, String>)
}
