package com.appetiser.module.domain.models.marketplace

import java.time.Instant

data class Seller(
    val id: Long = 0,
    val fullName: String = "",
    val description: String = "",
    val birthdate: String = "",
    val gender: String = "",
    val email: String = "",
    val phoneNumber: String = "",
    val createdAt: Instant = Instant.now(),
    val updatedAt: Instant = Instant.now(),
    val blockedAt: Instant = Instant.now(),
    val emailVerified: Boolean = false,
    val phoneNumberVerified: Boolean = false,
    val verified: Boolean = false,
    val avatarPermanentUrl: String = "",
    val avatarPermanentThumbUrl: String = "",
    val mine: Boolean = false
)
