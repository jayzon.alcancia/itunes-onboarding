package com.appetiser.module.domain.models.marketplace

data class ProductCategory(
    val id: Long = 0,
    val label: String = "",
    val type: String = ""
)