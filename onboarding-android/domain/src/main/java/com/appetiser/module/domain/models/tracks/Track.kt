package com.appetiser.module.domain.models.tracks

import com.google.gson.annotations.SerializedName

data class Track(
    @SerializedName("trackId")
    val id: Int,
    val artistName: String,
    val trackName: String,
    val artworkUrl30: String,
    val artworkUrl60: String,
    val artworkUrl100: String,
    val trackPrice: Double,
    @SerializedName("primaryGenreName")
    val genre: String,
    val longDescription: String
)


