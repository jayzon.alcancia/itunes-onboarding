package com.appetiser.module.domain.models.feed

import com.appetiser.module.domain.models.user.MediaFile
import com.appetiser.module.domain.models.user.User

data class Feed(
    val id: Long = 0,
    val body: String = "",
    val authorId: Long = 0,
    val createdAt: String = "",
    val updatedAt: String = "",
    val author: User = User.empty(),
    val photo: MediaFile = MediaFile.empty(),

    // additional fields
    var favoritesCount: Long = 0,
    var commentsCount: Long = 0,
    var isFavorite: Boolean = false,
    var isUserFeedOwner: Boolean = false
)