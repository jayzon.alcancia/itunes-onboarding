package com.appetiser.module.domain.models.marketplace

import java.math.BigDecimal
import java.time.Instant

data class Product(
    val id: Long,
    val title: String,
    val description: String,
    val price: BigDecimal,
    val formattedPrice: String,
    val currency: String,
    val categoryId: Long,
    val placesId: String,
    val placesAddress: String,
    val createdAt: Instant,
    val updatedAt: Instant,
    val deletedAt: Instant?,
    val photos: List<ProductPhoto>,
    val seller: Seller
)