package com.appetiser.module.data.features.notification

import com.appetiser.module.domain.models.notification.Notification
import io.reactivex.Observable
import io.reactivex.Single

interface NotificationRepository {

    fun registerDeviceToken(
        deviceToken: String,
        deviceId: String
    ): Single<Boolean>

    fun unRegisterDeviceToken(
        deviceToken: String,
        deviceId: String
    ): Single<Boolean>

    fun getNotifications(type: String): Observable<List<Notification>>
}
