package com.appetiser.module.data.features.tracks

import com.appetiser.module.domain.models.tracks.Track
import com.appetiser.module.local.features.track.models.TrackDB
import com.appetiser.module.network.features.track.model.response.TrackResponse
import io.reactivex.Single

interface TracksRepository {

    fun getAllItunesTracks(): Single<TrackResponse<Track>>

    fun saveAllItunesTracksLocally(list: List<TrackDB>)

    fun getAllItunesTracksLocally(): Single<List<TrackDB>>

    fun saveTrackToCache(trackLocal: TrackDB)

    fun getTrackFromCache(): TrackDB?

    fun clearTrack()

}