package com.appetiser.module.data.features.marketplace

import com.appetiser.module.domain.models.Paging
import com.appetiser.module.domain.models.marketplace.Product
import com.appetiser.module.domain.models.marketplace.ProductCategory
import com.appetiser.module.domain.models.marketplace.SearchFilters
import com.appetiser.module.local.features.session.SessionLocalSource
import com.appetiser.module.network.features.marketplace.MarketplaceRemoteSource
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

class MarketplaceRepositoryImpl @Inject constructor(
    private val sessionLocalSource: SessionLocalSource,
    private val marketplaceRemoteSource: MarketplaceRemoteSource
) : MarketplaceRepository {

    override fun postProducts(
        title: String,
        description: String,
        price: String,
        photos: List<String>?,
        placesId: String?,
        categoryId: Long?
    ): Single<Product> {
        return sessionLocalSource
            .getSession()
            .flatMap { session ->
                marketplaceRemoteSource
                    .postProducts(
                        session.accessToken,
                        title,
                        description,
                        price,
                        photos,
                        placesId,
                        categoryId
                    )
            }
    }

    override fun getProductCategories(): Observable<List<ProductCategory>> {
        return sessionLocalSource
            .getSession()
            .flatMapObservable { session ->
                marketplaceRemoteSource
                    .getProductCategories(session.accessToken)
            }
    }

    override fun getProductDetails(productId: Long): Single<Product> {
        return sessionLocalSource
            .getSession()
            .flatMap { session ->
                marketplaceRemoteSource.getProductDetails(session.accessToken, productId)
            }
    }

    override fun getSimilarProducts(productId: Long): Observable<List<Product>> {
        return sessionLocalSource
            .getSession()
            .flatMapObservable { session ->
                marketplaceRemoteSource.getSimilarProducts(session.accessToken, productId)
            }
    }

    override fun getProducts(page: Int): Single<Paging<Product>> {
        return sessionLocalSource
            .getSession()
            .flatMap { session ->
                marketplaceRemoteSource.getProducts(session.accessToken, page)
            }
    }

    override fun getSellerProducts(page: Int, profileId: Long): Single<Paging<Product>> {
        return sessionLocalSource
            .getSession()
            .flatMap { session ->
                marketplaceRemoteSource.getSellerProducts(session.accessToken, page, profileId)
            }
    }

    override fun searchProducts(
        page: Int,
        keyword: String,
        searchFilters: SearchFilters
    ): Single<Paging<Product>> {
        return sessionLocalSource
            .getSession()
            .flatMap { session ->
                marketplaceRemoteSource
                    .searchProducts(
                        session.accessToken,
                        page,
                        keyword,
                        searchFilters
                    )
            }
    }
}
