package com.appetiser.module.data.features.tracks

import com.appetiser.module.domain.models.tracks.Track
import com.appetiser.module.local.features.track.TracksLocalSource
import com.appetiser.module.local.features.track.models.TrackDB
import com.appetiser.module.network.features.track.TrackRemoteSource
import javax.inject.Inject

class TracksRepositoryImpl @Inject constructor(
    private val tracksRemoteSource: TrackRemoteSource,
    private val tracksLocalSource: TracksLocalSource
) : TracksRepository {
    override fun getAllItunesTracks() = tracksRemoteSource.getAllTrack()

    override fun saveAllItunesTracksLocally(list: List<TrackDB>) {
        tracksLocalSource.saveAllItunesTracksLocally(list)
    }

    override fun getAllItunesTracksLocally() = tracksLocalSource.getAllItunesTracksLocally()

    override fun saveTrackToCache(trackLocal: TrackDB) {
       tracksLocalSource.saveTrackToCache(trackLocal)
    }

    override fun getTrackFromCache() = tracksLocalSource.getTrackFromCache()

    override fun clearTrack()  {
        tracksLocalSource.clearTrack()
    }
}