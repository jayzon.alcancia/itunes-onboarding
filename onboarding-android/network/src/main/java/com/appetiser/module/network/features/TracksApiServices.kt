package com.appetiser.module.network.features

import com.appetiser.module.domain.models.tracks.Track
import com.appetiser.module.network.features.track.model.response.TrackResponse
import io.reactivex.Single
import retrofit2.http.GET

interface TracksApiServices {

    @GET("search?term=star&country=au&media=movie&all")
    fun getAllTracks(): Single<TrackResponse<Track>>

}