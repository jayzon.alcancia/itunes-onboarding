package com.appetiser.module.network.features.track

import com.appetiser.module.domain.models.tracks.Track
import com.appetiser.module.network.features.track.model.response.TrackResponse
import io.reactivex.Single

interface TrackRemoteSource {
    fun getAllTrack(): Single<TrackResponse<Track>>
}