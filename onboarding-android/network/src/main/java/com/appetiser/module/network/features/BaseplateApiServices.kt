package com.appetiser.module.network.features

import com.appetiser.module.domain.models.feed.Feed
import com.appetiser.module.domain.models.marketplace.SearchFilters
import com.appetiser.module.domain.models.tracks.Track
import com.appetiser.module.network.base.response.BasePagedResponse
import com.appetiser.module.network.base.response.BaseResponse
import com.appetiser.module.network.base.response.BaseResponseNew
import com.appetiser.module.network.features.auth.models.response.AuthDataResponse
import com.appetiser.module.network.features.auth.models.response.CountryCodeResponse
import com.appetiser.module.network.features.auth.models.response.EmailCheckDataResponse
import com.appetiser.module.network.features.auth.models.response.GetVerificationTokenResponse
import com.appetiser.module.network.features.auth.models.response.VerifyEmailResponse
import com.appetiser.module.network.features.comment.models.CommentDTO
import com.appetiser.module.network.features.comment.models.response.CommentResponse
import com.appetiser.module.network.features.comment.models.response.CommentsResponse
import com.appetiser.module.network.features.feed.models.FeedDTO
import com.appetiser.module.network.features.feed.models.response.FeedResponse
import com.appetiser.module.network.features.marketplace.model.ProductCategoryDTO
import com.appetiser.module.network.features.marketplace.model.ProductDTO
import com.appetiser.module.network.features.miscellaneous.models.ReportCategoryDTO
import com.appetiser.module.network.features.miscellaneous.models.ReportedUserDTO
import com.appetiser.module.network.features.notification.models.response.NotificationsReponse
import com.appetiser.module.network.features.payment.models.EphemeralKey
import com.appetiser.module.network.features.payouts.ExternalAccountRequest
import com.appetiser.module.network.features.payouts.GetPayoutDetailsResponse
import com.appetiser.module.network.features.payouts.IdVerificationResponse
import com.appetiser.module.network.features.payouts.PayoutDetailsRequest
import com.appetiser.module.network.features.places.models.PlaceDTO
import com.appetiser.module.network.features.profile.models.response.ProfileDataResponse
import com.appetiser.module.network.features.track.model.response.TrackResponse
import com.appetiser.module.network.features.user.models.MediaFileDTO
import com.appetiser.module.network.features.user.models.UserDTO
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*

interface BaseplateApiServices {

    @POST("auth/register")
    fun register(@Body registerBody: RequestBody): Single<AuthDataResponse>

    @FormUrlEncoded
    @POST("auth/login")
    fun login(@FieldMap fields: Map<String, @JvmSuppressWildcards Any>): Single<AuthDataResponse>

    @FormUrlEncoded
    @POST("auth/check-username")
    fun checkUsernameIfExists(@FieldMap fields: Map<String, @JvmSuppressWildcards Any>): Single<EmailCheckDataResponse>

    @POST("auth/verification/verify")
    fun verifyAccount(
        @Header("Authorization") token: String,
        @Body registerBody: RequestBody
    ): Single<VerifyEmailResponse>

    @GET("auth/connect/account")
    fun getPayoutDetails(@Header("Authorization") token: String): Single<GetPayoutDetailsResponse>

    @PUT("auth/connect/external-accounts/{id}")
    fun updateExternalAccount(
        @Header("Authorization") token: String,
        @Path("id") id: String,
        @Body request: ExternalAccountRequest
    ): Completable

    @POST("auth/connect/account")
    fun postPayoutDetails(
        @Header("Authorization") token: String,
        @Body request: PayoutDetailsRequest
    ): Completable

    @DELETE("auth/connect/account")
    fun deletePayoutDetails(
        @Header("Authorization") token: String
    ): Completable

    @Multipart
    @POST("auth/connect/file-upload")
    fun postIdVerification(
        @Header("Authorization") token: String,
        @Part("purpose") purpose: String = "identity_document",
        @Part file: MultipartBody.Part
    ): Single<IdVerificationResponse>

    @POST("auth/verification/resend")
    fun resendVerificationCode(
        @Header("Authorization") token: String,
        @Body registerBody: RequestBody
    ): Single<BaseResponse>

    @FormUrlEncoded
    @POST("auth/social")
    fun socialLogin(@FieldMap fields: Map<String, @JvmSuppressWildcards Any>): Single<AuthDataResponse>

    @GET("auth/me")
    fun getUser(
        @Header("Authorization") token: String
    ): Single<BaseResponseNew<UserDTO>>

    @GET("users/{id}")
    fun getUserById(
        @Header("Authorization") token: String,
        @Path("id") userId: Long
    ): Single<BaseResponseNew<UserDTO>>

    @PUT("auth/profile")
    fun updateUserInfo(
        @Header("Authorization") token: String,
        @Body registerBody: RequestBody
    ): Single<ProfileDataResponse>

    @POST("auth/logout")
    fun logout(@Header("Authorization") token: String): Completable

    @DELETE("auth/account")
    fun deleteAccount(
        @Header("Authorization") token: String,
        @Header("Verification-Token") verificationToken: String
    ): Completable

    @POST("auth/reset-password")
    fun resetPassword(
        @Body resetPasswordJson: RequestBody
    ): Single<BaseResponse>

    @POST("auth/forgot-password")
    fun forgotPassword(@Body emailJson: RequestBody): Single<BaseResponse>

    @POST("auth/reset-password/check")
    fun forgotPasswordCheckCode(@Body requestBody: RequestBody): Single<BaseResponse>

    @POST("auth/change/password")
    fun changePassword(
        @Header("Authorization") token: String,
        @Body requestBody: RequestBody
    ): Single<BaseResponse>

    @Multipart
    @POST("auth/profile/avatar")
    fun uploadPhoto(
        @Header("Authorization") token: String,
        @Part avatar: MultipartBody.Part? = null
    ): Single<BaseResponseNew<MediaFileDTO>>

    @POST("auth/account/verification-token")
    fun getVerificationToken(
        @Header("Authorization") token: String,
        @Body requestBody: RequestBody
    ): Single<BaseResponseNew<GetVerificationTokenResponse>>

    @POST("auth/change/email")
    fun requestChangeEmail(
        @Header("Authorization") token: String,
        @Header("Verification-Token") verificationToken: String,
        @Body requestBody: RequestBody
    ): Completable

    @POST("auth/change/email/verify")
    fun verifyChangeEmail(
        @Header("Authorization") token: String,
        @Header("Verification-Token") verificationToken: String,
        @Body requestBody: RequestBody
    ): Completable

    @POST("auth/change/phone-number")
    fun requestChangePhone(
        @Header("Authorization") token: String,
        @Header("Verification-Token") verificationToken: String,
        @Body requestBody: RequestBody
    ): Completable

    @POST("auth/change/phone-number/verify")
    fun verifyChangePhone(
        @Header("Authorization") token: String,
        @Header("Verification-Token") verificationToken: String,
        @Body requestBody: RequestBody
    ): Completable

    @POST("auth/ephemeral-key")
    fun createEphemeralKey(
        @Header("Authorization") token: String
    ): Single<BaseResponseNew<EphemeralKey>>

    /**
     *
     *  getCountryCodes(@Body body: RequestBody)
     *
     *  sample request body
     *   {
     *   "country_ids": [
     *       "608"
     *       ]
     *   }
     *
     * */
    @POST("countries")
    fun getCountryCodes(@Body body: RequestBody): Single<CountryCodeResponse>

    @POST("countries")
    fun getAllCountryCodes(): Single<CountryCodeResponse>

    @POST("devices")
    fun registerToken(
        @Header("Authorization") token: String,
        @Body body: RequestBody
    ): Single<BaseResponse>

    // https://stackoverflow.com/questions/41509195/how-to-send-a-http-delete-with-a-body-in-retrofit
    @HTTP(method = "DELETE", path = "devices", hasBody = true)
    fun unRegisterToken(
        @Header("Authorization") token: String,
        @Body body: RequestBody
    ): Single<BaseResponse>

    @GET("report/categories")
    fun getReportCategories(
        @Header("Authorization") token: String
    ): Single<BaseResponseNew<List<ReportCategoryDTO>>>

    @Multipart
    @POST("users/{id}/report")
    fun reportUser(
        @Header("Authorization") token: String,
        @Path("id") reportedUserId: String,
        @Query("reason_id") reasonId: String,
        @Query("description") description: String,
        @Part attachments: MutableList<MultipartBody.Part>
    ): Single<BaseResponseNew<ReportedUserDTO>>

    @POST("users/{id}/report")
    fun reportUser(
        @Header("Authorization") token: String,
        @Path("id") reportedUserId: String,
        @Query("reason_id") reasonId: String,
        @Query("description") description: String
    ): Single<BaseResponseNew<ReportedUserDTO>>

    @Multipart
    @POST("posts/{id}/report")
    fun reportFeed(
        @Header("Authorization") token: String,
        @Path("id") reportedUserId: Long,
        @Query("reason_id") reasonId: Long,
        @Query("description") description: String,
        @Part attachments: MutableList<MultipartBody.Part>
    ): Single<BaseResponse>

    @Multipart
    @POST("posts/{id}/report")
    fun reportFeed(
        @Header("Authorization") token: String,
        @Path("id") reportedUserId: Long,
        @Query("reason_id") reasonId: Long,
        @Query("description") description: String
    ): Single<BaseResponse>

    @GET("notifications/{type}")
    fun getNotifications(
        @Header("Authorization") token: String,
        @Path("type") type: String,
        @Query("include") query: String
    ): Observable<NotificationsReponse>

    @GET("posts")
    fun getFeeds(
        @Header("Authorization") token: String,
        @QueryMap map: Map<String, @JvmSuppressWildcards Any>
    ): Single<BasePagedResponse<FeedDTO>>

    @GET("posts/{id}")
    fun getFeedDetails(
        @Header("Authorization") token: String,
        @Path("id") postId: Long
    ): Observable<FeedResponse>

    @POST("posts/{id}/comments")
    fun postComment(
        @Header("Authorization") token: String,
        @Path("id") postId: Long,
        @Body body: RequestBody
    ): Single<BaseResponseNew<CommentDTO>>

    @DELETE("posts/{id}")
    fun deleteAccount(
        @Header("Authorization") token: String,
        @Path("id") postId: Long
    ): Single<BaseResponse>

    @Multipart
    @POST("posts")
    fun postFeed(
        @Header("Authorization") token: String,
        @Part image: MultipartBody.Part,
        @Part("body") body: RequestBody
    ): Single<BaseResponseNew<Feed>>

    @POST("posts/{id}/favorite")
    fun markFavorite(
        @Header("Authorization") token: String,
        @Path("id") id: Long
    ): Single<BaseResponse>

    @POST("posts/{id}/unfavorite")
    fun unMarkFavorite(
        @Header("Authorization") token: String,
        @Path("id") id: Long
    ): Single<BaseResponse>

    @GET("posts/{id}/comments")
    fun getComments(
        @Header("Authorization") token: String,
        @Path("id") postId: Long,
        @QueryMap map: Map<String, @JvmSuppressWildcards Any>
    ): Single<CommentsResponse>

    @GET("comments/{id}")
    fun getComment(
        @Header("Authorization") token: String,
        @Path("id") postId: Long
    ): Observable<CommentResponse>

    @Multipart
    @POST("marketplace/products")
    fun postProducts(
        @Header("Authorization") token: String,
        @PartMap map: Map<String, @JvmSuppressWildcards RequestBody>,
        @Part attachments: MutableList<MultipartBody.Part>? = null
    ): Single<BaseResponseNew<ProductDTO>>

    @GET("marketplace/products/categories")
    fun getProductCategories(
        @Header("Authorization") token: String
    ): Observable<BaseResponseNew<List<ProductCategoryDTO>>>

    @GET("places")
    fun getPlaces(
        @Header("Authorization") token: String,
        @Query("input") input: String
    ): Single<BaseResponseNew<List<PlaceDTO>>>

    @GET("places/nearby")
    fun getNearbyPlaces(
        @Header("Authorization") token: String,
        @Query("lat") latitude: Double,
        @Query("lng") longitude: Double
    ): Single<BaseResponseNew<List<PlaceDTO>>>

    @GET("places/{places-id}")
    fun getPlaceDetails(
        @Header("Authorization") token: String,
        @Path("places-id") placesId: String
    ): Single<BaseResponseNew<PlaceDTO>>

    @GET("marketplace/products")
    fun getProducts(
        @Header("Authorization") token: String,
        @Query("page") page: Int,
        @Query("include[]") include: String = "photos"
    ): Single<BasePagedResponse<ProductDTO>>

    /**
     * @param page current page for pagination
     * @param keyword searched keyword
     * @param minMaxPrice comma separated values of the price range "20,50". where "20" is the minimum price and "50" is the max price.
     * @param placesId google places id when user filters by specific location
     * @param withinDistanceTo comma separated values of user specified lat/lng and distance set from that location
     * @param sort either [SearchFilters.SORT_TYPE_TITLE], [SearchFilters.SORT_TYPE_PRICE_HIGH_TO_LOW], [SearchFilters.SORT_TYPE_PRICE_LOW_TO_HIGH].
     */
    @GET("marketplace/products")
    fun searchProducts(
        @Header("Authorization") token: String,
        @Query("page") page: Int,
        @Query("include[]") include: String = "photos",
        @Query("filter[search]") keyword: String,
        @Query("filter[price_range]") minMaxPrice: String?,
        @Query("filter[places_id]") placesId: String?,
        @Query("filter[within_distance_to]") withinDistanceTo: String?,
        @Query("sort") sort: String
    ): Single<BasePagedResponse<ProductDTO>>

    @GET("marketplace/products")
    fun getSellerProducts(
        @Header("Authorization") token: String,
        @Query("page") page: Int,
        @Query("filter[seller_id]") sellerId: Long,
        @Query("include[]") include: String = "photos"
    ): Single<BasePagedResponse<ProductDTO>>

    @GET("marketplace/products/{productId}")
    fun getProductsDetails(
        @Header("Authorization") token: String,
        @Path("productId") productId: Long
    ): Single<BaseResponseNew<ProductDTO>>

    @GET("marketplace/products/{productId}/similar")
    fun getSimilarProducts(
        @Header("Authorization") token: String,
        @Path("productId") productId: Long,
        @Query("include[]") seller: String = "seller",
        @Query("include[]") photos: String = "photos"
    ): Observable<BaseResponseNew<List<ProductDTO>>>

}
