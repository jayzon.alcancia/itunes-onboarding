package com.appetiser.module.network.features.marketplace.model

import com.appetiser.module.domain.models.marketplace.ProductCategory

data class ProductCategoryDTO(
    val id: Long? = 0,
    val label: String = "",
    val type: String = ""
) {
    companion object {
        fun toDomain(dto: ProductCategoryDTO): ProductCategory {
            return with(dto) {
                ProductCategory(
                    id = id ?: 0,
                    label = label.orEmpty(),
                    type = type.orEmpty()
                )
            }
        }

        fun mapProductCategoryDTOToProductCategories(dto: List<ProductCategoryDTO>?): List<ProductCategory> {
            return if (!dto.isNullOrEmpty()) dto.map { toDomain(it) } else emptyList()
        }
    }
}
