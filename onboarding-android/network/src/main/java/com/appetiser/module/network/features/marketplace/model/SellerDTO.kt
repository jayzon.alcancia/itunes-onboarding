package com.appetiser.module.network.features.marketplace.model

import com.appetiser.module.domain.models.marketplace.Seller
import com.appetiser.module.network.ext.fromIsoDateTimeToInstant
import com.google.gson.annotations.SerializedName
import java.time.Instant

data class SellerDTO(
    val id: Long,
    @SerializedName("full_name")
    val fullName: String,
    val description: String? = "",
    val birthdate: String? = "",
    val gender: String? = "",
    val email: String,
    @SerializedName("phone_number")
    val phoneNumber: String? = "",
    @SerializedName("created_at")
    val createdAt: String? = "",
    @SerializedName("updated_at")
    val updatedAt: String? = "",
    @SerializedName("blocked_at")
    val blockedAt: String? = "",
    @SerializedName("email_verified")
    val emailVerified: Boolean? = false,
    @SerializedName("phone_number_verified")
    val phoneNumberVerified: Boolean? = false,
    val verified: Boolean? = false,
    @SerializedName("avatar_permanent_url")
    val avatarPermanentUrl: String,
    @SerializedName("avatar_permanent_thumb_url")
    val avatarPermanentThumbUrl: String,
    val mine: Boolean
) {
    companion object {
        fun toDomain(dto: SellerDTO?): Seller {
            if (dto == null) {
                return Seller()
            } else {
                return with(dto) {
                    Seller(
                        id,
                        fullName,
                        description.orEmpty(),
                        birthdate.orEmpty(),
                        gender.orEmpty(),
                        email,
                        phoneNumber.orEmpty(),
                        createdAt.orEmpty().fromIsoDateTimeToInstant(),
                        updatedAt.orEmpty().fromIsoDateTimeToInstant(),
                        blockedAt?.fromIsoDateTimeToInstant() ?: Instant.now(),
                        emailVerified ?: false,
                        phoneNumberVerified ?: false,
                        verified ?: false,
                        avatarPermanentUrl,
                        avatarPermanentThumbUrl,
                        mine
                    )
                }
            }
        }
    }
}
