package com.appetiser.module.network.features.marketplace

import com.appetiser.module.domain.models.Paging
import com.appetiser.module.domain.models.marketplace.Product
import com.appetiser.module.domain.models.marketplace.ProductCategory
import com.appetiser.module.domain.models.marketplace.SearchFilters
import com.appetiser.module.domain.models.token.AccessToken
import com.appetiser.module.network.base.BaseRemoteSource
import com.appetiser.module.network.ext.convertAttachmentsToMultiPartBody
import com.appetiser.module.network.features.BaseplateApiServices
import com.appetiser.module.network.features.marketplace.model.ProductCategoryDTO
import com.appetiser.module.network.features.marketplace.model.ProductDTO
import io.reactivex.Observable
import io.reactivex.Single
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import javax.inject.Inject

class MarketplaceRemoteSourceImpl @Inject constructor(
    private val apiServices: BaseplateApiServices
) : BaseRemoteSource(), MarketplaceRemoteSource {

    override fun postProducts(
        accessToken: AccessToken,
        title: String,
        description: String,
        price: String,
        photos: List<String>?,
        placesId: String?,
        categoryId: Long?
    ): Single<Product> {
        val query = hashMapOf<String, RequestBody>()
        if (title.isNotEmpty()) {
            query["title"] = title.toRequestBody("multipart/form-data".toMediaTypeOrNull())
        }

        if (description.isNotEmpty()) {
            query["description"] = description.toRequestBody("multipart/form-data".toMediaTypeOrNull())
        }
        if (price.isNotEmpty()) {
            query["price"] = price.toRequestBody("multipart/form-data".toMediaTypeOrNull())
        }
        if (!placesId.isNullOrEmpty()) {
            query["places_id"] = placesId.toRequestBody("multipart/form-data".toMediaTypeOrNull())
        }

        categoryId?.let {
            if (it > 0) {
                query["category_id"] = categoryId.toString().toRequestBody("multipart/form-data".toMediaTypeOrNull())
            }
        }

        val multipart = photos?.convertAttachmentsToMultiPartBody("photos")

        return apiServices
            .postProducts(
                accessToken.bearerToken,
                query,
                multipart
            )
            .map {
                ProductDTO.toDomain(it.data)
            }
    }

    override fun getProductDetails(accessToken: AccessToken, productId: Long): Single<Product> {
        return apiServices
            .getProductsDetails(
                accessToken.bearerToken,
                productId
            )
            .map {
                ProductDTO.toDomain(it.data)
            }
    }

    override fun getSimilarProducts(accessToken: AccessToken, productId: Long): Observable<List<Product>> {
        return apiServices
            .getSimilarProducts(
                accessToken.bearerToken,
                productId
            )
            .flatMapIterable { it.data }
            .map {
                ProductDTO.toDomain(it)
            }
            .toList()
            .toObservable()
    }

    override fun getProductCategories(accessToken: AccessToken): Observable<List<ProductCategory>> {
        return apiServices
            .getProductCategories(
                accessToken.bearerToken
            )
            .flatMapIterable { it.data }
            .map {
                ProductCategoryDTO.toDomain(it)
            }
            .toList()
            .toObservable()
    }

    override fun getProducts(accessToken: AccessToken, page: Int): Single<Paging<Product>> {
        return apiServices.getProducts(accessToken.bearerToken, page)
            .map { response ->
                Paging(
                    response.data.map {
                        ProductDTO.toDomain(it)
                    },
                    if (response.meta.currentPage >= response.meta.lastPage) {
                        null
                    } else {
                        response.meta.currentPage + 1
                    }
                )
            }
    }

    override fun searchProducts(
        accessToken: AccessToken,
        page: Int,
        keyword: String,
        searchFilters: SearchFilters
    ): Single<Paging<Product>> {
        return apiServices
            .searchProducts(
                token = accessToken.bearerToken,
                page = page,
                keyword = keyword,
                minMaxPrice = searchFilters.getPriceRangeCsv(),
                placesId = searchFilters.placesId,
                withinDistanceTo = searchFilters.getWithinDistanceToCsv(),
                sort = searchFilters.sortType
            ).map { response ->
                Paging(
                    response.data.map { ProductDTO.toDomain(it) },

                    if (response.meta.currentPage >= response.meta.lastPage) {
                        null
                    } else {
                        response.meta.currentPage + 1
                    }
                )
            }
    }

    override fun getSellerProducts(accessToken: AccessToken, page: Int, profileId: Long): Single<Paging<Product>> {
        return apiServices.getSellerProducts(accessToken.bearerToken, page, profileId)
            .map { response ->
                Paging(
                    response.data.map { ProductDTO.toDomain(it) },

                    if (response.meta.currentPage >= response.meta.lastPage) {
                        null
                    } else {
                        response.meta.currentPage + 1
                    }
                )
            }
    }
}
