package com.appetiser.module.network.features.notification

import com.appetiser.module.domain.models.notification.Notification
import com.appetiser.module.domain.models.token.AccessToken
import io.reactivex.Observable
import io.reactivex.Single

interface NotificationRemoteSource {

    fun registerDeviceToken(
        accessToken: AccessToken,
        deviceToken: String,
        deviceId: String
    ): Single<Boolean>

    fun unRegisterDeviceToken(accessToken: AccessToken, deviceToken: String, deviceId: String): Single<Boolean>

    fun getNotifications(accessToken: AccessToken, type: String): Observable<List<Notification>>
}
