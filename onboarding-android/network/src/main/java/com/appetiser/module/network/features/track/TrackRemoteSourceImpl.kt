package com.appetiser.module.network.features.track

import com.appetiser.module.domain.models.tracks.Track
import com.appetiser.module.network.base.BaseRemoteSource
import com.appetiser.module.network.features.BaseplateApiServices
import com.appetiser.module.network.features.TracksApiServices
import com.appetiser.module.network.features.track.model.response.TrackResponse
import io.reactivex.Single
import javax.inject.Inject

class TrackRemoteSourceImpl @Inject constructor(
    private val apiServices: TracksApiServices
) : BaseRemoteSource(), TrackRemoteSource {

    override fun getAllTrack(): Single<TrackResponse<Track>> =
        apiServices.getAllTracks()

}