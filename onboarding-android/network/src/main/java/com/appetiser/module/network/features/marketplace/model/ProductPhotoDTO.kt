package com.appetiser.module.network.features.marketplace.model

import com.appetiser.module.domain.models.marketplace.ProductPhoto
import com.google.gson.annotations.SerializedName

data class ProductPhotoDTO(
    @SerializedName("collection_name")
    val collectionName: String? = "",
    @SerializedName("created_at")
    val createdAt: String? = "",
    @SerializedName("file_name")
    val fileName: String? = "",
    @SerializedName("id")
    val id: Long? = 0,
    @SerializedName("mime_type")
    val mimeType: String? = "",
    @SerializedName("name")
    val name: String? = "",
    @SerializedName("size")
    val size: Long? = 0,
    @SerializedName("thumb_url")
    val thumbUrl: String? = "",
    @SerializedName("url")
    val url: String? = ""
) {
    companion object {
        fun toDomain(dto: ProductPhotoDTO): ProductPhoto {
            return with(dto) {
                ProductPhoto(
                    collectionName = collectionName.orEmpty(),
                    createdAt = createdAt.orEmpty(),
                    fileName = fileName.orEmpty(),
                    id = id ?: 0,
                    mimeType = mimeType.orEmpty(),
                    name = name.orEmpty(),
                    size = size ?: 0,
                    thumbUrl = thumbUrl.orEmpty(),
                    url = url.orEmpty()
                )
            }
        }

        fun mapProductPhotoDTOToProductPhotos(dto: List<ProductPhotoDTO>?): List<ProductPhoto> {
            return if (!dto.isNullOrEmpty()) dto.map { toDomain(it) } else emptyList()
        }
    }
}
