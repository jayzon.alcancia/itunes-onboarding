package com.appetiser.module.network.features.notification.models.response

import com.appetiser.module.network.features.notification.models.NotificationDTO

data class NotificationsReponse(val data: List<NotificationDTO>)
