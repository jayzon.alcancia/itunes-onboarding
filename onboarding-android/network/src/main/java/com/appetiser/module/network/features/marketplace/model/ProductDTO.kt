package com.appetiser.module.network.features.marketplace.model

import com.appetiser.module.domain.models.marketplace.Product
import com.appetiser.module.domain.models.marketplace.ProductPhoto
import com.appetiser.module.network.ext.fromIsoDateTimeToInstant
import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

data class ProductDTO(
    val id: Long? = 0,
    val title: String? = "",
    val description: String? = "",
    val price: Double? = 0.0,
    @SerializedName("formatted_price")
    val formattedPrice: String? = "",
    val currency: String? = "",
    @SerializedName("category_id")
    val categoryId: Long? = 0,
    @SerializedName("places_id")
    val placesId: String? = "",
    @SerializedName("places_address")
    val placesAddress: String? = "",
    @SerializedName("created_at")
    val createdAt: String = "",
    @SerializedName("updated_at")
    val updatedAt: String = "",
    @SerializedName("deleted_at")
    val deletedAt: String? = "",
    val seller: SellerDTO? = null,
    val photos: List<ProductPhotoDTO>? = null,
) {
    companion object {
        fun toDomain(dto: ProductDTO): Product {
            return with(dto) {
                Product(
                    id = id ?: 0,
                    title = title.orEmpty(),
                    description = description.orEmpty(),
                    price = BigDecimal(price ?: 0.0),
                    formattedPrice = formattedPrice.orEmpty(),
                    currency = currency.orEmpty(),
                    categoryId = categoryId ?: 0,
                    placesId = placesId.orEmpty(),
                    placesAddress = placesAddress.orEmpty(),
                    createdAt = createdAt.fromIsoDateTimeToInstant(),
                    updatedAt = updatedAt.fromIsoDateTimeToInstant(),
                    deletedAt = deletedAt?.fromIsoDateTimeToInstant(),
                    photos = ProductPhotoDTO.mapProductPhotoDTOToProductPhotos(photos),
                    seller = SellerDTO.toDomain(seller)
                )
            }
        }

        fun mapProductDTOToProducts(dto: List<ProductPhotoDTO>?): List<ProductPhoto> {
            return if (!dto.isNullOrEmpty()) dto.map { ProductPhotoDTO.toDomain(it) } else emptyList()
        }
    }
}
