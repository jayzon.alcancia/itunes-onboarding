package com.appetiser.module.network.features.miscellaneous

import com.appetiser.module.domain.models.miscellaneous.ReportCategory
import com.appetiser.module.domain.models.miscellaneous.ReportedUser
import com.appetiser.module.domain.models.token.AccessToken
import io.reactivex.Single

interface MiscellaneousRemoteSource {
    fun getReportCategories(accessToken: AccessToken): Single<List<ReportCategory>>

    fun reportUser(
        accessToken: String,
        reportedUserId: String,
        reasonId: String,
        description: String,
        attachmentPathList: List<String>
    ): Single<ReportedUser>
}
