package com.appetiser.module.network.features.track.model.response

data class TrackResponse<T> (
    val resultCount: Int,
    val results: List<T>
)