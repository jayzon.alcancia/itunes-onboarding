package com.appetiser.baseplate.features.marketplace.seller.profile

import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.rxjava2.cachedIn
import androidx.paging.rxjava2.observable
import com.appetiser.baseplate.R
import com.appetiser.baseplate.base.BaseViewModel
import com.appetiser.baseplate.features.marketplace.seller.profile.MarketplaceSellerProfileActivity.Companion.KEY_PROFILE_ID
import com.appetiser.baseplate.utils.ResourceManager
import com.appetiser.module.data.features.marketplace.MarketplaceRepository
import com.appetiser.module.data.features.user.UserRepository
import com.appetiser.module.domain.models.user.User
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class MarketplaceSellerProfileViewModel @Inject constructor(
    private val resourceManager: ResourceManager,
    private val userRepository: UserRepository,
    private val marketplaceRepository: MarketplaceRepository
) : BaseViewModel() {

    private val pager by lazy {
        Pager(
            config = PagingConfig(
                pageSize = 20,
                prefetchDistance = 5,
                initialLoadSize = 20
            ),
            pagingSourceFactory = {
                MarketplaceSellerProfileFeedPagingSource(marketplaceRepository, schedulers, profileId)
            }
        )
    }

    private var profileId: Long = 0

    private var isFirstRun = true

    private val _state by lazy {
        PublishSubject.create<MarketplaceSellerProfileState>()
    }

    val state: Observable<MarketplaceSellerProfileState> = _state

    private val _seller by lazy {
        MutableLiveData<User>()
    }

    val seller: LiveData<User> = _seller

    private val _loadingVisibility by lazy {
        MutableLiveData<Boolean>()
    }

    val loadingVisibility: LiveData<Boolean> = _loadingVisibility

    override fun isFirstTimeUiCreate(bundle: Bundle?) {
        super.isFirstTimeUiCreate(bundle)

        profileId = bundle?.getLong(KEY_PROFILE_ID, -1) ?: -1

        pager.observable.cachedIn(viewModelScope)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribeBy(
                onNext = {
                    _state.onNext(MarketplaceSellerProfileState.GetProducts(it))
                },
                onError = {
                    _state.onNext(
                        MarketplaceSellerProfileState.Error(
                            resourceManager.getString(
                                R.string.generic_error_short
                            )
                        )
                    )
                }
            )
            .addTo(disposables)
    }

    fun getSellerDetails() {
        userRepository.getUserById(profileId)
            .subscribeOn(schedulers.io())
            .delay(1000, TimeUnit.MILLISECONDS)
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                if (isFirstRun) {
                    isFirstRun = false
                    _loadingVisibility.value = true
                }
            }
            .subscribeBy(
                onSuccess = {
                    _loadingVisibility.value = false
                    _seller.value = it
                },
                onError = {
                    _loadingVisibility.value = false
                    _state.onNext(
                        MarketplaceSellerProfileState.Error(
                            resourceManager.getString(
                                R.string.generic_error_short
                            )
                        )
                    )
                }
            )
            .addTo(disposables)
    }

    fun showPhotoViewerScreen() {
        _seller.value?.let {
            val list = arrayListOf<String>()
            list.add(it.avatarPermanentUrl)
            _state.onNext(MarketplaceSellerProfileState.ShowPhotoViewerScreen(list))
        }
    }
}
