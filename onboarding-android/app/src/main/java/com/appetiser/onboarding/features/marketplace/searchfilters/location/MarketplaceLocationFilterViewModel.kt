package com.appetiser.baseplate.features.marketplace.searchfilters.location

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.appetiser.baseplate.base.BaseViewModel
import com.appetiser.module.data.features.places.PlacesRepository
import com.appetiser.module.domain.models.places.Place
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import javax.inject.Inject

class MarketplaceLocationFilterViewModel @Inject constructor(
    private val placesRepository: PlacesRepository
) : BaseViewModel() {

    private val _state by lazy {
        PublishSubject.create<MarketplaceLocationFilterState>()
    }

    val state: Observable<MarketplaceLocationFilterState> = _state

    private val _currentLocationDetails by lazy {
        MutableLiveData<Place>()
    }

    val currentLocationDetails: LiveData<Place> = _currentLocationDetails

    fun loadCurrentLocationDetails(currentLatitude: Double, currentLongitude: Double) {
        placesRepository
            .getNearbyPlaces(currentLatitude, currentLongitude)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe { _state.onNext(MarketplaceLocationFilterState.ShowLoading) }
            .doOnSuccess { _state.onNext(MarketplaceLocationFilterState.HideLoading) }
            .doOnError { _state.onNext(MarketplaceLocationFilterState.HideLoading) }
            .subscribeBy(
                onSuccess = ::handleNearbyPlacesResult,
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }

    private fun handleNearbyPlacesResult(nearbyPlaces: List<Place>) {
        if (nearbyPlaces.isNullOrEmpty()) {
            _state.onNext(
                MarketplaceLocationFilterState.Error
            )
            return
        }

        _currentLocationDetails.value = nearbyPlaces.first()
    }

    fun setNewLocation(place: Place) {
        _currentLocationDetails.value = place
    }
}
