package com.appetiser.onboarding.features.trackdetails

import android.content.Intent
import android.os.Bundle
import androidx.core.view.ViewCompat
import androidx.core.view.updatePadding
import com.appetiser.baseplate.R
import com.appetiser.baseplate.base.BaseViewModelActivity
import com.appetiser.baseplate.databinding.ActivityTrackDetailsBinding
import com.appetiser.baseplate.ext.loadImageUrl
import com.appetiser.module.local.features.track.models.TrackDB
import com.google.gson.Gson
import javax.inject.Inject

class TrackDetailsActivity : BaseViewModelActivity<ActivityTrackDetailsBinding, TrackDetailsViewModel>() {

    @Inject
    lateinit var gson: Gson

    override fun getLayoutId() = R.layout.activity_track_details

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val track = gson.fromJson(intent.getStringExtra("track"), TrackDB::class.java)

        track.run {
            viewModel.executeSaveTrack(this)
            binding.imgTrack.loadImageUrl(artworkUrl100)
            binding.txtArtist.text = artistName
            binding.txtGenre.text = genre
            binding.txtLongDesc.text = longDescription
            binding.txtTrack.text = trackName
        }

        ViewCompat.setOnApplyWindowInsetsListener(binding.apbMain) { v, insets ->
            v.updatePadding(top = insets.systemWindowInsetTop)
            insets
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.executeClearTrack()
    }
}