package com.appetiser.baseplate.features.searchlocation

import com.appetiser.module.domain.models.places.Place

sealed class SearchLocationState {
    object ShowLoading : SearchLocationState()

    object HideLoading : SearchLocationState()

    data class SetLocationDetailsResult(
        val place: Place
    ) : SearchLocationState()

    object Error : SearchLocationState()
}
