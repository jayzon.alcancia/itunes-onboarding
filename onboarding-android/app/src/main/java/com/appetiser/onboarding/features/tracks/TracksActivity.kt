package com.appetiser.onboarding.features.tracks

import android.content.Intent
import android.os.Bundle
import androidx.core.view.ViewCompat
import androidx.core.view.isVisible
import androidx.core.view.updatePadding
import androidx.recyclerview.widget.LinearLayoutManager
import com.appetiser.baseplate.R
import com.appetiser.baseplate.base.BaseViewModelActivity
import com.appetiser.baseplate.databinding.ActivityTracksBinding
import com.appetiser.baseplate.features.searchlocation.adapter.SearchLocationAdapter
import com.appetiser.module.common.toast
import com.appetiser.onboarding.features.trackdetails.TrackDetailsActivity
import com.appetiser.onboarding.features.tracks.adapter.TracksAdapter
import com.google.gson.Gson
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.activity_tracks.*
import javax.inject.Inject

class TracksActivity : BaseViewModelActivity<ActivityTracksBinding, TracksViewModel>() {

    @Inject
    lateinit var gson: Gson

    private lateinit var adapter: TracksAdapter

    override fun getLayoutId() = R.layout.activity_tracks

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setupToolbar()
        setupViews()
        setupVmObservers()

        ViewCompat.setOnApplyWindowInsetsListener(binding.apbMain) { v, insets ->
            v.updatePadding(top = insets.systemWindowInsetTop)
            insets
        }
    }

    private fun setupVmObservers() {
        viewModel
            .state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = {
                    binding.prgTracks.isVisible = it == TracksState.Loading
                    binding.rcvTracks.isVisible = it == TracksState.Success
                    binding.btnRetry.isVisible = it is TracksState.Error
                    if (it is TracksState.Error) {
                        binding.btnRetry.text = it.e.localizedMessage ?: "Please try again"
                    }
                },
                onError = {
                    toast(it.localizedMessage ?: "Something went wrong")
                }
            ).addTo(disposables)

        viewModel.tracks.observe(this) {
            adapter.submitList(it)
        }

        if (viewModel.savedTrack != null) {
            val intent = Intent(this@TracksActivity, TrackDetailsActivity::class.java).apply {
                putExtra("track", gson.toJson(viewModel.savedTrack))
            }
            startActivity(intent)
        }
    }

    private fun setupViews() {
        setupRecyclerView()
        viewModel.setTracks()
    }

    private fun setupRecyclerView() {
        binding.rcvTracks.apply {
            layoutManager = LinearLayoutManager(this@TracksActivity)

            this@TracksActivity.adapter = TracksAdapter {
                val intent = Intent(this@TracksActivity, TrackDetailsActivity::class.java).apply {
                    putExtra("track", gson.toJson(it))
                }
                startActivity(intent)
            }

            adapter = this@TracksActivity.adapter
        }
    }

    private fun setupToolbar() {
        setToolbarTitle(getString(R.string.tracks))
        enableToolbarHomeIndicator()
    }
}
