package com.appetiser.baseplate.features.searchlocation

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.appetiser.baseplate.R
import com.appetiser.baseplate.base.BaseViewModelActivity
import com.appetiser.baseplate.databinding.ActivitySearchLocationBinding
import com.appetiser.baseplate.features.searchlocation.adapter.SearchLocationAdapter
import com.appetiser.module.common.TEXT_WATCHER_DEBOUNCE_TIME
import com.appetiser.module.common.setVisible
import com.appetiser.module.common.showGenericErrorSnackBar
import com.google.gson.Gson
import com.jakewharton.rxbinding3.widget.textChanges
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class SearchLocationActivity : BaseViewModelActivity<ActivitySearchLocationBinding, SearchLocationViewModel>() {

    @Inject
    lateinit var gson: Gson

    private lateinit var adapter: SearchLocationAdapter

    override fun getLayoutId(): Int = R.layout.activity_search_location

    override fun canBack() = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setupToolbar()
        setupViews()
        setupVmObservers()
    }

    private fun setupVmObservers() {
        viewModel
            .searchResults
            .observe(this) { places ->
                adapter.submitList(places)
            }

        viewModel
            .state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = ::handleState,
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }

    private fun handleState(state: SearchLocationState) {
        when (state) {
            SearchLocationState.ShowLoading -> {
                binding.loading setVisible true
            }
            SearchLocationState.HideLoading -> {
                binding.loading setVisible false
            }
            SearchLocationState.Error -> {
                showGenericErrorSnackBar(
                    binding.root,
                    getString(R.string.generic_error_short)
                )
            }
            is SearchLocationState.SetLocationDetailsResult -> {
                setResult(
                    Activity.RESULT_OK,
                    Intent().apply {
                        putExtra(
                            SearchLocationContract.EXTRA_PLACE_JSON,
                            gson.toJson(state.place)
                        )
                    }
                )
                finish()
            }
        }
    }

    private fun setupViews() {
        setupListeners()
        setupRecyclerView()
    }

    private fun setupRecyclerView() {
        binding.recyclerView.apply {
            layoutManager = LinearLayoutManager(this@SearchLocationActivity)

            this@SearchLocationActivity.adapter =
                SearchLocationAdapter { item ->
                    viewModel.onSelectLocation(item)
                }

            adapter = this@SearchLocationActivity.adapter
        }
    }

    private fun setupListeners() {
        binding
            .inputSearch
            .textChanges()
            .debounce(TEXT_WATCHER_DEBOUNCE_TIME, TimeUnit.MILLISECONDS)
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = { charSequence ->
                    viewModel.onKeywordChanged(charSequence.toString())
                },
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }

    private fun setupToolbar() {
        setToolbarTitle(getString(R.string.location))
        enableToolbarHomeIndicator()
    }
}
