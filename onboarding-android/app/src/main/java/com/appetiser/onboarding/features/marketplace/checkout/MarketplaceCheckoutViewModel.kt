package com.appetiser.baseplate.features.marketplace.checkout

import com.appetiser.baseplate.base.BaseViewModel
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class MarketplaceCheckoutViewModel @Inject constructor() : BaseViewModel() {

    private val _state by lazy {
        PublishSubject.create<MarketplaceCheckoutState>()
    }

    val state: Observable<MarketplaceCheckoutState> = _state
}