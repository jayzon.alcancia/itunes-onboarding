package com.appetiser.baseplate.features.profile

import android.os.Bundle
import android.view.View
import com.appetiser.baseplate.R
import com.appetiser.baseplate.base.BaseViewModelFragment
import com.appetiser.baseplate.databinding.FragmentProfileBinding
import com.appetiser.baseplate.ext.loadAvatarUrl
import com.appetiser.baseplate.features.account.AccountActivity
import com.appetiser.baseplate.features.profile.editprofile.EditProfileActivity
import com.appetiser.module.common.ninjaTap
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy

class ProfileFragment : BaseViewModelFragment<FragmentProfileBinding, ProfileViewModel>() {

    companion object {
        fun newInstance(): ProfileFragment {
            return ProfileFragment()
        }
    }

    override fun getLayoutId(): Int = R.layout.fragment_profile

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupToolbarAndStatusBar()
        setupViewModel()

        binding.editProfile
            .ninjaTap {
                EditProfileActivity.openActivity(requireActivity())
            }
            .addTo(disposables)
    }

    override fun onResume() {
        super.onResume()
        viewModel.loadAccount()
    }

    private fun setupViewModel() {
        viewModel
            .state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = {
                    handleState(it)
                }
            ).addTo(disposables)
    }

    private fun handleState(state: ProfileState) {
        when (state) {
            is ProfileState.UserProfileSession -> {
                binding.avatar.loadAvatarUrl(state.user.avatar.thumbUrl)
                binding.description.text = state.user.description
            }
        }
    }

    private fun setupToolbarAndStatusBar() {
        binding.toolbarView.title = ""
        binding.toolbarView.inflateMenu(R.menu.profile_menu)

        binding.toolbarView.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.action_settings -> {
                    AccountActivity.openActivity(requireActivity())
                    true
                }
                else -> {
                    super.onOptionsItemSelected(it)
                }
            }
        }

        binding.toolbarTitle.text = getString(R.string.my_profile)
    }
}
