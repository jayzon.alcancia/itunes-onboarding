package com.appetiser.baseplate.di.builders

import com.appetiser.baseplate.di.scopes.FragmentScope
import com.appetiser.baseplate.features.feed.list.FeedsFragment
import com.appetiser.baseplate.features.main.DummyFragment
import com.appetiser.baseplate.features.marketplace.feed.MarketplaceFeedFragment
import com.appetiser.baseplate.features.marketplace.searchfilters.*
import com.appetiser.baseplate.features.marketplace.searchfilters.location.MarketplaceLocationFilterDialog
import com.appetiser.baseplate.features.notification.NotificationFragment
import com.appetiser.baseplate.features.photoviewer.ImageFragment
import com.appetiser.baseplate.features.profile.ProfileFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuilder {

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeNotificationFragment(): NotificationFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeFeedsFragment(): FeedsFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeProfileFragment(): ProfileFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeMarketplaceFeedFragment(): MarketplaceFeedFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeMarketplaceSearchFiltersDialog(): MarketplaceSearchFiltersDialog

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeMarketplaceLocationFilterDialog(): MarketplaceLocationFilterDialog

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeMarketplacePriceFilterDialog(): MarketplacePriceFilterDialog

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeMarketplaceDistanceFilterDialog(): MarketplaceDistanceFilterDialog

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeMarketplaceSortByFilterDialog(): MarketplaceSortByFilterDialog

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeDummyFragment(): DummyFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeImageFragment(): ImageFragment
}
