package com.appetiser.baseplate.features.main

import com.appetiser.baseplate.R
import com.appetiser.baseplate.base.BaseFragment
import com.appetiser.baseplate.databinding.FragmentDummyBinding

class DummyFragment : BaseFragment<FragmentDummyBinding>() {
    companion object {
        fun newInstance(): DummyFragment {
            return DummyFragment()
        }
    }

    override fun getLayoutId(): Int = R.layout.fragment_dummy
}
