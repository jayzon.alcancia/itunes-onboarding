package com.appetiser.baseplate.features.marketplace.details

import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.appetiser.baseplate.R
import com.appetiser.baseplate.base.BaseViewModel
import com.appetiser.baseplate.features.marketplace.details.MarketplaceProductDetailsActivity.Companion.KEY_PRODUCT_ID
import com.appetiser.baseplate.utils.ResourceManager
import com.appetiser.module.data.features.marketplace.MarketplaceRepository
import com.appetiser.module.domain.models.marketplace.Product
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class MarketplaceProductDetailsViewModel @Inject constructor(
    private val repository: MarketplaceRepository,
    private val resourceManager: ResourceManager
) : BaseViewModel() {

    private val _state by lazy {
        PublishSubject.create<MarketplaceProductDetailsState>()
    }

    val state: Observable<MarketplaceProductDetailsState> = _state

    private val _loadingVisibility by lazy {
        MutableLiveData<Boolean>()
    }

    val loadingVisibility: LiveData<Boolean> = _loadingVisibility

    private var sellerProduct: Product? = null
    private var productId: Long = 0

    override fun isFirstTimeUiCreate(bundle: Bundle?) {
        productId = bundle?.getLong(KEY_PRODUCT_ID, 0) ?: 0
    }

    fun getProductDetails() {
        Observable.combineLatest<Product, List<Product>, Pair<Product, List<Product>>>(
            repository
                .getProductDetails(productId)
                .toObservable(),
            repository.getSimilarProducts(productId),
            BiFunction { productDetails, similarProducts ->
                return@BiFunction Pair(productDetails, similarProducts)
            }
        )
            .delay(1000, TimeUnit.MILLISECONDS)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _loadingVisibility.value = true
            }
            .subscribeBy(
                onNext = {
                    sellerProduct = it.first
                    _loadingVisibility.value = false
                    _state.onNext(MarketplaceProductDetailsState.GetProductDetails(it.first))
                    _state.onNext(MarketplaceProductDetailsState.GetSimilarProducts(it.second))
                },
                onError = {
                    Timber.e("Error $it")
                    _loadingVisibility.value = false

                    _state.onNext(
                        MarketplaceProductDetailsState.Error(
                            resourceManager.getString(
                                R.string.generic_error_short
                            )
                        )
                    )
                }
            )
            .addTo(disposables)
    }

    fun showSellerProfileScreen() {
        sellerProduct?.let {
            _state.onNext(MarketplaceProductDetailsState.ShowSellerProfileScreen(it.seller.id))
        }
    }

    fun showPhotoViewerScreen() {
        sellerProduct?.let { product ->
            _state.onNext(
                MarketplaceProductDetailsState.ShowPhotoViewerScreen(
                    product.photos.map { it.url }
                )
            )
        }
    }
}
