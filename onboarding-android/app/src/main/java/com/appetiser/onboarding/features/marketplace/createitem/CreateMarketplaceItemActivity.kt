package com.appetiser.baseplate.features.marketplace.createitem

import android.Manifest
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import com.appetiser.baseplate.R
import com.appetiser.baseplate.base.BaseCameraActivity
import com.appetiser.baseplate.databinding.ActivityMarketplaceCreateItemBinding
import com.appetiser.baseplate.ext.disabledWithAlpha
import com.appetiser.baseplate.ext.enabledWithAlpha
import com.appetiser.baseplate.features.marketplace.createitem.adapter.*
import com.appetiser.module.common.NINJA_TAP_THROTTLE_TIME
import com.appetiser.module.common.ninjaTap
import com.appetiser.module.common.showGenericErrorSnackBar
import com.appetiser.module.common.toast
import com.jakewharton.rxbinding3.widget.textChangeEvents
import io.reactivex.Observable
import io.reactivex.functions.Function3
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import java.util.concurrent.TimeUnit

class CreateMarketplaceItemActivity : BaseCameraActivity<ActivityMarketplaceCreateItemBinding, CreateMarketplaceItemViewModel>() {

    companion object {
        fun openActivity(context: Context) {
            context.startActivity(
                Intent(
                    context,
                    CreateMarketplaceItemActivity::class.java
                )
            )
        }
    }

    private lateinit var categoryAdapter: ProductCategoryAdapter

    override fun canBack(): Boolean {
        return true
    }

    override fun getMultipleImageEnabled(): Boolean {
        return true
    }

    override fun getLayoutId(): Int = R.layout.activity_marketplace_create_item

    private val adapter: PhotoAttachmentsAdapter by lazy {
        PhotoAttachmentsAdapter(this@CreateMarketplaceItemActivity, onItemClickListener)
    }

    private val onItemClickListener = object : PhotoAttachmentCallback {
        override fun addPhoto() {
            launchPhotoChooser()
        }

        override fun deletePhoto(item: PhotoAttachmentsViewTypeObject) {
            deleteUploadedPhoto(item)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupToolbar()
        setupViewModel()
        setupRecyclerView()
        observeInputViews()

        viewModel.getProductCategories()

        binding.btnPostItem
            .ninjaTap {
                viewModel.postProduct(
                    title = binding.etItemTitle.text.toString(),
                    categoryId = categoryAdapter.getItem(binding.spinnerReason.selectedItemPosition)?.id!!,
                    price = binding.etPrice.text.toString(),
                    description = binding.etDescription.text.toString()
                )
            }
            .addTo(disposables)

        // setSampleData()
    }

    private fun setSampleData() {
        binding.etItemTitle.setText("Sample Title ")
        binding.etPrice.setText("99.99")
        binding.etDescription.setText("Sample description ")
    }

    private fun observeInputViews() {
        val titleObservable = binding.etItemTitle.textChangeEvents()
            .skipInitialValue()
            .map { it.text }
            .map { it.isNotEmpty() }

        val priceObservable = binding.etPrice.textChangeEvents()
            .skipInitialValue()
            .map { it.text }
            .map { it.isNotEmpty() }

        val descriptionObservable = binding.etDescription.textChangeEvents()
            .skipInitialValue()
            .map { it.text }
            .map { it.isNotEmpty() }

        Observable.combineLatest<Boolean, Boolean, Boolean, Boolean>(
            titleObservable, priceObservable, descriptionObservable,
            Function3 { title, price, description ->
                return@Function3 title && price && description
            }
        )
            .debounce(NINJA_TAP_THROTTLE_TIME, TimeUnit.MILLISECONDS)
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = {
                    if (it) {
                        binding.btnPostItem.enabledWithAlpha()
                    } else {
                        binding.btnPostItem.disabledWithAlpha()
                    }
                },
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }

    private fun setupToolbar() {
        setToolbarTitle(
            getString(R.string.list_an_item)
        )
    }

    private fun setupViewModel() {
        viewModel.state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = {
                    handleState(it)
                }
            ).addTo(disposables)
    }

    private fun setupRecyclerView() {
        with(binding.photoList) {
            layoutManager = GridLayoutManager(this@CreateMarketplaceItemActivity, 3)
            adapter = this@CreateMarketplaceItemActivity.adapter
        }
        viewModel.displayAddPhotoButton(PhotoAttachmentsViewType.DISPLAY_ADD_BTN)
    }

    private fun launchPhotoChooser() {
        rxPermissions.requestEachCombined(
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )
            .doOnNext {
                when {
                    it.granted -> {
                        showPickerDialog()
                    }
                    else -> {
                        hidePickerDialog()
                    }
                }
            }
            .subscribe()
    }

    override fun imageUrlPath(captureCameraPath: String) {
        viewModel.addPhoto(captureCameraPath)
    }

    private fun handleState(state: CreateMarketPlaceItemState) {
        when (state) {
            is CreateMarketPlaceItemState.UploadProductSuccess -> handleUploadProductSuccess()
            is CreateMarketPlaceItemState.NoImagesFound -> handleNoImagesFound()
            is CreateMarketPlaceItemState.DisplayAddButton -> handleDisplayAddButton()
            is CreateMarketPlaceItemState.AddPhoto -> handleAddPhoto(state)
            is CreateMarketPlaceItemState.RemovePhoto -> handleRemovePhoto(state)
            is CreateMarketPlaceItemState.GetProductCategories -> handleProductCategories(state)
            is CreateMarketPlaceItemState.ShowProgressLoading -> handleShowProgress()
            is CreateMarketPlaceItemState.HideProgressLoading -> handleHideProgress()
            is CreateMarketPlaceItemState.Error -> handleError(state.throwable.message)
        }
    }

    private fun handleUploadProductSuccess() {
        toast("Product item upload success!")
        finish()
    }

    private fun handleNoImagesFound() {
        showGenericErrorSnackBar(
            binding.root,
            getString(R.string.images_must_not_be_empty)
        )
    }

    private fun deleteUploadedPhoto(item: PhotoAttachmentsViewTypeObject) {
        viewModel.removePhoto(item)
    }

    private fun handleRemovePhoto(state: CreateMarketPlaceItemState.RemovePhoto) {
        adapter.removeSingleItem(state.item)
    }

    private fun handleProductCategories(state: CreateMarketPlaceItemState.GetProductCategories) {
        binding
            .spinnerReason
            .apply {
                categoryAdapter = ProductCategoryAdapter(this@CreateMarketplaceItemActivity, state.items)
                adapter = categoryAdapter
            }
    }

    private fun handleDisplayAddButton() {
        adapter.addSingleItem(PhotoAttachmentsViewTypeObject(PhotoAttachmentsViewType.DISPLAY_ADD_BTN, ""))
    }

    private fun handleAddPhoto(state: CreateMarketPlaceItemState.AddPhoto) {
        adapter.addSingleItem(PhotoAttachmentsViewTypeObject(PhotoAttachmentsViewType.DISPLAY_PHOTO, state.imagePath))
    }

    private fun handleHideProgress() {
    }

    private fun handleShowProgress() {
    }

    private fun handleError(message: String?) {
        showGenericErrorSnackBar(
            binding.root,
            message.orEmpty()
        )
    }
}
