package com.appetiser.onboarding.features.trackdetails

import androidx.lifecycle.ViewModel
import com.appetiser.baseplate.base.BaseViewModel
import com.appetiser.module.data.features.tracks.TracksRepository
import com.appetiser.module.local.features.track.models.TrackDB
import javax.inject.Inject

class TrackDetailsViewModel @Inject constructor(
    private val tracksRepository: TracksRepository
) : BaseViewModel() {

    fun executeSaveTrack(track: TrackDB) {
        tracksRepository.saveTrackToCache(track)
    }

    fun executeClearTrack() {
        tracksRepository.clearTrack()
    }
}