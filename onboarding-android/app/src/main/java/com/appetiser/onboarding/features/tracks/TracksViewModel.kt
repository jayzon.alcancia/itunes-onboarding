package com.appetiser.onboarding.features.tracks

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.appetiser.baseplate.base.BaseViewModel
import com.appetiser.module.data.features.tracks.TracksRepository
import com.appetiser.module.domain.models.places.SearchPlace
import com.appetiser.module.domain.models.tracks.Track
import com.appetiser.module.local.features.track.models.TrackDB
import com.appetiser.module.local.features.track.models.TrackDB.Companion.fromDomain
import com.appetiser.module.local.features.track.models.TrackDB.Companion.toDomain
import com.appetiser.module.network.features.track.model.response.TrackResponse
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class TracksViewModel @Inject constructor(
    private val tracksRepository: TracksRepository
) : BaseViewModel() {

    private val _state by lazy {
        PublishSubject.create<TracksState>()
    }

    val state: Observable<TracksState> = _state

    private val _tracks by lazy {
        MutableLiveData<List<Track>>()
    }

    val tracks: LiveData<List<Track>> = _tracks

    val savedTrack = tracksRepository.getTrackFromCache()

    fun setTracks() {
        tracksRepository.getAllItunesTracksLocally()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.io())
            .doOnSubscribe { _state.onNext(TracksState.Loading) }
            .doOnSuccess { _state.onNext(TracksState.Success) }
            .subscribeBy(
                onSuccess = {
                    if (it.isEmpty()) {
                        getTracksOnline()
                    } else {
                        handleTracks(it)
                    }
                },
                onError = { _state.onNext(TracksState.Error(it)) }
            )
            .addTo(disposables)


    }

    private fun getTracksOnline() {
        tracksRepository.getAllItunesTracks()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.io())
            .doOnSubscribe { _state.onNext(TracksState.Loading) }
            .doOnSuccess { _state.onNext(TracksState.Success) }
            .subscribeBy(
                onSuccess = {
                    tracksRepository.saveAllItunesTracksLocally(it.results.map {
                        it.fromDomain()
                    })
                    _tracks.postValue(it.results)
                },
                onError = { _state.onNext(TracksState.Error(it)) }
            )
            .addTo(disposables)
    }

    private fun handleTracks(tracks: List<TrackDB>) {
        tracksRepository.getAllItunesTracksLocally()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.io())
            .doOnSubscribe { _state.onNext(TracksState.Loading) }
            .doOnSuccess { _state.onNext(TracksState.Success) }
            .subscribeBy {

                if (it.isEmpty()) {
                    it.run {
                        tracksRepository.saveAllItunesTracksLocally(tracks)
                        this
                    }
                }
                _tracks.postValue(it.map {
                    it.toDomain()
                })
            }
            .addTo(disposables)

    }

}