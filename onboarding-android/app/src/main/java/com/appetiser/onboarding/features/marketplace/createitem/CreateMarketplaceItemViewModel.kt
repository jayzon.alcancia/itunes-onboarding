package com.appetiser.baseplate.features.marketplace.createitem

import android.net.Uri
import android.os.Bundle
import com.appetiser.baseplate.base.BaseViewModel
import com.appetiser.baseplate.features.marketplace.createitem.adapter.PhotoAttachmentsViewType
import com.appetiser.baseplate.features.marketplace.createitem.adapter.PhotoAttachmentsViewTypeObject
import com.appetiser.baseplate.utils.FileUtils
import com.appetiser.module.data.features.marketplace.MarketplaceRepository
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class CreateMarketplaceItemViewModel @Inject constructor(
    private val repository: MarketplaceRepository
) : BaseViewModel() {

    private var pathList: ArrayList<String> = arrayListOf()

    private val _state by lazy {
        PublishSubject.create<CreateMarketPlaceItemState>()
    }

    val state: Observable<CreateMarketPlaceItemState> = _state

    override fun isFirstTimeUiCreate(bundle: Bundle?) = Unit

    fun addPhoto(imagePath: String) {
        this.pathList.add(imagePath)
        _state.onNext(CreateMarketPlaceItemState.AddPhoto(imagePath))
    }

    fun removePhoto(item: PhotoAttachmentsViewTypeObject) {
        this.pathList.remove(item.imagePath)
        _state.onNext(CreateMarketPlaceItemState.RemovePhoto(item))
    }

    fun displayAddPhotoButton(item: PhotoAttachmentsViewType) {
        _state.onNext(CreateMarketPlaceItemState.DisplayAddButton(item))
    }

    private fun deleteResizedPhotos(uris: List<Uri>) {
        uris.forEach { uri ->
            FileUtils.deleteFile(uri)
        }
    }

    fun getProductCategories() {
        repository
            .getProductCategories()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribeBy(
                onNext = {
                    _state.onNext(CreateMarketPlaceItemState.GetProductCategories(it))
                },
                onError = {
                    _state.onNext(CreateMarketPlaceItemState.Error(it))
                }
            )
            .addTo(disposables)
    }

    fun postProduct(title: String, categoryId: Long, price: String, description: String) {
        if (pathList.isEmpty()) {
            _state.onNext(CreateMarketPlaceItemState.NoImagesFound)
            return
        }

        repository
            .postProducts(
                title = title,
                categoryId = categoryId,
                description = description,
                price = price,
                photos = pathList
            )
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribeBy(
                onSuccess = {
                    _state.onNext(CreateMarketPlaceItemState.UploadProductSuccess(it))
                },
                onError = {
                    _state.onNext(CreateMarketPlaceItemState.Error(it))
                }
            )
            .addTo(disposables)
    }
}
