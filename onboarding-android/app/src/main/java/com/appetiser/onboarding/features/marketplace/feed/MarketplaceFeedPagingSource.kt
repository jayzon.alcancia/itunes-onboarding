package com.appetiser.baseplate.features.marketplace.feed

import androidx.paging.rxjava2.RxPagingSource
import com.appetiser.baseplate.utils.schedulers.BaseSchedulerProvider
import com.appetiser.module.data.features.marketplace.MarketplaceRepository
import com.appetiser.module.domain.models.marketplace.Product
import io.reactivex.Single
import javax.inject.Inject

class MarketplaceFeedPagingSource @Inject constructor(
    private val marketplaceRepository: MarketplaceRepository,
    private val schedulers: BaseSchedulerProvider
) : RxPagingSource<Int, Product>() {

    override fun loadSingle(params: LoadParams<Int>): Single<LoadResult<Int, Product>> {
        val page = params.key ?: 1
        return marketplaceRepository.getProducts(page)
            .subscribeOn(schedulers.io())
            .map {
                LoadResult.Page(
                    it.list,
                    null,
                    it.nextPage
                )
            }
    }
}
