package com.appetiser.baseplate.features.marketplace.details.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import com.appetiser.baseplate.R
import com.appetiser.baseplate.base.BaseListAdapter
import com.appetiser.baseplate.databinding.ItemMarketplaceFeedMiniBinding
import com.appetiser.baseplate.ext.loadImageUrlFitCenter
import com.appetiser.module.common.ninjaTap
import com.appetiser.module.domain.models.marketplace.Product

val PRODUCT_COMPARATOR = object : DiffUtil.ItemCallback<Product>() {
    override fun areItemsTheSame(oldItem: Product, newItem: Product): Boolean =
        oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: Product, newItem: Product): Boolean =
        oldItem == newItem
}

class MarketplaceSimilarProductsAdapter(val listener: Listener) :
    BaseListAdapter<Product, BaseListAdapter.BaseViewViewHolder<Product>>(PRODUCT_COMPARATOR) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewViewHolder<Product> {
        return createMarketplaceSimilarProductsViewHolder(parent)
    }

    override fun onBindViewHolder(holder: BaseViewViewHolder<Product>, position: Int) =
        holder.bind(getItem(position))

    inner class MarketplaceSimimlarProductViewHolder(override val binding: ItemMarketplaceFeedMiniBinding) : BaseListAdapter.BaseViewViewHolder<Product>(binding) {
        override fun bind(item: Product) {
            super.bind(item)
            binding.name.text = item.title

            binding.price.text = item.formattedPrice

            binding.photo.loadImageUrlFitCenter(item.photos[0].url)

            binding.root.ninjaTap {
                listener.onItemClicked(item.id)
            }
        }
    }

    private fun createMarketplaceSimilarProductsViewHolder(parent: ViewGroup): BaseViewViewHolder<Product> {
        val view =
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.item_marketplace_feed_mini, parent, false)

        val binding = ItemMarketplaceFeedMiniBinding.bind(view)

        return MarketplaceSimimlarProductViewHolder(binding)
    }

    interface Listener {
        fun onItemClicked(id: Long)
    }
}
