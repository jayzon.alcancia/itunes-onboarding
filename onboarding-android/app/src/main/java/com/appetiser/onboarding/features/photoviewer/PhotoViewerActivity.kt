package com.appetiser.baseplate.features.photoviewer

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.appetiser.baseplate.R
import com.appetiser.baseplate.base.BaseActivity
import com.appetiser.baseplate.databinding.ActivityPhotoViewerBinding

class PhotoViewerActivity : BaseActivity<ActivityPhotoViewerBinding>() {

    companion object {
        const val URI_LIST_DATA = "URI_LIST_DATA"
        const val IMAGE_FULL_SCREEN_CURRENT_POS = "IMAGE_FULL_SCREEN_CURRENT_POS"

        fun openActivity(context: Context, images: ArrayList<String>, position: Int) {
            val intent = Intent(context, PhotoViewerActivity::class.java)
            intent.putStringArrayListExtra(URI_LIST_DATA, images)
            intent.putExtra(IMAGE_FULL_SCREEN_CURRENT_POS, position)
            context.startActivity(intent)
        }
    }

    override fun getLayoutId(): Int = R.layout.activity_photo_viewer

    override fun canBack(): Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setToolbarNoTitle()
        setupViewPager()
    }

    private fun setupViewPager() {
        val imagePaths = intent.getStringArrayListExtra(URI_LIST_DATA) ?: ArrayList<String>()
        val currentPos = intent.getIntExtra(IMAGE_FULL_SCREEN_CURRENT_POS, 0)
        val adapter = PhotoViewerPagerAdapter(supportFragmentManager, imagePaths)
        binding.viewPager.adapter = adapter
        binding.viewPager.currentItem = currentPos
    }

    private inner class PhotoViewerPagerAdapter(fm: FragmentManager, private val images: ArrayList<String>) : FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

        override fun getCount(): Int = images.size

        override fun getItem(position: Int): Fragment = ImageFragment.newInstance(images[position])
    }
}
