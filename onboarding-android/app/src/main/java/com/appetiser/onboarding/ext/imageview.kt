package com.appetiser.baseplate.ext

import android.graphics.drawable.Drawable
import android.widget.ImageView
import androidx.annotation.DrawableRes
import androidx.databinding.BindingAdapter
import com.appetiser.baseplate.GlideApp
import com.appetiser.baseplate.R
import com.appetiser.baseplate.widget.RotateTransformation
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import timber.log.Timber

@BindingAdapter("avatarUrl")
fun ImageView.loadAvatarUrl(url: String?) {
    GlideApp.with(this.context)
        .load(url)
        .placeholder(R.drawable.ic_default_avatar)
        .error(R.drawable.ic_default_avatar)
        .fallback(R.drawable.ic_default_avatar)
        .skipMemoryCache(false)
        .dontAnimate()
        .listener(object : RequestListener<Drawable> {
            override fun onLoadFailed(
                e: GlideException?,
                model: Any?,
                target: Target<Drawable>?,
                isFirstResource: Boolean
            ): Boolean {
                Timber.e("onLoadFailed Error $e")
                return false
            }

            override fun onResourceReady(
                resource: Drawable?,
                model: Any?,
                target: Target<Drawable>?,
                dataSource: DataSource?,
                isFirstResource: Boolean
            ): Boolean {
                return false
            }
        })
        .into(this)
}

@BindingAdapter("imageSrc")
fun ImageView.loadImageUrl(url: String?) {
    if (url != null && url.isNotEmpty()) {
        GlideApp.with(this.context)
            .load("$url")
            .placeholder(R.drawable.ic_create_post_placeholder)
            .skipMemoryCache(false)
            .centerCrop()
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    Timber.e("onLoadFailed Error $e ")
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: Target<Drawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    return false
                }
            })
            .into(this)
    }
}

@BindingAdapter("imageSrc")
fun ImageView.loadImageUrlFitCenter(url: String?) {
    if (url != null && url.isNotEmpty()) {
        GlideApp.with(this.context)
            .load("$url")
            .placeholder(R.drawable.ic_create_post_placeholder)
            .skipMemoryCache(false)
            .fitCenter()
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    Timber.e("onLoadFailed Error $e ")
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: Target<Drawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    return false
                }
            })
            .into(this)
    }
}

fun ImageView.loadDrawable(@DrawableRes resId: Int?) {
    resId?.let {
        GlideApp.with(context)
            .load(resId)
            .dontAnimate()
            .into(this)
    }
}

fun ImageView.loadImageUrl(url: String?, angle: Float) {
    if (url != null && url.isNotEmpty()) {
        GlideApp.with(this.context)
            .load("$url")
            .transform(RotateTransformation(angle))
            .placeholder(R.drawable.ic_create_post_placeholder)
            .skipMemoryCache(false)
            .diskCacheStrategy(DiskCacheStrategy.NONE)
            .centerCrop().listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    Timber.e("onLoadFailed Error $e ")
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: Target<Drawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    return false
                }
            })
            .into(this)
    }
}
