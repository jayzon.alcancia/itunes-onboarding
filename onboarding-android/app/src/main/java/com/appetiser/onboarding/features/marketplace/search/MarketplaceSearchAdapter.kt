package com.appetiser.baseplate.features.marketplace.search

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.appetiser.baseplate.R
import com.appetiser.baseplate.databinding.ItemMarketplaceFeedBinding
import com.appetiser.baseplate.ext.loadImageUrlFitCenter
import com.appetiser.module.common.ninjaTap
import com.appetiser.module.domain.models.marketplace.Product

val PRODUCT_COMPARATOR = object : DiffUtil.ItemCallback<Product>() {
    override fun areItemsTheSame(oldItem: Product, newItem: Product): Boolean =
        oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: Product, newItem: Product): Boolean =
        oldItem == newItem
}

class MarketplaceSearchAdapter(val listener: Listener) :
    ListAdapter<Product, MarketplaceSearchAdapter.ViewHolder>(PRODUCT_COMPARATOR) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_marketplace_feed, parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(getItem(position)!!)

    inner class ViewHolder(val binding: ItemMarketplaceFeedBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Product) {

            binding.name.text = item.title

            binding.price.text = item.formattedPrice

            binding.photo.loadImageUrlFitCenter(item.photos[0].url)

            binding.root.ninjaTap {
                listener.onItemClicked(item.id)
            }
        }
    }

    interface Listener {
        fun onItemClicked(productId: Long)
    }
}
