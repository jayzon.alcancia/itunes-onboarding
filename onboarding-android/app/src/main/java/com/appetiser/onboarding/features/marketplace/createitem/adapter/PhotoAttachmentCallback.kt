package com.appetiser.baseplate.features.marketplace.createitem.adapter

interface PhotoAttachmentCallback {
    fun addPhoto()
    fun deletePhoto(item: PhotoAttachmentsViewTypeObject)
}
