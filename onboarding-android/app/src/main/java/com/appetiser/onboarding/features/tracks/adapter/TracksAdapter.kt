package com.appetiser.onboarding.features.tracks.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import com.appetiser.baseplate.R
import com.appetiser.baseplate.base.BaseListAdapter
import com.appetiser.baseplate.databinding.ItemTracksBinding
import com.appetiser.baseplate.ext.loadImageUrl
import com.appetiser.module.domain.models.tracks.Track
import com.appetiser.module.local.features.track.models.TrackDB
import com.appetiser.module.local.features.track.models.TrackDB.Companion.fromDomain

/**
 * @param setupTracksBinding setup viewbinding for each item
 */
class TracksAdapter(
    private val onTrackClicked: (TrackDB) -> Unit
) : BaseListAdapter<Track, BaseListAdapter.BaseViewViewHolder<Track>>(TracksDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewViewHolder<Track> {
        return createTracksHolder(parent)
    }

    override fun onBindViewHolder(holder: BaseViewViewHolder<Track>, position: Int) {
        holder.bind(getItem(position))
    }

    private fun createTracksHolder(parent: ViewGroup): BaseListAdapter.BaseViewViewHolder<Track> {
        val view =
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.item_tracks, parent, false)

        val binding = ItemTracksBinding.bind(view)

        return TracksItemViewHolder(binding)
    }

    inner class TracksItemViewHolder(override val binding: ItemTracksBinding) : BaseListAdapter.BaseViewViewHolder<Track>(binding) {
        override fun bind(item: Track) {
            super.bind(item)
            binding.run {
                item.let {
                    txtTrackName.text = it.trackName
                    txtGenre.text = it.genre
                    txtPrice.text = "$ ${it.trackPrice}"
                    imgTrack.loadImageUrl(it.artworkUrl100)

                    root.setOnClickListener {
                        onTrackClicked(item.fromDomain())
                    }
                }
            }
        }
    }
}

class TracksDiffCallback : DiffUtil.ItemCallback<Track>() {
    override fun areItemsTheSame(oldItem: Track, newItem: Track) = oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: Track, newItem: Track) = oldItem == newItem
}


