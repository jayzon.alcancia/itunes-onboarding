package com.appetiser.baseplate.features.notification

sealed class NotificationState {

    data class FetchNotificationItems(val items: List<NotificationViewTypeItem>) : NotificationState()

    data class Error(val throwable: Throwable) : NotificationState()
}
