package com.appetiser.baseplate.features.feed.details

import android.animation.ObjectAnimator
import android.app.Activity
import android.os.Bundle
import android.view.animation.LinearInterpolator
import android.view.inputmethod.EditorInfo
import androidx.core.view.postDelayed
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.appetiser.baseplate.R
import com.appetiser.baseplate.base.BaseViewModelActivity
import com.appetiser.baseplate.databinding.ActivityFeedDetailsBinding
import com.appetiser.baseplate.ext.loadAvatarUrl
import com.appetiser.baseplate.ext.loadImageUrl
import com.appetiser.module.common.*
import com.appetiser.module.domain.models.feed.Feed
import com.google.gson.Gson
import com.jakewharton.rxbinding3.widget.textChanges
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class FeedDetailsActivity : BaseViewModelActivity<ActivityFeedDetailsBinding, FeedDetailsViewModel>() {

    companion object {
        const val KEY_FEED_ID = "feed_id"
        const val KEY_COMMENT_CLICKED = "comment_clicked"
    }

    override fun getLayoutId(): Int = R.layout.activity_feed_details

    private val linearLayoutManager by lazy {
        LinearLayoutManager(this@FeedDetailsActivity, RecyclerView.VERTICAL, false)
    }

    @Inject
    lateinit var gson: Gson

    private lateinit var adapter: CommentsAdapter

    override fun canBack(): Boolean {
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupRecyclerView()
        setupViewModel()
        setupViews()

        binding.ivSend.ninjaTap {
            viewModel.postComment(binding.etComment.text.toString())
        }.addTo(disposables)
    }

    private fun setupViews() {
        with(binding.etComment) {
            setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    val comment = text.toString()
                    viewModel.postComment(comment)
                    true
                } else {
                    false
                }
            }

            textChanges()
                .skipInitialValue()
                .debounce(TEXT_WATCHER_DEBOUNCE_TIME, TimeUnit.MILLISECONDS)
                .subscribe {
                    if (it.isNotEmpty()) {
                        toggleSearchSend()
                    } else {
                        toggleSearchSend(false)
                    }
                }
                .addTo(disposables)
        }

        binding.heart
            .ninjaTap { viewModel.toggleFavorite() }
            .addTo(disposables)
    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_OK)
        super.onBackPressed()
    }

    private fun toggleSearchSend(isVisible: Boolean = true) {
        with(binding.ivSend) {
            val tempScaleY: Float
            val tempScaleX: Float
            if (isVisible) {
                if (scaleY >= 1 && scaleX >= 1) {
                    return@with
                }
                tempScaleX = 1f
                tempScaleY = 1f
            } else {
                if (scaleY <= 0 && scaleX <= 0) {
                    return@with
                }
                tempScaleX = 0f
                tempScaleY = 0f
            }

            post {
                animate()
                    .scaleX(tempScaleX)
                    .scaleY(tempScaleY)
                    .start()
            }
        }
    }

    private fun setupRecyclerView() {
        adapter = CommentsAdapter(this@FeedDetailsActivity, gson)
        with(binding.commentList) {
            layoutManager = linearLayoutManager
        }
        binding.commentList.adapter = adapter.withLoadStateFooter(
            footer = CommentsLoadStateAdapter(adapter)
        )
    }

    private fun setupViewModel() {
        binding.vm = viewModel

        viewModel.feedState
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = {
                    handleFeedDetailsState(it)
                }
            ).addTo(disposables)

        viewModel.commentState
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = {
                    handleCommentsState(it)
                }
            ).addTo(disposables)
    }

    private fun handleFeedDetailsState(state: FeedDetailsState) {
        when (state) {
            is FeedDetailsState.GetFeed -> {
                handleFeed(state.item)
            }

            is FeedDetailsState.UpdateFeed -> {
                handleFeed(state.item)
            }
        }
    }

    private fun handleCommentsState(state: CommentState) {
        when (state) {
            is CommentState.GetComments -> {
                adapter.updateItems(lifecycle, state.items)
            }

            is CommentState.ShowComment -> {
                scrollToBottomPosition()
            }

            is CommentState.ShowLastCommentItem -> {
                scrollToBottomPosition(true)
            }

            is CommentState.AddingComment -> {
                toggleSearchSend(false)
                binding.etComment.setText("")
                binding.etComment.hideKeyboardClearFocus()
            }

            is CommentState.AddCommentSuccess -> {
                adapter.refresh()
            }
        }
    }

    private fun scrollToBottomPosition(showLastCommentItem: Boolean = false) {
        with(binding.commentList) {
            postDelayed(
                {
                    if (showLastCommentItem) {
                        val height = (binding.appBarLayout.height + this.height)
                        scrollAnimation(height)
                    } else {
                        scrollAnimation(0)
                    }
                },
                1000
            )
        }
    }

    private fun scrollAnimation(scrollYPosition: Int) {
        val objectAnimator = ObjectAnimator.ofInt(binding.nestedScrollView, "scrollY", scrollYPosition)
            .setDuration(250)
        objectAnimator.interpolator = LinearInterpolator()
        objectAnimator.start()
    }

    private fun handleFeed(feed: Feed) {
        binding.avatar.loadAvatarUrl(feed.author.avatarPermanentThumbUrl)
        binding.name.text = feed.author.fullName
        binding.time.text = feed.createdAt.getTimeSpanString()
        binding.message.text = feed.body
        binding.image.loadImageUrl(feed.photo.url)
        binding.heartCount.text = feed.favoritesCount.toString()
        binding.commentCount.text = getString(R.string.comment_feed_format, feed.commentsCount)
        binding.heart.setImageResource(if (feed.isFavorite) R.drawable.ic_heart_selected else R.drawable.ic_heart_unselected)
    }
}
