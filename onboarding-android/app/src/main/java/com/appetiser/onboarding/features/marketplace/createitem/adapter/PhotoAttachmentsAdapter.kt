package com.appetiser.baseplate.features.marketplace.createitem.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import com.appetiser.baseplate.R
import com.appetiser.baseplate.base.BaseListAdapter
import com.appetiser.baseplate.databinding.ItemAddPhotoBtnBinding
import com.appetiser.baseplate.databinding.ItemReportUserPhotoBinding
import com.appetiser.baseplate.ext.loadImageUrl
import com.appetiser.module.common.ninjaTap

class PhotoAttachmentsAdapter(
    val context: Context,
    val listener: PhotoAttachmentCallback
) : BaseListAdapter<PhotoAttachmentsViewTypeObject, BaseListAdapter.BaseViewViewHolder<PhotoAttachmentsViewTypeObject>>(PhotoAttachmentsDiffCallback()) {

    companion object {
        private const val VIEW_TYPE_ADD_PHOTO_BTN = 0
        private const val VIEW_TYPE_DISPLAY_PHOTO = 1
    }

    override fun getItemViewType(position: Int): Int {
        return when (items[position].type) {
            PhotoAttachmentsViewType.DISPLAY_ADD_BTN -> VIEW_TYPE_ADD_PHOTO_BTN
            PhotoAttachmentsViewType.DISPLAY_PHOTO -> VIEW_TYPE_DISPLAY_PHOTO
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewViewHolder<PhotoAttachmentsViewTypeObject> {
        return when (viewType) {
            VIEW_TYPE_ADD_PHOTO_BTN -> createAddPhotoHolder(parent)
            else -> createDisplayPhotoHolder(parent)
        }
    }

    override fun onBindViewHolder(holder: BaseViewViewHolder<PhotoAttachmentsViewTypeObject>, position: Int) {
        holder.bind(items[position])
    }

    private fun createAddPhotoHolder(parent: ViewGroup): BaseViewViewHolder<PhotoAttachmentsViewTypeObject> {
        val view =
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.item_add_photo_btn, parent, false)

        val binding = ItemAddPhotoBtnBinding.bind(view)

        return AddPhotoItemViewHolder(binding)
    }

    private fun createDisplayPhotoHolder(parent: ViewGroup): BaseViewViewHolder<PhotoAttachmentsViewTypeObject> {
        val view =
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.item_report_user_photo, parent, false)

        val binding = ItemReportUserPhotoBinding.bind(view)

        return DisplayPhotoItemViewHolder(binding)
    }

    inner class AddPhotoItemViewHolder(override val binding: ItemAddPhotoBtnBinding) : BaseListAdapter.BaseViewViewHolder<PhotoAttachmentsViewTypeObject>(binding) {
        override fun bind(item: PhotoAttachmentsViewTypeObject) {
            super.bind(item)
            binding.addPhoto.ninjaTap {
                listener.addPhoto()
            }
        }
    }

    inner class DisplayPhotoItemViewHolder(override val binding: ItemReportUserPhotoBinding) : BaseListAdapter.BaseViewViewHolder<PhotoAttachmentsViewTypeObject>(binding) {
        override fun bind(item: PhotoAttachmentsViewTypeObject) {
            super.bind(item)
            binding.deletePhoto.ninjaTap {
                listener.deletePhoto(item)
            }

            binding.imageUserPhoto.loadImageUrl(item.imagePath)
        }
    }
}
