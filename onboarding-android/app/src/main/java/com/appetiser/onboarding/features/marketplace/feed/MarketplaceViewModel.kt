package com.appetiser.baseplate.features.marketplace.feed

import android.os.Bundle
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.rxjava2.cachedIn
import androidx.paging.rxjava2.observable
import com.appetiser.baseplate.base.BaseViewModel
import com.appetiser.baseplate.features.marketplace.feed.MarketplaceFeedState.Error
import com.appetiser.baseplate.features.marketplace.feed.MarketplaceFeedState.GetProducts
import com.appetiser.module.data.features.marketplace.MarketplaceRepository
import com.appetiser.module.payouts.data.PayoutsRepository
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class MarketplaceViewModel @Inject constructor(
    private val repository: MarketplaceRepository,
    private val payoutsRepository: PayoutsRepository
) : BaseViewModel() {

    private val pager by lazy {
        Pager(
            config = PagingConfig(
                pageSize = 20,
                prefetchDistance = 5,
                initialLoadSize = 20
            ),
            pagingSourceFactory = {
                MarketplaceFeedPagingSource(repository, schedulers)
            }
        )
    }

    private val _state by lazy {
        PublishSubject.create<MarketplaceFeedState>()
    }

    val state: Observable<MarketplaceFeedState> = _state

    override fun isFirstTimeUiCreate(bundle: Bundle?) {
        super.isFirstTimeUiCreate(bundle)

        pager.observable.cachedIn(viewModelScope)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribeBy(
                onNext = {
                    _state.onNext(GetProducts(it))
                },
                onError = {
                    _state.onNext(Error(it))
                }
            )
            .addTo(disposables)
    }

    fun fetchPayoutsState() {
        if (payoutsRepository.isPayoutsEnabled()) {
            _state.onNext(MarketplaceFeedState.CreateProductVisible)
        } else {
            _state.onNext(MarketplaceFeedState.CreateProductInvisible)
        }
    }
}
