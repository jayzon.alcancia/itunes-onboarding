package com.appetiser.baseplate.features.marketplace.feed

import androidx.paging.PagingData
import com.appetiser.module.domain.models.marketplace.Product

sealed class MarketplaceFeedState {
    data class GetProducts(val products: PagingData<Product>) : MarketplaceFeedState()

    data class Error(val throwable: Throwable) : MarketplaceFeedState()

    object CreateProductVisible : MarketplaceFeedState()

    object CreateProductInvisible : MarketplaceFeedState()
}
