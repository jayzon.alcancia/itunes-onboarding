package com.appetiser.onboarding.features.tracks

import com.appetiser.module.domain.models.places.Place
import com.appetiser.module.domain.models.tracks.Track

sealed class TracksState {
    object Loading : TracksState()

    object Success : TracksState()

    data class Error(val e: Throwable) : TracksState()
}