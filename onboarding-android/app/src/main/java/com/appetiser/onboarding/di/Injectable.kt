package com.appetiser.baseplate.di

/**
 * Marks an activity / fragment injectable.
 */
interface Injectable
