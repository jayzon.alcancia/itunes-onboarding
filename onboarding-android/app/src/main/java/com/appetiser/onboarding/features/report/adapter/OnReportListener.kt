package com.appetiser.baseplate.features.report.adapter

interface OnReportListener {

    fun addPhoto()

    fun deletePhoto(item: ReportAttachmentsViewTypeObject)
}
