package com.appetiser.baseplate.features.searchlocation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.appetiser.baseplate.base.BaseViewModel
import com.appetiser.module.data.features.places.PlacesRepository
import com.appetiser.module.domain.models.places.SearchPlace
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import javax.inject.Inject

class SearchLocationViewModel @Inject constructor(
    private val placesRepository: PlacesRepository
) : BaseViewModel() {

    private val _state by lazy {
        PublishSubject.create<SearchLocationState>()
    }

    val state: Observable<SearchLocationState> = _state

    private val _searchResults by lazy {
        MutableLiveData<List<SearchPlace>>()
    }

    val searchResults: LiveData<List<SearchPlace>> = _searchResults

    fun onKeywordChanged(keyword: String) {
        if (keyword.isEmpty()) return

        placesRepository
            .getPlaces(keyword)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe { _state.onNext(SearchLocationState.ShowLoading) }
            .doOnSuccess { _state.onNext(SearchLocationState.HideLoading) }
            .doOnError { _state.onNext(SearchLocationState.HideLoading) }
            .subscribeBy(
                onSuccess = ::handleGetPlacesResult,
                onError = {
                    Timber.e(it)
                    _state.onNext(SearchLocationState.Error)
                }
            )
            .addTo(disposables)
    }

    private fun handleGetPlacesResult(places: List<SearchPlace>) {
        if (places.isEmpty()) {
            // TODO: 12/17/20 Show empty state
            return
        }

        _searchResults.value = places
    }

    fun onSelectLocation(item: SearchPlace) {
        placesRepository
            .getPlacesDetails(item.placeId)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe { _state.onNext(SearchLocationState.ShowLoading) }
            .doOnSuccess { _state.onNext(SearchLocationState.HideLoading) }
            .doOnError { _state.onNext(SearchLocationState.HideLoading) }
            .subscribeBy(
                onSuccess = { placeDetails ->
                    _state.onNext(
                        SearchLocationState.SetLocationDetailsResult(
                            placeDetails
                        )
                    )
                },
                onError = {
                    Timber.e(it)
                    _state.onNext(SearchLocationState.Error)
                }
            )
            .addTo(disposables)
    }
}
