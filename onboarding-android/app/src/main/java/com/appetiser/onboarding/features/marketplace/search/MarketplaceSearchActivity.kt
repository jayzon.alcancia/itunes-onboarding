package com.appetiser.baseplate.features.marketplace.search

import android.annotation.SuppressLint
import android.content.IntentSender
import android.os.Bundle
import android.os.Looper
import android.view.MenuItem
import android.view.inputmethod.EditorInfo
import android.widget.ArrayAdapter
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.appetiser.baseplate.R
import com.appetiser.baseplate.base.BaseViewModelActivity
import com.appetiser.baseplate.databinding.ActivityMarketplaceSearchBinding
import com.appetiser.baseplate.features.marketplace.details.MarketplaceProductDetailsActivity
import com.appetiser.baseplate.features.marketplace.searchfilters.MarketplaceSearchFiltersDialog
import com.appetiser.module.common.NINJA_TAP_THROTTLE_TIME
import com.appetiser.module.common.TEXT_WATCHER_DEBOUNCE_TIME
import com.appetiser.module.common.showGenericErrorSnackBar
import com.appetiser.module.domain.models.marketplace.SearchFilters
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.material.badge.BadgeDrawable
import com.google.android.material.badge.BadgeUtils
import com.google.android.material.internal.ToolbarUtils
import com.jakewharton.rxbinding3.appcompat.itemClicks
import com.jakewharton.rxbinding3.appcompat.navigationClicks
import com.jakewharton.rxbinding3.widget.textChangeEvents
import com.tbruyelle.rxpermissions2.RxPermissions
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import java.util.concurrent.TimeUnit

class MarketplaceSearchActivity : BaseViewModelActivity<ActivityMarketplaceSearchBinding, MarketplaceSearchViewModel>() {

    companion object {
        private const val REQUEST_CODE_CHECK_SETTINGS = 1009
    }

    protected val rxPermissions: RxPermissions by lazy {
        RxPermissions(this)
    }

    private var fusedLocationClient: FusedLocationProviderClient? = null
    private var locationRequest: LocationRequest? = null

    private lateinit var menuFilter: MenuItem
    private var badgeDrawableOffset: Int = 0
    private var badgeDrawable: BadgeDrawable? = null

    // TODO: 12/15/20 Replace with Paging 3 lib
    private val listener = object : MarketplaceSearchAdapter.Listener {
        override fun onItemClicked(productId: Long) {
            MarketplaceProductDetailsActivity.openActivity(
                this@MarketplaceSearchActivity,
                productId
            )
        }
    }
    private var adapter: MarketplaceSearchAdapter? = null

    override fun getLayoutId() = R.layout.activity_marketplace_search

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Timber.tag("onCreate").d("")

        setupToolbar()
        setupViews()
        setupVmObservers()

        startLoadCurrentLocation()
    }

    override fun onDestroy() {
        adapter = null
        super.onDestroy()
    }

    private fun startLoadCurrentLocation() {
        rxPermissions
            .requestEachCombined(android.Manifest.permission.ACCESS_FINE_LOCATION)
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = { permission ->
                    when {
                        permission.granted -> {
                            loadCurrentLocation()
                        }
                        permission.shouldShowRequestPermissionRationale -> {
                            // TODO: 12/10/20 Show require permission message.
                        }
                        else -> {
                            // At least 1 permission was denied and marked "never ask again"
                            // User needs to go to settings
                            // TODO: 12/10/20 Show require permission message.
                        }
                    }

                    // Load recent searches regardless whether we have location permission or not.
                    viewModel.loadSearchScreen()
                },
                onError = {
                    Timber.e(it)
                    showGenericErrorSnackBar(
                        binding.root,
                        getString(R.string.generic_error_short)
                    )
                }
            )
            .addTo(disposables)
    }

    private fun loadCurrentLocation() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        locationRequest = LocationRequest().apply {
            interval = 10000
            fastestInterval = 5000
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }
        val settingsBuilder = LocationSettingsRequest.Builder()
            .addLocationRequest(locationRequest!!)

        val client = LocationServices.getSettingsClient(this)
        val task = client.checkLocationSettings(settingsBuilder.build())

        task.addOnSuccessListener {
            initializeLocationRequest()
        }
        task.addOnFailureListener { exception ->
            if (exception is ResolvableApiException) {
                // Location settings are not satisfied,
                // but this can be fixed by showing the user dialog.
                try {
                    // Show the dialog by calling startResolutionForResult(),
                    // and check the result on onActivityResult().
                    exception.startResolutionForResult(
                        this,
                        REQUEST_CODE_CHECK_SETTINGS
                    )
                } catch (ex: IntentSender.SendIntentException) {
                    // Ignore the error.
                }
            }
        }
    }

    @SuppressLint("MissingPermission")
    private fun initializeLocationRequest() {
        fusedLocationClient!!.requestLocationUpdates(
            locationRequest,
            object : LocationCallback() {
                override fun onLocationResult(locationResult: LocationResult?) {
                    locationResult?.lastLocation ?: return

                    viewModel.onLocationResult(
                        locationResult.lastLocation.latitude,
                        locationResult.lastLocation.longitude
                    )

                    fusedLocationClient!!.removeLocationUpdates(this)
                }
            },
            Looper.getMainLooper()
        )
    }

    private fun setupToolbar() {
        binding.toolbar.apply {
            navigationClicks()
                .throttleFirst(NINJA_TAP_THROTTLE_TIME, TimeUnit.MILLISECONDS)
                .observeOn(scheduler.ui())
                .subscribeBy(
                    onNext = {
                        finish()
                    },
                    onError = {
                        Timber.e(it)
                    }
                )
                .addTo(disposables)

            itemClicks()
                .throttleFirst(NINJA_TAP_THROTTLE_TIME, TimeUnit.MILLISECONDS)
                .observeOn(scheduler.ui())
                .subscribeBy(
                    onNext = {
                        viewModel.onFilterClick()
                    },
                    onError = {
                        Timber.e(it)
                    }
                )
                .addTo(disposables)

            val menuView = ToolbarUtils.getActionMenuItemView(this, R.id.itemFilters)!!
            menuView.post {
                badgeDrawableOffset = (menuView.height * .1).toInt()
                menuFilter = menu.findItem(R.id.itemFilters)
                menuFilter.isVisible = false // Hide by default
            }
        }
    }

    private fun setupVmObservers() {
        viewModel
            .state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = ::handleState,
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }

    @SuppressLint("UnsafeExperimentalUsageError")
    private fun handleState(state: SearchState) {
        when (state) {
            is SearchState.DisplayRecentSearches -> {
                displayRecentSearches(state.recentSearches)
            }
            SearchState.ShowFilterMenu -> {
                menuFilter.icon = ContextCompat.getDrawable(this, R.drawable.ic_filter_default)
                menuFilter.isVisible = true
            }
            is SearchState.ShowFilterCount -> {
                menuFilter.icon = ContextCompat.getDrawable(this, R.drawable.ic_filter_active)

                badgeDrawable = BadgeDrawable.create(this@MarketplaceSearchActivity).apply {
                    number = state.filterCount
                    backgroundColor = getColor(R.color.cerulean_blue)
                    badgeTextColor = getColor(R.color.white)
                    badgeGravity = BadgeDrawable.BOTTOM_END
                    maxCharacterCount = 2
                    verticalOffset = badgeDrawableOffset
                    horizontalOffset = badgeDrawableOffset
                }

                BadgeUtils.attachBadgeDrawable(
                    badgeDrawable!!,
                    binding.toolbar,
                    R.id.itemFilters
                )
            }
            is SearchState.ShowFilterDialog -> {
                showFiltersDialog(state.searchFilters)
            }
            SearchState.HideFilterCount -> {
                menuFilter.icon = ContextCompat.getDrawable(this, R.drawable.ic_filter_default)

                BadgeUtils.detachBadgeDrawable(
                    badgeDrawable,
                    binding.toolbar,
                    R.id.itemFilters
                )
            }
            SearchState.HideFilterMenu -> {
                menuFilter.isVisible = false
            }
            is SearchState.ShowSearchResults -> {
                adapter?.submitList(state.searchResults)
            }
            SearchState.Error -> {
                showGenericErrorSnackBar(
                    binding.root,
                    getString(R.string.generic_error_short)
                )
            }
        }
    }

    private fun showFiltersDialog(searchFilters: SearchFilters) {
        MarketplaceSearchFiltersDialog
            .newInstance(searchFilters)
            .apply {
                setOnApplyFilterCallback { searchFilters ->
                    viewModel.onApplyFilters(searchFilters)
                }
                show(supportFragmentManager, null)
            }
    }

    private fun displayRecentSearches(recentSearches: List<String>) {
        binding.inputAutoCompleteSearch.apply {
            val adapter = ArrayAdapter(
                this@MarketplaceSearchActivity,
                R.layout.item_recent_search,
                R.id.txtRecentSearch,
                recentSearches
            )
            setAdapter(adapter)
            showDropDown()
        }
    }

    private fun setupViews() {
        binding.recyclerView.apply {
            layoutManager = LinearLayoutManager(this@MarketplaceSearchActivity)

            this@MarketplaceSearchActivity.adapter = MarketplaceSearchAdapter(listener)

            adapter = this@MarketplaceSearchActivity.adapter
        }

        setupSearchView()
    }

    private fun setupSearchView() {
        binding.inputAutoCompleteSearch.apply {
            setOnEditorActionListener { _, keyCode, _ ->
                if (keyCode == EditorInfo.IME_ACTION_SEARCH) {
                    viewModel.search(text.toString())
                    return@setOnEditorActionListener true
                }
                return@setOnEditorActionListener false
            }

            threshold = 1

            textChangeEvents()
                .skipInitialValue()
                .debounce(TEXT_WATCHER_DEBOUNCE_TIME, TimeUnit.MILLISECONDS)
                .observeOn(scheduler.ui())
                .subscribeBy(
                    onNext = { event ->
                        if (event.text.isEmpty()) {
                            // Show recent searches when input is empty.
                            showDropDown()
                        }
                    },
                    onError = {
                        Timber.e(it)
                    }
                )
                .addTo(disposables)

            setOnFocusChangeListener { _, isFocused ->
                if (isFocused) {
                    showDropDown()
                }
            }
        }
    }
}
