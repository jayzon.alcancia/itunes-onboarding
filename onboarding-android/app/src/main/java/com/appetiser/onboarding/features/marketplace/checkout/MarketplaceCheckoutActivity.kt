package com.appetiser.baseplate.features.marketplace.checkout

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.appetiser.baseplate.R
import com.appetiser.baseplate.base.BaseViewModelActivity
import com.appetiser.baseplate.databinding.ActivityMarketplaceCheckoutBinding
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy

class MarketplaceCheckoutActivity : BaseViewModelActivity<ActivityMarketplaceCheckoutBinding,
    MarketplaceCheckoutViewModel>() {

    companion object {
        fun openActivity(context: Context, productId: Long) {
            val intent = Intent(
                context,
                MarketplaceCheckoutActivity::class.java
            )
            intent.putExtra(KEY_PRODUCT_ID, productId)
            context.startActivity(intent)
        }

        private const val KEY_PRODUCT_ID = "KEY_PRODUCT_ID"
    }

    override fun canBack(): Boolean {
        return true
    }

    override fun getLayoutId(): Int = R.layout.activity_marketplace_checkout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupToolbar()
        setupViewModel()
    }

    private fun setupToolbar() {
        setToolbarTitle(getString(R.string.checkout))
    }

    private fun setupViewModel() {
        viewModel.state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = {
                    handleState(it)
                }
            ).addTo(disposables)
    }

    private fun handleState(state: MarketplaceCheckoutState) {}
}