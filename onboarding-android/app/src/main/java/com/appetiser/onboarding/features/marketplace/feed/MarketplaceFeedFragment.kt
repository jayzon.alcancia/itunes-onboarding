package com.appetiser.baseplate.features.marketplace.feed

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import com.appetiser.baseplate.R
import com.appetiser.baseplate.base.BaseViewModelFragment
import com.appetiser.baseplate.databinding.FragmentMarketplaceFeedBinding
import com.appetiser.baseplate.features.marketplace.createitem.CreateMarketplaceItemActivity
import com.appetiser.baseplate.features.marketplace.details.MarketplaceProductDetailsActivity
import com.appetiser.baseplate.features.marketplace.search.MarketplaceSearchActivity
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber

class MarketplaceFeedFragment :
    BaseViewModelFragment<FragmentMarketplaceFeedBinding, MarketplaceViewModel>() {

    override fun getLayoutId() = R.layout.fragment_marketplace_feed

    private val adapter by lazy { MarketplaceFeedAdapter(listener) }

    private val listener = object : MarketplaceFeedAdapter.Listener {
        override fun onItemClicked(id: Long) {
            MarketplaceProductDetailsActivity.openActivity(requireContext(), id)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
        setupViewModel()

        setupToolbarAndStatusBar()
    }

    override fun onResume() {
        super.onResume()
        viewModel.fetchPayoutsState()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.marketplace_menu, menu)
    }

    private fun setupViews() {
        binding.feed.adapter = adapter

        binding.txtSearch.apply {
            setOnClickListener {
                startActivity(
                    Intent(
                        requireContext(),
                        MarketplaceSearchActivity::class.java
                    )
                )
            }
        }
    }

    private fun setupViewModel() {
        viewModel.state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = {
                    when (it) {
                        is MarketplaceFeedState.GetProducts -> {
                            adapter.submitData(lifecycle, it.products)
                        }
                        is MarketplaceFeedState.Error -> {
                            Timber.e(it.throwable)
                        }

                        is MarketplaceFeedState.CreateProductVisible -> {
                            binding.toolbarView.menu.findItem(R.id.action_add).isVisible = true
                        }
                        is MarketplaceFeedState.CreateProductInvisible -> {
                            binding.toolbarView.menu.findItem(R.id.action_add).isVisible = false
                        }
                    }
                }
            ).addTo(disposables)
    }

    private fun setupToolbarAndStatusBar() {
        binding.toolbarView.title = ""

        binding.toolbarView.inflateMenu(R.menu.marketplace_menu)
        binding.toolbarView.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.action_add -> {
                    CreateMarketplaceItemActivity.openActivity(requireContext())
                    true
                }
                else -> {
                    super.onOptionsItemSelected(it)
                }
            }
        }
    }
}
