package com.appetiser.baseplate.features.searchlocation

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.activity.result.contract.ActivityResultContract
import com.appetiser.module.domain.models.places.Place
import com.google.gson.Gson

class SearchLocationContract : ActivityResultContract<Unit, Place?>() {
    companion object {
        const val EXTRA_PLACE_JSON = "EXTRA_PLACE_JSON"
    }

    override fun createIntent(context: Context, input: Unit?): Intent {
        return Intent(context, SearchLocationActivity::class.java)
    }

    override fun parseResult(resultCode: Int, intent: Intent?): Place? {
        if (resultCode != Activity.RESULT_OK) {
            return null
        }

        if (intent == null) return null

        if (intent.extras == null) return null

        val placeJson = intent.extras!!.getString(EXTRA_PLACE_JSON, "{}")

        return Gson().fromJson(placeJson, Place::class.java)
    }
}
