package com.appetiser.module.local.features.track

import android.content.SharedPreferences
import androidx.core.content.edit
import com.appetiser.module.domain.models.tracks.Track
import com.appetiser.module.local.features.track.dao.TracksDao
import com.appetiser.module.local.features.track.models.TrackDB
import com.google.gson.Gson
import io.reactivex.Single
import javax.inject.Inject

class TracksLocalSourceImpl @Inject constructor(
    private val tracksDao: TracksDao,
    private val sharedPreferences: SharedPreferences,
) : TracksLocalSource {

    companion object {
        private const val PREF_TRACK_TOKEN = "PREF_TRACK_TOKEN"
    }

    override fun saveAllItunesTracksLocally(list: List<TrackDB>) {
        tracksDao.insetAllTracks(list)
    }

    override fun getAllItunesTracksLocally() = tracksDao.getAllTracks()

    override fun saveTrackToCache(trackLocal: TrackDB) {
      sharedPreferences.edit {
          putString(PREF_TRACK_TOKEN, Gson().toJson(trackLocal))
          apply()
      }
    }

    override fun getTrackFromCache() = Gson().fromJson(sharedPreferences.getString(PREF_TRACK_TOKEN, null), TrackDB::class.java)

    override fun clearTrack() {
        sharedPreferences.edit {
            clear()
            apply()
        }
    }
}