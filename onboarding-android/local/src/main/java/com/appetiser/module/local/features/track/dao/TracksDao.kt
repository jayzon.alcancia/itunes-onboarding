package com.appetiser.module.local.features.track.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.appetiser.module.local.base.BaseDao
import com.appetiser.module.local.features.track.models.TrackDB
import io.reactivex.Single

@Dao
abstract class TracksDao : BaseDao<TrackDB> {
    @Query("SELECT * FROM ${TrackDB.TRACKS_TABLE_NAME}")
    abstract fun getAllTracks(): Single<List<TrackDB>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insetAllTracks(tracks: List<TrackDB>)
}