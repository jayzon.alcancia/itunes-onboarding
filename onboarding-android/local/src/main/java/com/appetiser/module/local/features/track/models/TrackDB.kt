package com.appetiser.module.local.features.track.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.appetiser.module.domain.models.token.AccessToken
import com.appetiser.module.domain.models.tracks.Track
import com.appetiser.module.local.features.token.models.AccessTokenDB

@Entity(tableName = TrackDB.TRACKS_TABLE_NAME)
data class TrackDB(
    @PrimaryKey
    val id: Int,
    val artistName: String,
    val trackName: String,
    val artworkUrl30: String,
    val artworkUrl60: String,
    val artworkUrl100: String,
    val trackPrice: Double,
    val genre: String,
    val longDescription: String
) {
    companion object {
        const val TRACKS_TABLE_NAME = "track"

        fun TrackDB.toDomain() = Track(
                id, artistName, trackName, artworkUrl30, artworkUrl60, artworkUrl100, trackPrice, genre, longDescription
            )



        fun Track.fromDomain() = TrackDB(
                id, artistName, trackName, artworkUrl30, artworkUrl60, artworkUrl100, trackPrice, genre, longDescription
            )

    }
}
