package com.appetiser.module.local

import android.app.Application
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import com.appetiser.module.local.features.AppDatabase
import com.appetiser.module.local.features.session.SessionLocalSource
import com.appetiser.module.local.features.session.SessionLocalSourceImpl
import com.appetiser.module.local.features.token.AccessTokenLocalSource
import com.appetiser.module.local.features.token.AccessTokenLocalSourceImpl
import com.appetiser.module.local.features.track.TracksLocalSource
import com.appetiser.module.local.features.track.TracksLocalSourceImpl
import com.appetiser.module.local.features.user.UserLocalSource
import com.appetiser.module.local.features.user.UserLocalSourceImpl
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class StorageModule {

    @Provides
    @Singleton
    fun providesSharedPreferences(application: Application): SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(application.applicationContext)
    }

    @Provides
    @Singleton
    fun providesUserLocalSource(appDatabase: AppDatabase): UserLocalSource {
        return UserLocalSourceImpl(appDatabase.userDao())
    }

    @Provides
    @Singleton
    fun providesAccessTokenLocalSource(
        sharedPreferences: SharedPreferences,
        appDatabase: AppDatabase
    ): AccessTokenLocalSource {
        return AccessTokenLocalSourceImpl(
            sharedPreferences,
            appDatabase.tokenDao()
        )
    }

    @Provides
    @Singleton
    fun providesSessionLocalSource(
        userLocalSource: UserLocalSource,
        accessTokenLocalSource: AccessTokenLocalSource,
        sharedPreferences: SharedPreferences,
        gson: Gson
    ): SessionLocalSource {
        return SessionLocalSourceImpl(
            userLocalSource,
            accessTokenLocalSource,
            sharedPreferences,
            gson
        )
    }

    @Provides
    @Singleton
    fun providesTrackLocalSource(
        appDatabase: AppDatabase,
        sharedPreferences: SharedPreferences,
    ): TracksLocalSource {
        return TracksLocalSourceImpl(
            appDatabase.tracksDao(),
            sharedPreferences
        )
    }
}
