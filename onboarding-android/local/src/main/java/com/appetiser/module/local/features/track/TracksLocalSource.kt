package com.appetiser.module.local.features.track

import com.appetiser.module.local.features.track.models.TrackDB
import io.reactivex.Single

interface TracksLocalSource {
    fun saveAllItunesTracksLocally(list: List<TrackDB>)

    fun getAllItunesTracksLocally(): Single<List<TrackDB>>

    fun saveTrackToCache(trackLocal: TrackDB)

    fun getTrackFromCache(): TrackDB?

    fun clearTrack()
}