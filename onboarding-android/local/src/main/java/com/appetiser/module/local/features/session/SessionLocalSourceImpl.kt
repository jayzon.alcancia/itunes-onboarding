package com.appetiser.module.local.features.session

import android.content.SharedPreferences
import com.appetiser.module.domain.models.Session
import com.appetiser.module.local.features.token.AccessTokenLocalSource
import com.appetiser.module.local.features.user.UserLocalSource
import com.appetiser.module.local.utils.PREF_RECENT_SEARCHES
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

class SessionLocalSourceImpl @Inject constructor(
    private val userLocalSource: UserLocalSource,
    private val accessTokenLocalSource: AccessTokenLocalSource,
    private val sharedPreferences: SharedPreferences,
    private val gson: Gson
) : SessionLocalSource {

    var session: Session? = null

    override fun getSession(): Single<Session> {
        if (session == null) {
            return getSessionFromDb()
                .doOnSuccess { this.session = it }
        }

        return Single.just(session)
    }

    override fun saveSession(session: Session): Single<Session> {
        return Single
            .zip(
                userLocalSource.saveUser(session.user),
                accessTokenLocalSource.saveAccessToken(session.accessToken),
                { user, accessToken ->
                    Pair(user, accessToken)
                }
            )
            .map { pair ->
                session.saveSessionFlags()
                pair
            }
            .map { pair ->
                session.user = pair.first
                session.accessToken = pair.second

                this.session = session
                this.session
            }
    }

    override fun getUserToken(): String {
        return accessTokenLocalSource.getAccessTokenFromPref()
    }

    override fun clearSession(): Completable {
        return userLocalSource
            .deleteUser()
            .andThen(accessTokenLocalSource.deleteToken())
            .doOnComplete {
                this.session = null
            }
    }

    /**
     * Saves session flags to shared preferences.
     */
    private fun Session.saveSessionFlags() {
        sharedPreferences.edit().apply {
            putString(
                PREF_RECENT_SEARCHES,
                gson.toJson(recentSearches)
            )
            apply()
        }
    }

    /**
     * Loads session flags from shared preferences.
     */
    private fun Session.loadSessionFlags() {
        recentSearches = gson.fromJson(
            sharedPreferences.getString(PREF_RECENT_SEARCHES, "[]"),
            object : TypeToken<MutableList<String>>() {}.type
        )
    }

    private fun getSessionFromDb(): Single<Session> {
        return Single
            .zip(
                userLocalSource.getUser(),
                accessTokenLocalSource.getAccessToken(),
                { user, accessToken ->
                    val session = Session(user, accessToken)
                    session
                }
            )
            .map { session ->
                session.loadSessionFlags()
                session
            }
    }
}
