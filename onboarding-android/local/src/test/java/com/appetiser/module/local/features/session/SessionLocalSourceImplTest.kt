package com.appetiser.module.local.features.session

import com.appetiser.module.domain.utils.mock
import com.appetiser.module.local.features.token.AccessTokenLocalSource
import com.appetiser.module.local.features.user.UserLocalSource
import io.reactivex.Completable
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.`when`

class SessionLocalSourceImplTest {

    private val userLocalSource: UserLocalSource = mock()
    private val accessTokenLocalSource: AccessTokenLocalSource = mock()

    private lateinit var subject: SessionLocalSource

    @Before
    fun setUp() {
        subject = SessionLocalSourceImpl(userLocalSource, accessTokenLocalSource)
    }

    @Test
    fun logout_ShouldLogoutUserAndToken() {
        `when`(userLocalSource.deleteUser())
            .thenReturn(Completable.complete())
        `when`(accessTokenLocalSource.deleteToken())
            .thenReturn(Completable.complete())

        subject
            .clearSession()
            .test()
            .assertComplete()

        Mockito
            .verify(
                userLocalSource,
                Mockito.times(1)
            )
            .deleteUser()
        Mockito
            .verify(
                accessTokenLocalSource,
                Mockito.times(1)
            )
            .deleteToken()
    }
}
