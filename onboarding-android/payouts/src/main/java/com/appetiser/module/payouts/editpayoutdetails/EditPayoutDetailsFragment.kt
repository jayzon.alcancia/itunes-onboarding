package com.appetiser.module.payouts.editpayoutdetails

import android.app.DatePickerDialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.appetiser.module.common.ninjaTap
import com.appetiser.module.payouts.PayoutsActivity
import com.appetiser.module.payouts.PayoutsState
import com.appetiser.module.payouts.PayoutsState.*
import com.appetiser.module.payouts.R
import com.appetiser.module.payouts.data.PayoutDetails
import com.appetiser.module.payouts.data.PayoutDetails.*
import com.mukesh.countrypicker.CountryPicker
import dagger.android.support.AndroidSupportInjection
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

class EditPayoutDetailsFragment : Fragment() {

    private val countryPicker by lazy {
        CountryPicker.Builder()
            .with(requireContext())
            .listener {
                requireView().findViewById<TextView>(R.id.country).text = it.name
            }
            .build()
    }
    private val disposables: CompositeDisposable = CompositeDisposable()
    private val shortDateFormatter by lazy { DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT) }
    private val viewModel by lazy {
        (requireActivity() as PayoutsActivity).viewModel
    }

    private var birthDay = 0
    private var birthMonth = 0
    private var birthYear = 0

    private lateinit var datePickerDialog: DatePickerDialog
    private lateinit var externalAccountId: String

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_edit_payout_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpViews()
        startObservers()
    }

    private fun startObservers() {
        viewModel
            .state
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onNext = ::handleState,
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }

    private fun setUpViews() {
        requireActivity().run {
            findViewById<TextView>(R.id.toolbarTitle).text = getString(R.string.edit_payout_details)

            findViewById<TextView>(R.id.toolbarButton).apply {
                text = getString(R.string.save)
                visibility = View.VISIBLE
            }


            findViewById<TextView>(R.id.toolbarButton).ninjaTap {
                viewModel.updatePayoutDetails(getPayoutDetailsFromUi())
            }

            findViewById<TextView>(R.id.country).ninjaTap {
                countryPicker.showBottomSheet(requireActivity() as AppCompatActivity)
            }
        }
    }

    private fun getPayoutDetailsFromUi(): PayoutDetails {
        requireActivity().run {
            val country = countryPicker.getCountryByName(findViewById<TextView>(R.id.country).text.toString())

            return PayoutDetails(
                BankDetails(
                    id = externalAccountId,
                    accountHolderName = findViewById<EditText>(R.id.accountHolderName).text.toString(),
                    bsb = findViewById<EditText>(R.id.bsb).text.toString(),
                    accountNumber = findViewById<EditText>(R.id.accountNumber).text.toString()
                ),
                OwnerDetails(
                    firstName = findViewById<EditText>(R.id.firstName).text.toString(),
                    lastName = findViewById<EditText>(R.id.lastName).text.toString(),
                    birthDay = birthDay,
                    birthMonth = birthMonth,
                    birthYear = birthYear
                ),
                OwnerAddress(
                    street = findViewById<EditText>(R.id.accountHolderName).text.toString(),
                    city = findViewById<EditText>(R.id.citySuburb).text.toString(),
                    state = findViewById<EditText>(R.id.state).text.toString(),
                    postCode = findViewById<EditText>(R.id.postCode).text.toString(),
                    country = country.code
                )
            )
        }
    }

    private fun handleState(state: PayoutsState) {
        when (state) {
            GoBackToSettingsScreen -> {
                Toast.makeText(
                    requireContext(),
                    getString(R.string.payout_details_updated),
                    Toast.LENGTH_LONG
                ).show()
                requireActivity().finish()
            }

            is ShowPayoutDetails -> {
                state.details.ownerDetails.let {
                    birthDay = it.birthDay
                    birthMonth = it.birthMonth
                    birthYear = it.birthYear
                }

                showPayoutDetails(state.details)
            }

            is ShowErrorMessage -> {
                Toast.makeText(
                    requireContext(),
                    state.errorMessage ?: getString(R.string.generic_error_short),
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }

    private fun showPayoutDetails(details: PayoutDetails) {
        externalAccountId = details.bankDetails!!.id!!

        requireView().run {
            // Bank Details
            findViewById<EditText>(R.id.accountHolderName).setText(details.bankDetails.accountHolderName)
            findViewById<EditText>(R.id.bsb).setText(details.bankDetails.bsb)
            findViewById<EditText>(R.id.accountNumber).setText(details.bankDetails.accountNumber)

            // Owner Details
            findViewById<EditText>(R.id.firstName).setText(details.ownerDetails.firstName)
            findViewById<EditText>(R.id.lastName).setText(details.ownerDetails.lastName)

            findViewById<EditText>(R.id.dateOfBirth).setText(
                shortDateFormatter.format(
                    LocalDate.of(
                        details.ownerDetails.birthYear,
                        details.ownerDetails.birthMonth,
                        details.ownerDetails.birthDay
                    )
                )
            )

            findViewById<EditText>(R.id.dateOfBirth).let {
                datePickerDialog = DatePickerDialog(
                    requireContext(),
                    { _, year, month, day ->
                        (it as EditText).setText(
                            shortDateFormatter.format(
                                LocalDate.of(
                                    year,
                                    month,
                                    day
                                )
                            )
                        )

                        birthDay = year
                        birthMonth = month
                        birthYear = day
                    },
                    details.ownerDetails.birthYear,
                    details.ownerDetails.birthMonth - 1,
                    details.ownerDetails.birthDay
                )

                it.setText(
                    shortDateFormatter.format(
                        LocalDate.of(
                            details.ownerDetails.birthYear,
                            details.ownerDetails.birthMonth,
                            details.ownerDetails.birthDay
                        )
                    )
                )

                it.ninjaTap {
                    datePickerDialog.show()
                }
            }

            // Owner Address
            findViewById<EditText>(R.id.streetAddress).setText(details.ownerAddress.street)
            findViewById<EditText>(R.id.citySuburb).setText(details.ownerAddress.city)
            findViewById<EditText>(R.id.state).setText(details.ownerAddress.state)
            findViewById<EditText>(R.id.postCode).setText(details.ownerAddress.postCode)

            val country = countryPicker.getCountryByISO(details.ownerAddress.country)
            findViewById<TextView>(R.id.country).text = country.name
        }
    }
}
