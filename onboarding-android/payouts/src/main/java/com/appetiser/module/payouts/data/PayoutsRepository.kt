package com.appetiser.module.payouts.data

import com.appetiser.module.local.features.session.SessionLocalSource
import com.appetiser.module.network.features.payouts.IdVerificationResponse
import com.appetiser.module.payouts.PayoutsState
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import org.json.JSONObject
import retrofit2.HttpException
import javax.inject.Inject

class PayoutsRepository @Inject constructor(
    private val sessionLocalSource: SessionLocalSource,
    private val payoutsRemoteSource: PayoutsRemoteSource,
    private val payoutsLocalSource: PayoutsLocalSource,
    private val payoutsMapper: PayoutsMapper
) {

    fun savePayoutDetails(payoutDetails: PayoutDetails): Completable {
        return payoutsRemoteSource.savePayoutDetails(
            getToken(),
            payoutsMapper.domainToRemotePostPayoutDetails(payoutDetails)
        ).doOnComplete {
            payoutsLocalSource.enablePayouts()
        }
    }

    fun uploadVerificationDocument(filePath: String): Single<IdVerificationResponse> {
        return payoutsRemoteSource.uploadVerificationDocument(
            getToken(),
            filePath
        )
    }

    fun updatePayoutDetails(payoutDetails: PayoutDetails): Completable {
        return payoutsRemoteSource.updatePayoutDetails(
            getToken(),
            payoutsMapper.domainToRemoteUpdatePayoutDetails(payoutDetails),
            payoutsMapper.domainToRemoteUpdateExternalAccount(payoutDetails)
        )
    }

    fun getPayoutDetails(): Single<PayoutDetails> {
        return payoutsRemoteSource
            .getPayoutDetails(getToken())
            .map {
                payoutsMapper.remoteToDomain(it)
            }
            .doOnSuccess {
                payoutsLocalSource.enablePayouts()
            }
            .doOnError {
                if (it is HttpException) {
                    if (it.code() == 404) {
                        payoutsLocalSource.disablePayouts()
                    }
                }
            }
    }


    fun isPayoutsEnabled(): Boolean {
        if (!payoutsLocalSource.isPayoutsEnabled()) {
            // First Run,
            val payoutDetails = getPayoutDetails()
                .subscribeOn(Schedulers.io())
                .onErrorReturnItem(
                    PayoutDetails(
                        ownerAddress = PayoutDetails.emptyOwnerAddress(),
                        ownerDetails = PayoutDetails.emptyOwnerDetails())
                )
                .blockingGet()

            if (payoutDetails.bankDetails == null) {
                return false
            }
        }

        return true
    }

    fun disablePayouts() {
        payoutsLocalSource.disablePayouts()
    }

    fun deleteAccount(): Completable {
        return payoutsRemoteSource.deleteAccount(getToken())
            .doOnComplete {
                payoutsLocalSource.disablePayouts()
            }
    }

    private fun getToken() = "Bearer ${sessionLocalSource.getUserToken()}"
}
