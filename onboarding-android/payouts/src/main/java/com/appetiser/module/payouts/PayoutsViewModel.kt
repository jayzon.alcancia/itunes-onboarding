package com.appetiser.module.payouts

import androidx.lifecycle.ViewModel
import com.appetiser.module.network.features.payouts.IdVerificationResponse
import com.appetiser.module.payouts.data.PayoutDetails
import com.appetiser.module.payouts.data.PayoutsRepository
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.BiFunction
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import org.json.JSONObject
import retrofit2.HttpException
import timber.log.Timber
import javax.inject.Inject

class PayoutsViewModel @Inject constructor(private val repository: PayoutsRepository) :
    ViewModel() {

    private val disposables: CompositeDisposable = CompositeDisposable()

    private val _state by lazy {
        BehaviorSubject.create<PayoutsState>()
    }

    val state: Observable<PayoutsState> = _state


    fun savePayoutDetails(payoutDetails: PayoutDetails) {
        var uploadedFrontPhoto = ""
        var uploadedBackPhoto = ""

        Single.zip(
            repository.uploadVerificationDocument(payoutDetails.idVerification!!.frontPhoto),
            repository.uploadVerificationDocument(payoutDetails.idVerification.backPhoto),
            BiFunction { front: IdVerificationResponse, back: IdVerificationResponse ->
                uploadedFrontPhoto = front.data.id
                uploadedBackPhoto = back.data.id
            }
        ).flatMapCompletable {
            repository.savePayoutDetails(
                payoutDetails.copy(
                    idVerification = PayoutDetails.IdVerification(
                        uploadedFrontPhoto,
                        uploadedBackPhoto
                    )
                )
            )
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onComplete = {
                    _state.onNext(PayoutsState.GoBackToSettingsScreen)
                },
                onError = {
                    Timber.e(it)

                    (it as HttpException).response()?.errorBody()?.string()?.let { error ->
                        val json = JSONObject(error)

                        if (!json.isNull("message")) {
                            _state.onNext(
                                PayoutsState.ShowErrorMessage(
                                    json.getString(
                                        "message"
                                    )
                                )
                            )
                        } else {
                            _state.onNext(PayoutsState.ShowErrorMessage(null))
                        }
                    }
                }
            )
            .let { disposables.add(it) }
    }

    fun getPayoutDetails() {
        repository.getPayoutDetails()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onSuccess = {
                    _state.onNext(PayoutsState.ShowPayoutDetails(it))
                },
                onError = {
                    Timber.e(it)

                    if (it is HttpException) {
                        if (it.code() == 404) {
                            _state.onNext(PayoutsState.GoToCreatePaymentDetailsScreen)
                        } else {
                            it.response()?.errorBody()?.string()?.let { error ->
                                val json = JSONObject(error)

                                if (!json.isNull("message")) {
                                    _state.onNext(
                                        PayoutsState.ShowErrorMessage(
                                            json.getString(
                                                "message"
                                            )
                                        )
                                    )
                                } else {
                                    _state.onNext(PayoutsState.ShowErrorMessage(null))
                                }
                            }
                        }
                    } else {
                        _state.onNext(PayoutsState.ShowErrorMessage(null))
                    }
                }
            )
            .let { disposables.add(it) }
    }

    fun updatePayoutDetails(payoutDetails: PayoutDetails) {
        repository.updatePayoutDetails(payoutDetails).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onComplete = {
                    _state.onNext(PayoutsState.GoBackToSettingsScreen)
                },
                onError = {
                    Timber.e(it)

                    (it as HttpException).response()?.errorBody()?.string()?.let { error ->
                        val json = JSONObject(error)

                        if (!json.isNull("message")) {
                            _state.onNext(
                                PayoutsState.ShowErrorMessage(
                                    json.getString(
                                        "message"
                                    )
                                )
                            )
                        } else {
                            _state.onNext(PayoutsState.ShowErrorMessage(null))
                        }
                    }
                }
            )
            .let { disposables.add(it) }
    }

    fun deleteAccount() {
        repository.deleteAccount()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onComplete = {
                    _state.onNext(PayoutsState.GoBackToSettingsScreen)
                },
                onError = {
                    Timber.e(it)

                    if (it is HttpException) {
                        if (it.code() == 404) {
                            _state.onNext(PayoutsState.GoToCreatePaymentDetailsScreen)
                        } else {
                            it.response()?.errorBody()?.string()?.let { error ->
                                val json = JSONObject(error)

                                if (!json.isNull("message")) {
                                    _state.onNext(
                                        PayoutsState.ShowErrorMessage(
                                            json.getString(
                                                "message"
                                            )
                                        )
                                    )
                                } else {
                                    _state.onNext(PayoutsState.ShowErrorMessage(null))
                                }
                            }
                        }
                    } else {
                        _state.onNext(PayoutsState.ShowErrorMessage(null))
                    }
                }
            )
            .let { disposables.add(it) }
    }
}